#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from bpe.tests.code1_start import code1_start
from bpe.tests.code1_normal import code1_normal
from bpe.tests.code1_ranges import code1_ranges
from bpe.tests.code1_limits import code1_limits
from bpe.tests.code1_similar import code1_similar
from bpe.tests.code_start import code_start
from bpe.tests.code_normal import code_normal
from bpe.tests.code1_multy import code1_multy
from bpe.tests.demangle import demangle
from bpe.tests.matchoffset_normal import matchoffset_normal

print("--------------------------------------------------------------------\n")
if __name__ == '__main__':
    unittest.main(verbosity=2)
