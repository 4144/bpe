    id = GetStrucIdByName("{1}");
    if (id == -1)
        id = AddStrucEx({0}, "{1}", 0);
    if (id != -1)
    {{
        MakeUnknown({0}, GetStrucSize(id), DOUNK_DELNAMES);
        MakeStructEx({0}, GetStrucSize(id), "{1}");
    }}
