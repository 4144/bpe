#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # method1
    # 2019
    [
        (
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 0  movsx eax, word ptr [ebp+var_
            b"\x83\xC0\xAB"              # 7  add eax, 0FFFFFF8Dh
            b"\x3D\xAB\xAB\xAB\xAB"      # 10 cmp eax, 0ABDh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 15 ja loc_A044F4
            b"\xFF\x24\x85"              # 21 jmp ds:off_A04534[eax*4]
        ),
        {
            "add": (9, 1),
            "cmp": 11,
            "default": (17, False),
            "switch": 24,
        },
        {
            "method": 1,
            "switchStep": 4,
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xBF\x45\xAB"          # 0  movsx eax, word ptr [ebp+buf_38]
            b"\x83\xC0\xAB"              # 4  add eax, 0FFFFFF8Dh
            b"\x3D\xAB\xAB\xAB\xAB"      # 7  cmp eax, 0A6Bh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 12 ja loc_9F54F1
            b"\xFF\x24\x85"              # 18 jmp off_9F552C[eax*4]
        ),
        {
            "add": (6, 1),
            "cmp": 8,
            "default": (14, False),
            "switch": 21,
        },
        {
            "method": 1,
            "chain": False,
            "switchStep": 4,
        }
    ],
    # 2010-01-05
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_4190B0
            b"\x0F\xBF\xC0"              # 5  movsx eax, ax
            b"\x83\xC0\xAB"              # 8  add eax, 0FFFFFF8Dh
            b"\x3D\xAB\xAB\xAB\xAB"      # 11 cmp eax, 78Dh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 16 ja loc_5E1C18
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 22 jmp ds:off_5E1C50[eax*4]
        ),
        {
            "add": (10, 1),
            "cmp": 12,
            "default": (18, False),
            "switch": 25,
        },
        {
            "method": 1,
            "chain": False,
            "switchStep": 4,
        }
    ],
    # 2010-08-17
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_423630
            b"\x98"                      # 5  cwd cwde
            b"\x83\xC0\xAB"              # 6  add eax, 0FFFFFF8Dh
            b"\x3D\xAB\xAB\xAB\xAB"      # 9  cmp eax, 7CAh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 14 ja loc_62A68F
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 20 jmp ds:off_62A6DC[eax*4]
        ),
        {
            "add": (8, 1),
            "cmp": 10,
            "default": (16, False),
            "switch": 23,
        },
        {
            "method": 1,
            "chain": False,
            "switchStep": 4,
        }
    ],


    # method 2
    # 2004-01-07
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call CRagConnection_sub_418230
            b"\x0F\xBF\xC0"              # 5  movsx eax, ax
            b"\x83\xC0\xAB"              # 8  add eax, 0FFFFFF8Dh
            b"\x3D\xAB\xAB\xAB\xAB"      # 11 cmp eax, 17Fh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 16 ja loc_51D0EE
            b"\x33\xC9"                  # 22 xor ecx, ecx
            b"\x8A\x88\xAB\xAB\xAB\xAB"  # 24 mov cl, ds:byte_51D4B4[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 30 jmp ds:off_51D118[ecx*4]
        ),
        {
            "add": (10, 1),
            "cmp": 12,
            "default": (18, False),
            "switch": 33,
            "switch1": 26,
        },
        {
            "method": 2,
            "chain": False,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],

    # 2014-01-08
    [
        (
            b"\x0F\xBF\x45\xAB"          # 0  movsx eax, word ptr [ebp+var_78]
            b"\x3D\xAB\xAB\xAB\xAB"      # 4  cmp eax, 15Eh
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 9  jg loc_859EF3
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 15 jz loc_859EE2
            b"\x83\xE8\xAB"              # 21 sub eax, 73h
            b"\x3D\xAB\xAB\xAB\xAB"      # 24 cmp eax, 0E7h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 29 ja loc_85C5B0
            b"\x0F\xB6\x90\xAB\xAB\xAB\xAB"  # 35 movzx edx, byte_85C818[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 42 jmp off_85C5EC[edx*4]
        ),
        {
            "sub": (23, 1),
            "cmp1": 5,
            "cmp2": 25,
            "jg": (11, False),
            "jz": (17, False),
            "default": (31, False),
            "switch": 45,
            "switch1": 38,
        },
        {
            "method": 3,
            "chain": True,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],
    # 2014-09-24
    [
        (
            b"\x0F\xBF\x45\xAB"          # 0  movsx eax, word ptr [ebp+var_78]
            b"\x3D\xAB\xAB\xAB\xAB"      # 4  cmp eax, 15Eh
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 9  jg loc_8540F3
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 15 jz loc_8540E2
            b"\x83\xE8\xAB"              # 21 sub eax, 73h
            b"\x3D\xAB\xAB\xAB\xAB"      # 24 cmp eax, 0E7h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 29 ja loc_8568CF
            b"\x0F\xB6\x88\xAB\xAB\xAB\xAB"  # 35 movzx ecx, byte_856B0C[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 42 jmp off_8568E0[ecx*4]
        ),
        {
            "sub": (23, 1),
            "cmp1": 5,
            "cmp2": 25,
            "jg": (11, False),
            "jz": (17, False),
            "default": (31, False),
            "switch": 45,
            "switch1": 38,
        },
        {
            "method": 3,
            "chain": True,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],
    # 2013-01-03
    [
        (
            b"\x0F\xBF\x44\x24\xAB"      # 0  movsx eax, word ptr [esp+18h]
            b"\x3D\xAB\xAB\xAB\xAB"      # 5  cmp eax, 15Eh
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 10 jg loc_7F4989
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 16 jz loc_7F4978
            b"\x83\xE8\xAB"              # 22 sub eax, 73h
            b"\x3D\xAB\xAB\xAB\xAB"      # 25 cmp eax, 0E7h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 30 ja loc_7F68B4
            b"\x0F\xB6\x90\xAB\xAB\xAB\xAB"  # 36 movzx edx, byte_7F6B28[eax]
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 43 jmp off_7F68F8[edx*4]
        ),
        {
            "sub": (24, 1),
            "cmp1": 6,
            "cmp2": 26,
            "jg": (12, False),
            "jz": (18, False),
            "default": (32, False),
            "switch": 46,
            "switch1": 39,
        },
        {
            "method": 3,
            "chain": True,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],
    # 2011-01-04
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_423690
            b"\x98"                      # 5  cwd cwde
            b"\x3D\xAB\xAB\xAB\xAB"      # 6  cmp eax, 15Eh
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 11 jg loc_60062D
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 17 jz loc_60061C
            b"\x83\xE8\xAB"              # 23 sub eax, 73h
            b"\x3D\xAB\xAB\xAB\xAB"      # 26 cmp eax, 0E7h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 31 ja loc_601C9A
            b"\x0F\xB6\x90\xAB\xAB\xAB\xAB"  # 37 movzx edx, ds:byte_601F14[eax
            b"\xFF\x24\x95\xAB\xAB\xAB\xAB"  # 44 jmp ds:off_601CE4[edx*4]
        ),
        {
            "sub": (25, 1),
            "cmp1": 7,
            "cmp2": 27,
            "jg": (13, False),
            "jz": (19, False),
            "default": (33, False),
            "switch": 47,
            "switch1": 40,
        },
        {
            "method": 3,
            "chain": True,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],
    # 2019-06-05
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_97E290
            b"\x98"                      # 5  cwd cwde
            b"\x3D\xAB\xAB\xAB\xAB"      # 6  cmp eax, 187h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 11 jg loc_A62A9F
            b"\x74\xAB"                  # 17 jz short loc_A6247E
            b"\x83\xE8\xAB"              # 19 sub eax, 69h
            b"\x83\xF8\xAB"              # 22 cmp eax, 18h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 25 ja loc_A63CA8
            b"\x0F\xB6\x80\xAB\xAB\xAB\xAB"  # 31 movzx eax, ds:byte_A63D24[eax
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 38 jmp ds:off_A63CF0[eax*4]
        ),
        {
            "sub": (21, 1),
            "cmp1": 7,
            "cmp2": (24, 1),
            "jg": (13, False),
            "jz": (18, 1, False),
            "default": (27, False),
            "switch": 41,
            "switch1": 34,
        },
        {
            "method": 3,
            "chain": True,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],
    # 2019-01-09
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_8CDB50
            b"\x98"                      # 5  cwd cwde
            b"\x3D\xAB\xAB\xAB\xAB"      # 6  cmp eax, 187h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 11 jg loc_9DE386
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 17 jz loc_9DE749
            b"\x83\xE8\xAB"              # 23 sub eax, 69h
            b"\x83\xF8\xAB"              # 26 cmp eax, 18h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 29 ja loc_9DE73D
            b"\x0F\xB6\x80\xAB\xAB\xAB\xAB"  # 35 movzx eax, ds:byte_9DE7C0[eax
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 42 jmp ds:off_9DE78C[eax*4]
        ),
        {
            "sub": (25, 1),
            "cmp1": 7,
            "cmp2": (28, 1),
            "jg": (13, False),
            "jz": (19, False),
            "default": (31, False),
            "switch": 45,
            "switch1": 38,
        },
        {
            "method": 3,
            "chain": True,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],
    # 2015-01-07
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_7B4B70
            b"\x98"                      # 5  cwd cwde
            b"\x3D\xAB\xAB\xAB\xAB"      # 6  cmp eax, 187h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 11 jg loc_8A61F6
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 17 jz loc_8A64C2
            b"\x83\xE8\xAB"              # 23 sub eax, 69h
            b"\x83\xF8\xAB"              # 26 cmp eax, 18h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 29 ja loc_8A64B6
            b"\x0F\xB6\x88\xAB\xAB\xAB\xAB"  # 35 movzx ecx, byte_8A654C[eax]
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 42 jmp off_8A6518[ecx*4]
        ),
        {
            "sub": (25, 1),
            "cmp1": 7,
            "cmp2": (28, 1),
            "jg": (13, False),
            "jz": (19, False),
            "default": (31, False),
            "switch": 45,
            "switch1": 38,
        },
        {
            "method": 3,
            "chain": True,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],
    # 2010-01-05
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_4190B0
            b"\x0F\xBF\xC0"              # 5  movsx eax, ax
            b"\x3D\xAB\xAB\xAB\xAB"      # 8  cmp eax, 81h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 13 jg loc_5FDADB
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 19 jz loc_5FDAC8
            b"\x83\xC0\xAB"              # 25 add eax, 0FFFFFF97h
            b"\x83\xF8\xAB"              # 28 cmp eax, 0Bh
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 31 ja loc_5FDCA9
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 37 jmp ds:off_5FDD74[eax*4]
        ),
        {
            "add": (27, 1),
            "cmp1": 9,
            "cmp2": (30, 1),
            "jg": (15, False),
            "jz": (21, False),
            "default": (33, False),
            "switch": 40,
        },
        {
            "method": 4,
            "chain": True,
            "switchStep": 4,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_4232F0
            b"\x98"                      # 5  cwd cwde
            b"\x3D\xAB\xAB\xAB\xAB"      # 6  cmp eax, 187h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 11 jg loc_607BCA
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 17 jz loc_607CB0
            b"\x83\xE8\xAB"              # 23 sub eax, 69h
            b"\x3D\xAB\xAB\xAB\xAB"      # 26 cmp eax, 0C4h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 31 ja loc_607CA4
            b"\x0F\xB6\x80\xAB\xAB\xAB\xAB"  # 37 movzx eax, ds:byte_607D58[eax
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 44 jmp ds:off_607D24[eax*4]
        ),
        {
            "sub": (25, 1),
            "cmp1": 7,
            "cmp2": 27,
            "jg": (13, False),
            "jz": (19, False),
            "default": (33, False),
            "switch": 47,
            "switch1": 40,
        },
        {
            "method": 3,
            "chain": True,
            "switch1Step": 1,
            "switchStep": 4,
        }
    ],


    # method 7
    # 2008-06-17
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 5  mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call sub_4192F0
            b"\x0F\xBF\xC0"              # 12 movsx eax, ax
            b"\x3D\xAB\xAB\xAB\xAB"      # 15 cmp eax, 235h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 20 jg loc_5BEF6A
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 26 jz loc_5C1928
        ),
        {
            "cmp": 16,
            "jg": (22, False),
            "jz": (28, False),
        },
        {
            "method": 7,
            "next": 32,
        }
    ],
    # aro
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push offset buf
            b"\xE8\xAB\xAB\xAB\xAB"      # 5  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call sub_65D7F0
            b"\x98"                      # 17 cwd cwde
            b"\x3D\xAB\xAB\xAB\xAB"      # 18 cmp eax, 20Eh
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 23 jg loc_6F8376
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 29 jz loc_6F8365
        ),
        {
            "cmp": 19,
            "jg": (25, False),
            "jz": (31, False),
        },
        {
            "method": 7,
            "next": 35,
        }
    ],
    # bro 2018-04-12
    [
        (
            b"\x0F\xBF\x45\xAB"         # 0  movsx eax, [ebp+var_8]
            b"\x3D\xAB\xAB\xAB\xAB"     # 4  cmp eax, 20Eh
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 9  jg loc_8D4013
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 15 jz loc_8D4002
        ),
        {
            "cmp": 5,
            "jg": (11, False),
            "jz": (17, False),
        },
        {
            "method": 7,
            "next": 21,
        }
    ],
    # cro 2004-03-09
    [
        (
            b"\x50"                     # 0  push eax
            b"\xE8\xAB\xAB\xAB\xAB"     # 1  call CRagConnection_GetPacketType
            b"\x0F\xBF\xC0"             # 6  movsx eax, ax
            b"\x3D\xAB\xAB\xAB\xAB"     # 9  cmp eax, 81h
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 14 jg loc_53568A
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 20 jz loc_535677
        ),
        {
            "cmp": 10,
            "jg": (16, False),
            "jz": (22, False),
        },
        {
            "method": 7,
            "next": 26,
        }
    ],
]
