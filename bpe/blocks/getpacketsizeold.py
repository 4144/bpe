#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2003-10-28
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_10], 7Eh
            b"\xFF\xAB"                  # 6  call ebx
            b"\x89\x45\xAB"              # 8  mov [ebp+var_E], eax
            b"\x8D\x55\xAB"              # 11 lea edx, [ebp+var_10]
            b"\x0F\xBF\x45\xAB"          # 14 movsx eax, [ebp+var_10]
            b"\x52"                      # 18 push edx
            b"\x50"                      # 19 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 20 mov ecx, offset g_instanceR
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (21, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            b"\xAB"                      # 5  push edx
            b"\x6A\xAB"                  # 6  push 7Dh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 8  mov [ebp+var_2], 7Dh
            b"\xE8"                      # 14 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 14,
            "packetId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_10], 7Eh
            b"\xFF\xD6"                  # 6  call esi
            b"\x0F\xBF\x4D\xF0"          # 8  movsx ecx, [ebp+var_10]
            b"\x89\x45\xF2"              # 12 mov [ebp+var_E], eax
            b"\x8D\x45\xF0"              # 15 lea eax, [ebp+var_10]
            b"\x50"                      # 18 push eax
            b"\x51"                      # 19 push ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 20 mov ecx, offset g_instanceR
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (21, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 94h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_1C], 94h
            b"\x89\x45\xAB"              # 16 mov [ebp+var_1A], eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 94h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_1C], 94h
            b"\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 193h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_18], 193h
            b"\x89\x75\xAB"              # 16 mov [ebp+var_16], esi
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 193h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_18], 193h
            b"\x89\x5D\xAB"              # 16 mov [ebp+var_16], ebx
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 90h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_10], 90h
            b"\x89\x45\xAB"              # 16 mov [ebp+var_E], eax
            b"\xC6\x45\xAB\xAB"          # 19 mov [ebp+var_A], 1
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            b"\x52"                      # 5  push edx
            b"\x68\xAB\xAB\x00\x00"      # 6  push 0D0h
            b"\xC7\x83\xAB\xAB\x00\x00\xAB\xAB\x00\x00"  # 11 mov [ebx+0E4h], 1
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 21 mov word ptr [ebp+arg_4], 0D0h
            b"\xC6\x45\xAB\xAB"          # 27 mov byte ptr [ebp+arg_4+2], 1
            b"\xE8"                      # 31 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 31,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0CFh
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\xE8"                      # 10 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 10,
            "packetId": 1,
            "retOffset": 0,
            "goto": (5, 4),  # offset 5, jmp size addr 4
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            b"\x50"                      # 5  push eax
            b"\x68\xAB\xAB\x00\x00"      # 6  push 0ABh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_4], 0ABh
            b"\x66\x89\x55\xAB"          # 17 mov word ptr [ebp+arg_4+2], dx
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            b"\x50"                      # 5  push eax
            b"\x68\xAB\xAB\x00\x00"      # 6  push 0A9h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+var_14], 0A9h
            b"\x66\x89\x55\xAB"          # 17 mov word ptr [ebp+var_14+2], dx
            b"\x66\x89\x75\xAB"          # 21 mov word ptr [ebp+var_10], si
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0A2h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\xC7\x83\xAB\xAB\x00\x00\xAB\xAB\x00\x00"  # 10 mov [ebx+2C4h], 1
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 20 mov word ptr [ebp+var_14], 0A2h
            b"\x66\x89\x45\xAB"          # 26 mov word ptr [ebp+var_10], ax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_14], 0A7h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_51AF40
            b"\x89\x45\xAB"              # 11 mov [ebp+var_10], eax
            b"\x8D\x55\xAB"              # 14 lea edx, [ebp+var_14]
            b"\x0F\xBF\x45\xAB"          # 17 movsx eax, word ptr [ebp+var_14]
            b"\x52"                      # 21 push edx
            b"\x50"                      # 22 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 23 mov ecx, offset g_instanceR
            b"\x66\x89\x75\xAB"          # 28 mov word ptr [ebp+var_14+2], si
            b"\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (24, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 103h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 10 mov [ebp+var_4], 29h
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp+var_84+2], eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_48], 72h
            b"\xFF\xD7"                  # 6  call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 8  mov ecx, dword_690A94
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 14 mov edx, dword_690A98
            b"\x89\x45\xAB"              # 20 mov [ebp+var_3A], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 23 mov eax, dword_690378
            b"\x89\x4D\xAB"              # 28 mov [ebp+var_46], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_68FDB8
            b"\x89\x55\xAB"              # 36 mov [ebp+var_42], edx
            b"\x89\x45\xAB"              # 39 mov [ebp+var_3E], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_5A6F00
            b"\x0F\xBF\xAB\xAB"          # 47 movsx edx, [ebp+var_48]
            b"\x8D\x4D\xAB"              # 51 lea ecx, [ebp+var_48]
            b"\x88\x45\xAB"              # 54 mov [ebp+var_36], al
            b"\x51"                      # 57 push ecx
            b"\x52"                      # 58 push edx
            b"\xB9\xAB\xAB\xAB\xAB"      # 59 mov ecx, offset g_instanceR
            b"\xE8"                      # 64 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 64,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (60, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_18], 65h
            b"\x66\x89\x75\xAB"          # 6  mov word ptr [ebp+var_E+4], si
            b"\x89\x55\xAB"              # 10 mov [ebp+var_16], edx
            b"\x89\x45\xAB"              # 13 mov [ebp+var_12], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_5A6F00
            b"\x88\x45\xAB"              # 21 mov [ebp+var_8], al
            b"\x8D\x55\xAB"              # 24 lea edx, [ebp+var_18]
            b"\x0F\xBF\x45\xAB"          # 27 movsx eax, [ebp+var_18]
            b"\x52"                      # 31 push edx
            b"\x50"                      # 32 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 33 mov ecx, offset g_instanceR
            b"\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": (4, 2),
            "retOffset": 0,
            "goto": (33, 4)
        },
        {
            "g_instanceR": (34, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_instanceR
            b"\x66\x89\x55\xAB"          # 7  mov [ebp-9], dx
            b"\xE8"                      # 11 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 11,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_instanceR": (3, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_34], 68h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call _strncpy
            b"\xA1\xAB\xAB\xAB\xAB"      # 11 mov eax, g_serviceType
            b"\x83\xC4\xAB"              # 16 add esp, 0Ch
            b"\x85\xC0"                  # 19 test eax, eax
            b"\xC6\x45\xAB\xAB"          # 21 mov [ebp+var_7], 0
            b"\x75\xAB"                  # 25 jnz short loc_537169
            b"\x8A\x83\xAB\xAB\xAB\x00"  # 27 mov al, [ebx+0A7h]
            b"\x84\xC0"                  # 33 test al, al
            b"\x74\xAB"                  # 35 jz short loc_537169
            b"\xC6\x45\xAB\xAB"          # 37 mov [ebp+var_2E], 0
            b"\x56"                      # 41 push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 42 push offset aS
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call nullsub_4
            b"\x0F\xBF\xAB\xAB"          # 52 movsx ecx, [ebp+var_34]
            b"\x83\xC4\xAB"              # 56 add esp, 8
            b"\x8D\x45\xAB"              # 59 lea eax, [ebp+var_34]
            b"\x50"                      # 62 push eax
            b"\x51"                      # 63 push ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 64 mov ecx, offset g_instanceR
            b"\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (65, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+cp], 72h
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 6  call ds:timeGetTime
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 12 mov ecx, dword_690378
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 18 mov edx, dword_690A94
            b"\x89\x45\xAB"              # 24 mov dword ptr [ebp+var_E], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 27 mov eax, dword_690A98
            b"\x89\x4D\xAB"              # 32 mov [ebp+var_12], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset dword_68FDB8
            b"\x89\x55\xAB"              # 40 mov [ebp-1Ah], edx
            b"\x89\x45\xAB"              # 43 mov [ebp+var_16], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 46 call sub_5A6F00
            b"\x88\x45\xAB"              # 51 mov [ebp+var_E+4], al
            b"\x8D\x55\xAB"              # 54 lea edx, [ebp+cp]
            b"\x0F\xBF\x45\xAB"          # 57 movsx eax, [ebp+cp]
            b"\x52"                      # 61 push edx
            b"\x50"                      # 62 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 63 mov ecx, offset g_instanceR
            b"\xE8"                      # 68 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 68,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (64, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 1BFh
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\xC6\x45\xAB\xAB"          # 10 mov [ebp+var_2], 1
            b"\xE8"                      # 14 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 14,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            b"\x50"                      # 5  push eax
            b"\x68\xAB\xAB\x00\x00"      # 6  push 1BFh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov [ebp+var_4], 1BFh
            b"\xC6\x45\xAB\xAB"          # 17 mov [ebp+var_2], 1
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2004-04-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_14], 7Eh
            b"\xFF\xD7"                  # 6  call edi
            b"\x0F\xBF\x4D\xAB"          # 8  movsx ecx, [ebp+var_14]
            b"\x89\x45\xAB"              # 12 mov [ebp+var_12], eax
            b"\x8D\x45\xAB"              # 15 lea eax, [ebp+var_14]
            b"\x50"                      # 18 push eax
            b"\x51"                      # 19 push ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 20 mov ecx, offset g_instanceR
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (21, False),
        }
    ],
    # 2004-04-22
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 149h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_C], 149h
            b"\x89\x45\xAB"              # 16 mov [ebp+var_A], eax
            b"\xC6\x45\xAB\xAB"          # 19 mov [ebp+var_6], 2
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 23 mov [ebp+var_5], 0Ah
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2004-04-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_48], 72h
            b"\xA3\xAB\xAB\xAB\xAB"      # 6  mov dword_63D184, eax
            b"\xFF\xD7"                  # 11 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 13 mov ecx, dword_67EE28
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 19 mov edx, dword_67E718
            b"\x89\x45\xAB"              # 25 mov [ebp+var_3A], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 28 mov eax, dword_67EE24
            b"\x89\x4D\xAB"              # 33 mov [ebp+var_42], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 36 mov ecx, offset dword_67E1B8
            b"\x89\x45\xAB"              # 41 mov [ebp+var_46], eax
            b"\x89\x55\xAB"              # 44 mov [ebp+var_3E], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call sub_5BDCD0
            b"\x0F\xBF\x4D\xAB"          # 52 movsx ecx, [ebp+var_48]
            b"\x88\x45\xAB"              # 56 mov [ebp+var_36], al
            b"\x8D\x45\xAB"              # 59 lea eax, [ebp+var_48]
            b"\x50"                      # 62 push eax
            b"\x51"                      # 63 push ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 64 mov ecx, offset g_instanceR
            b"\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (65, False),
        }
    ],
    # 2004-04-22
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            b"\x50"                      # 5  push eax
            b"\x68\xAB\xAB\x00\x00"      # 6  push 1DBh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_0+2], 1DBh
            b"\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2004-04-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_6C], 1FBh
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call _strncpy
            b"\xA1\xAB\xAB\xAB\xAB"      # 11 mov eax, g_serviceType
            b"\x83\xC4\xAB"              # 16 add esp, 18h
            b"\x85\xC0"                  # 19 test eax, eax
            b"\xC6\x45\xAB\xAB"          # 21 mov [ebp+var_35], 0
            b"\x75\x0E"                  # 25 jnz short loc_53D606
            b"\x8A\x83\xAB\xAB\xAB\xAB"  # 27 mov al, [ebx+0A7h]
            b"\x84\xC0"                  # 33 test al, al
            b"\x74\x04"                  # 35 jz short loc_53D606
            b"\xC6\x45\xAB\xAB"          # 37 mov [ebp+var_2E], 0
            b"\x57"                      # 41 push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 42 push offset aS
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call nullsub_4
            b"\xA1\xAB\xAB\xAB\xAB"      # 52 mov eax, g_serviceType
            b"\x83\xC4\xAB"              # 57 add esp, 8
            b"\x83\xF8\xAB"              # 60 cmp eax, 0Ah
            b"\x75\x0B"                  # 63 jnz short loc_53D629
            b"\x0F\xBF\x45\xAB"          # 65 movsx eax, [ebp+var_6C]
            b"\x8D\x55\xAB"              # 69 lea edx, [ebp+var_6C]
            b"\x52"                      # 72 push edx
            b"\x50"                      # 73 push eax
            b"\xEB\x09"                  # 74 jmp short loc_53D632
            b"\x0F\xBF\x55\xAB"          # 76 movsx edx, [ebp+var_34]
            b"\x8D\x4D\xAB"              # 80 lea ecx, [ebp+var_34]
            b"\x51"                      # 83 push ecx
            b"\x52"                      # 84 push edx
            b"\xB9\xAB\xAB\xAB\xAB"      # 85 mov ecx, offset g_instanceR
            b"\xE8"                      # 90 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 90,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (86, False),
        }
    ],
    # 2004-08-09
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            b"\x50"                      # 5  push eax
            b"\x68\xAB\xAB\x00\x00"      # 6  push 0BBh
            b"\xE8"                      # 11 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 11,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2004-08-09
    [
        (
            b"\x6A\xAB"                  # 0  push 72h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_instanceR
            b"\xC7\x83\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [ebx+2DCh], 1
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 17 mov word ptr [ebp+var_18], 72h
            b"\x66\x89\x45\xAB"          # 23 mov word ptr [ebp+var_10], ax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_instanceR": (3, False),
        }
    ],
    # 2004-08-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 103h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 10 mov [ebp+var_4], 2Fh
            b"\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2004-08-09
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_1C], 65h
            b"\x66\x89\x75\xAB"          # 6  mov word ptr [ebp+var_10+2], si
            b"\x89\x55\xAB"              # 10 mov [ebp+var_1C+2], edx
            b"\x89\x45\xAB"              # 13 mov [ebp+var_16], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_5C6420
            b"\x8D\x55\xAB"              # 21 lea edx, [ebp+var_1C]
            b"\x88\x45\xAB"              # 24 mov [ebp+var_C], al
            b"\x0F\xBF\x45\xAB"          # 27 movsx eax, word ptr [ebp+var_1C]
            b"\x52"                      # 31 push edx
            b"\x50"                      # 32 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 33 mov ecx, offset g_instanceR
            b"\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (34, False),
        }
    ],
    # 2004-08-09
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_30], 1DDh
            b"\x89\x45\xAB"              # 6  mov [ebp+var_2E], eax
            b"\xF3\xA5"                  # 9  rep movsd
            b"\x8D\x4D\xAB"              # 11 lea ecx, [ebp+var_12]
            b"\x51"                      # 14 push ecx
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 15 lea ecx, [ebp+var_88]
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_41E600
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_546020
            b"\x88\x45\xAB"              # 31 mov [ebp+var_2], al
            b"\x8D\x55\xAB"              # 34 lea edx, [ebp+var_30]
            b"\x0F\xBF\x45\xAB"          # 37 movsx eax, [ebp+var_30]
            b"\x52"                      # 41 push edx
            b"\x50"                      # 42 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 43 mov ecx, offset g_instanceR
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (44, False),
        }
    ],
]
