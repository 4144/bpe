#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchStrings(self, strings, strOffset=-1):
    arr = dict()
    exe_stringBySection = self.exe.stringBySection
    rdata = self.exe.rdataSection
    cache = self.exe.cache.strings
    if strOffset == -1:
        for string in strings:
            if string in cache.arr:
                offset, section = cache.arr[string]
            else:
                offset, section = exe_stringBySection(string, rdata)
                cache.arr[string] = (offset, section)
            if offset is False:
                continue
            addr = offset - section.rawVaDiff  # rawToVa
            arr[string] = addr
    else:
        for data in strings:
            string = data[strOffset]
            if string in cache.arr:
                offset, section = cache.arr[string]
            else:
                offset, section = exe_stringBySection(string, rdata)
                cache.arr[string] = (offset, section)
            if offset is False:
                continue
            addr = offset - section.rawVaDiff  # rawToVa
            arr[string] = addr
    return arr
