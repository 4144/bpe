#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from bpe.tests.common import loadClient


class matchoffset_normal(unittest.TestCase):
    def setUp(self):
        self.extractor = loadClient("head.exe")


    def tearDown(self):
        self.extractor = None


    def test1(self):
        pattern = (b"\x2E\x74\x65\x78\x74\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 392)
        self.assertTrue(offset is True)


    def test2(self):
        pattern = (b"\x2E\x74\x65\x78\x74\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 391)
        self.assertTrue(offset is False)


    def test3(self):
        pattern = (b"\x2E\x74\x65\x78\x74\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 393)
        self.assertTrue(offset is False)


    def test4(self):
        pattern = (b"\x2E\x74\xAB\x78\x74\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 392)
        self.assertTrue(offset is True)


    def test5(self):
        pattern = (b"\x2E\x74\xAB\xAB\x74\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 392)
        self.assertTrue(offset is True)


    def test6(self):
        pattern = (b"\x2E\x74\x65\x78\x74\x00\x01")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 392)
        self.assertTrue(offset is False)


    def test7(self):
        pattern = (b"\x2E")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 392)
        self.assertTrue(offset is True)


    def test8(self):
        pattern = (b"\x0F\x1F\x84\x00\x00\x00\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 1032)
        self.assertTrue(offset is True)


    def test9(self):
        pattern = (b"\x0F\x1F\x84\x00\x00\x00\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 1033)
        self.assertTrue(offset is False)


    def test10(self):
        pattern = (b"\x0F\x1F\x84\x00\x00\x00\x00\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 1032)
        self.assertTrue(offset is False)


    def test11(self):
        pattern = (b"\x0F\x1F\x84\x00\x00\x00\x00\x00\x00\x00")
        offset = self.extractor.exe.matchWildcardOffset(pattern, b"\xAB", 1032)
        self.assertTrue(offset is False)
