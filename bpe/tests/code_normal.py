#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from bpe.tests.common import loadClient


class code_normal(unittest.TestCase):
    def setUp(self):
        self.extractor = loadClient("head.exe")


    def tearDown(self):
        self.extractor = None


    def test1(self):
        code = (b"\x2E\x74\x65\x78\x74\x00\x00")
        offsets = self.extractor.exe.codesWildcard(code, b"\xAB")
        self.assertTrue(offsets is not False)
        self.assertEqual(offsets, 392)


    def test2(self):
        code = (b"\x2E\xAB\x65\x78\x74\xAB\x00")
        offsets = self.extractor.exe.codes(code)
        self.assertTrue(offsets is False)


    def test3(self):
        code = (b"\x2E\x65\x64\x61\x74\x61\x00\x00")
        offsets = self.extractor.exe.codes(code, -1)
        self.assertTrue(offsets is not False)
        self.assertEqual(len(offsets), 1)
        self.assertEqual(offsets[0], 632)


    def test4(self):
        code = (b"\x2E\x65\x64\x61\x74\x61\x01\x00")
        offsets = self.extractor.exe.codes(code, 1)
        self.assertTrue(offsets is False)


    def test5(self):
        code = (b"\x2E\xAB\xAB\x61\x74\x61\x00\x00")
        offsets = self.extractor.exe.codesWildcard(code, b"\xAB", -1)
        self.assertTrue(offsets is not False)
        self.assertEqual(len(offsets), 5)
        self.assertEqual(offsets, [472, 512, 552, 632, 672])
