#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.callfunction import CallFunction
from bpe.math import Math


def unimplimentedOpcode(asm):
    asm.printLine()
    cmd = asm.getWord(asm.pos)
    asm.exe.log("Error: Unimplimented opcode detected: {0}".format(hex(cmd)))
    exit(1)
    pass


def op_unset_reg(asm):
    register = asm.regs[0]
    asm.registers[register] = 0


def op_push_reg(asm):
    register = asm.regs[0]
    asm.registers["esp"] = asm.registers["esp"] - 4
    asm.setMemory32(asm.registers["esp"], asm.registers[register])
    if asm.runFlag == 0:
        asm.pushCounter = asm.pushCounter + 4


def op_push_big(asm):
    param = asm.params[0]
    asm.registers["esp"] = asm.registers["esp"] - 4
    val = asm.exe.getVarAddr(asm.pos, param)
    asm.setMemory32(asm.registers["esp"], val)
    if asm.runFlag == 0:
        asm.pushCounter = asm.pushCounter + 4


def op_push_small(asm):
    param = asm.params[0]
    asm.registers["esp"] = asm.registers["esp"] - 4
    val = Math.extendByteToInt(asm.exe.getVarAddr(asm.pos, param))
    asm.setMemory32(asm.registers["esp"], val)
    if asm.runFlag == 0:
        asm.pushCounter = asm.pushCounter + 4


def op_pop_reg(asm):
    register = asm.regs[0]
    asm.registers[register] = asm.getMemory32(asm.registers["esp"])
    asm.registers["esp"] = asm.registers["esp"] + 4
    if asm.runFlag == 0:
        asm.popCounter = asm.popCounter + 4


def op_add_reg_val(asm):
    register = asm.regs[0]
    param = asm.params[0]
    val = asm.exe.getVarAddr(asm.pos, param)
    asm.addRegister(register, val)
    if register == "esp":
        if asm.runFlag == 0:
            if asm.pos - asm.lastCallFunctionPos == 5:
                asm.callFunctions[asm.lastCallFunctionAddr].fixSp = val


def op_sub_val(asm):
    register = asm.regs[0]
    param = asm.params[0]
    val = asm.exe.getVarAddr(asm.pos, param)
    asm.subRegister(register, val)


def op_and_reg_val(asm):
    register = asm.regs[0]
    param = asm.params[0]
    val = Math.extendByteToInt(asm.exe.getVarAddr(asm.pos, param))
    asm.andRegister(register, val)


def op_or_reg_val(asm):
    register = asm.regs[0]
    param = asm.params[0]
    val = Math.extendByteToInt(asm.exe.getVarAddr(asm.pos, param))
    asm.orRegister(register, val)


def op_mov_reg_reg(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    asm.registers[register1] = asm.registers[register2]


def op_mov_reg_val(asm):
    register = asm.regs[0]
    param = asm.params[0]
    val = asm.exe.getVarAddr(asm.pos, param)
    asm.registers[register] = val
    if asm.lastRegValue is not True:
        if register == "eax":
            asm.lastRegValue = val
        else:
            asm.lastRegValue = None
    if asm.exe.isClientTooOld() is True:
        if register == "ecx":
            if asm.runFlag == 1:
                if asm.beforeSecondCall is not None:
                    if asm.beforeSecondCall is not True and \
                       (asm.beforeSecondCall is not False or
                            asm.exe.isClientSkipFlag() is False):
                        print("Error: wrong flag op_mov_reg_val: {0}".format(
                            asm.beforeSecondCall))
                        exit(0)
                if asm.exe.isClientSkipFlag() is False and asm.cxSet is True:
                    print("Double cx set")
                    exit(0)
            if asm.beforeSecondCall is not True:
                asm.beforeSecondCall = False
                if asm.debug is True:
                    print("asm.beforeSecondCall = False")
            asm.cxSet = True
            if asm.debug is True:
                print("asm.cxSet = True")


def op_mov_reg_ptr_val(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    param = asm.params[0]
    offset = asm.registers[register2] + \
        asm.exe.getVarAddr(asm.pos, param)
    asm.registers[register1] = asm.getMemory32(offset)


def op_mov_reg_ptr_smallval(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    param = asm.params[0]
    offset = asm.registers[register2] + \
        asm.exe.getVarAddr(asm.pos, param)
    asm.registers[register1] = asm.getMemory32(offset)


def op_mov_reg_ptr_zero(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    offset = asm.registers[register2]
    asm.registers[register1] = asm.getMemory32(offset)


def op_mov_ptr_reg_byte(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    param = asm.params[0]
    offset = asm.registers[register1] + \
        asm.exe.getVarAddr(asm.pos, param)
    val = asm.registers[register2]
    asm.setMemory32(offset, val)
    if asm.exe.isClientTooOld() is True:
        if register1 == "eax":
            if asm.beforeSecondCall is True or \
               (asm.beforeSecondCall is None and
                    asm.prevPacketId != 0 and
                    asm.cxSet is False):
                asm.addPacket(asm.prevPacketId, val, val, -1)
                asm.prevPacketId = 0
                if asm.debug is True:
                    print("asm.prevPacketId = 0")
            else:
                if asm.runFlag == 1:
                    print("Error: wrong flag op_mov_ptr_reg_byte: {0}".format(
                        asm.beforeSecondCall))
                    exit(0)
            asm.beforeSecondCall = None
            if asm.debug is True:
                print("asm.beforeSecondCall = None3")


def op_mov_ptr_reg_int(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    param = asm.params[0]
    offset = asm.registers[register1] + \
        asm.exe.getVarAddr(asm.pos, param)
    asm.setMemory32(offset, asm.registers[register2])


def op_lea_reg_val(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    param = asm.params[0]
    asm.setRegister(register1, asm.registers[register2] +
                    asm.exe.getVarAddr(asm.pos, param))


def op_lea_reg_zero(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    asm.setRegister(register1, asm.registers[register2])


def op_mov_ptr_reg_byte_int(asm):
    register = asm.regs[0]
    param1 = asm.params[0]
    param2 = asm.params[1]
    offset = asm.registers[register] + \
        asm.exe.getVarAddr(asm.pos, param1)
    val = asm.exe.getVarAddr(asm.pos, param2)
    asm.setMemory32(offset, val)
    if asm.exe.isClientTooOld() is True:
        if register == "eax":
            if asm.beforeSecondCall is True or \
               (asm.beforeSecondCall is None and
                    asm.prevPacketId != 0 and
                    asm.cxSet is False):
                asm.addPacket(asm.prevPacketId, val, val, -1)
                asm.prevPacketId = 0
                if asm.debug is True:
                    print("asm.prevPacketId = 0")
            else:
                if asm.runFlag == 1:
                    print("Error: wrong flag op_mov_ptr_reg_byte_int: {0}".
                          format(asm.beforeSecondCall))
                    exit(0)
            asm.beforeSecondCall = None
            if asm.debug is True:
                print("asm.beforeSecondCall = None1")


def op_mov_ptr_reg_zero_int(asm):
    register = asm.regs[0]
    param = asm.params[0]
    offset = asm.registers[register]
    val = asm.exe.getVarAddr(asm.pos, param)
    asm.setMemory32(offset, val)
    if asm.exe.isClientTooOld() is True:
        if register == "eax":
            if asm.beforeSecondCall is True:
                asm.addPacket(asm.prevPacketId, val, val, -1)
                asm.prevPacketId = 0
                if asm.debug is True:
                    print("asm.prevPacketId = 0")
            else:
                if asm.runFlag == 1:
                    print("Error: wrong flag op_mov_ptr_reg_zero_int: {0}".
                          format(asm.beforeSecondCall))
                    exit(0)
            asm.beforeSecondCall = None
            if asm.debug is True:
                print("asm.beforeSecondCall = None2")


def op_mov_ptr_reg_zero_reg(asm):
    register1 = asm.regs[0]
    register2 = asm.regs[1]
    offset = asm.registers[register1]
    val = asm.registers[register2]
    asm.setMemory32(offset, val)
    if asm.exe.isClientTooOld() is True:
        if register1 == "eax":
            if asm.beforeSecondCall is True:
                asm.addPacket(asm.prevPacketId, val, val, -1)
                asm.prevPacketId = 0
                if asm.debug is True:
                    print("asm.prevPacketId = 0")
            else:
                if asm.runFlag == 1:
                    print("Error: wrong flag op_mov_ptr_reg_zero_reg: {0}".
                          format(asm.beforeSecondCall))
                    exit(0)
            asm.beforeSecondCall = None
            if asm.debug is True:
                print("asm.beforeSecondCall = None3")


def op_mov_ptr_reg_int_int(asm):
    register = asm.regs[0]
    param1 = asm.params[0]
    param2 = asm.params[1]
    offset = asm.registers[register] + \
        asm.exe.getVarAddr(asm.pos, param1)
    val = asm.exe.getVarAddr(asm.pos, param2)
    asm.setMemory32(offset, val)


def op_inc_reg(asm):
    register = asm.regs[0]
    asm.addRegister(register, 1)


def op_call(asm):
    param = asm.params[0]
    ptr = asm.exe.getVarAddr(asm.pos, param)
    addr = Math.sumOffset(asm.pos + asm.codeSize, ptr)
    if asm.inJump is True:
        # emulate return value from function
        asm.registers["eax"] = 0

    # probably this fix need for all versions?
    ver = asm.exe.client_date
    if asm.exe.isClientTooOld() is True and \
       ver in (20080527, 20080528, 20080603):
        asm.registers["eax"] = 0xE000000

    if asm.runFlag == 0:
        if addr not in asm.callFunctions:
            asm.callFunctions[addr] = CallFunction(addr)
        asm.callFunctions[addr].addCall(asm)
        asm.lastCallFunctionAddr = addr
        asm.lastCallFunctionPos = asm.pos
        asm.pushCounter = 0
        asm.popCounter = 0
        asm.allCallAddrs.append(addr)
    elif asm.runFlag == 1:
        if addr in asm.allCallFunctions:
            func = asm.allCallFunctions[addr]
            if func.callCounter == 1:
                # probably alloc or other crap,
                # need walk inside or emulate walking.
                if asm.lastRegValue is not None:
                    asm.addRegister("esp", asm.lastRegValue)
                    if asm.debug is True:
                        print("Alloca emulation. Moving esp to +{0}".format(
                              hex(asm.lastRegValue)))
                    asm.lastRegValue = True
                    asm.addRegister("esp", func.adjustSp)
                    return
        if addr in asm.callFunctions:
            func = asm.callFunctions[addr]
            func.collectArgs(asm)
            # asm.cxSet = False
            if func.fixSp != 0 and func.fixSp == func.adjustSp:
                if asm.debug is True:
                    print("Skip call adjust because detected esp fix after")
            else:
                asm.addRegister("esp", func.adjustSp)
            # print("adjust stack: {0}".format(func.adjustSp))
        elif addr in asm.allCallFunctions:
            func = asm.allCallFunctions[addr]
            asm.addRegister("esp", func.adjustSp)
        else:
            asm.exe.log("Error. Need adjust stack based on function")
            exit(0)


def op_cmp_reg_int(asm):
    register = asm.regs[0]
    param = asm.params[0]
    val1 = asm.registers[register]
    val2 = asm.exe.getVarAddr(asm.pos, param)
    if val1 <= val2:
        asm.flags["le"] = True
    else:
        asm.flags["le"] = False


def op_jmp_le_byte(asm):
    asm.inJump = True
    param = asm.params[0]
    if asm.flags["le"] is True:
        offset = asm.pos + \
            asm.codeSize + \
            asm.exe.getVarAddr(asm.pos, param)
        asm.pos = offset
        asm.Jumped = True


def op_retn(asm):
    pass
