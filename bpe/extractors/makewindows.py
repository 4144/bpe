#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.calls import searchCallsToCall
from bpe.blocks.blocks import searchBlocks, blocksToLabels, showBlocksInfo


def searchMakeWindows(self):
    showInfo = False
    makeWindowVa = self.UIWindowMgrMakeWindowVa
    offsets = searchCallsToCall(self, makeWindowVa)
    if offsets is False:
        print("Error: found no UIWindowMgr::MakeWindow calls")
        exit(1)
    relVars = {"g_windowMgr": self.gWindowMgr}
    from bpe.blocks.makewindow import blocks
    found, errors = searchBlocks(self,
                                 "MakeWindows",
                                 offsets,
                                 blocks,
                                 relVars,
                                 "windowId")
    self.makeWindowBlocks = found
    self.makeWindowStats = (len(found), len(errors))
    if showInfo is True:
        showBlocksInfo(self, offsets, found, errors)


def addMakeWindowLabels(self):
    name = "MakeWindow_{0}"
    blocksToLabels(self, self.makeWindowBlocks, name)
