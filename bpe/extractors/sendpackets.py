#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.calls import searchCallsToCall
from bpe.blocks.blocks import searchBlocks, blocksToLabels, showBlocksInfo


def reduceErrors(self, errors, otherBlocks):
    addrList = set()
    for block in otherBlocks:
        addrList.add(block[3])

    # 0  call CRagConnection_GetPacketSize
    # 5  push eax
    # 6  call CRagConnection_instanceR
    # 11 mov ecx, eax
    # 13 call CRagConnection_SendPacket
    code = (
        b"\xE8\xAB\xAB\xAB\xAB"            # 0 call CRagConnection_GetPacketSiz
        b"\x50"                            # 5 push eax
        b"\xE8\xAB\xAB\xAB\xAB"            # 6 call CRagConnection_instanceR
        b"\x8B\xC8"                        # 11 mov ecx, eax
        b"\xE8"                            # 13 call CRagConnection_SendPacket
    )
    instanceOffset = 7

    errors2 = []
    for error in errors:
        addr = error - 13
        if addr in addrList:
            if self.exe.matchWildcardOffset(code,
                                            b"\xAB",
                                            addr) is False:
                errors2.append(error)
                continue
            instance = self.getAddr(addr, instanceOffset, instanceOffset + 4)
            if instance == self.instanceR:
                # print "intersect: 0x{0}".format(hex(self.exe.rawToVa(error)))
                continue
        errors2.append(error)
    return errors2


def searchSendPackets(self):
    showInfo = False
    offsets = searchCallsToCall(self, self.sendPacketVa)
    if self.instanceR != 0:
        from bpe.blocks.sendpacketnew import blocks
        relVars = {"instanceR": self.instanceR}
    else:
        from bpe.blocks.sendpacketold import blocks
        relVars = {"g_instanceR": self.g_instanceR}
    found, errors = searchBlocks(self,
                                 "sendPackets",
                                 offsets,
                                 blocks,
                                 relVars,
                                 "packetId")
    errors = reduceErrors(self, errors, self.getPacketSizeBlocks)
    self.sendPacketBlocks = found
    self.sendPacketStats = (len(found), len(errors))
    if showInfo is True:
        showBlocksInfo(self, offsets, found, errors)


def addSendPacketLabels(self):
    name = "CRagConnection_SendPacket_{0}"
    blocksToLabels(self, self.sendPacketBlocks, name)
