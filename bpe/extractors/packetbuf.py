#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchPacketBuf(self, errorExit):
    # search in CLoginMode_PollNewtorkStatus
    # 2017-11-01
    # 0  push packet_buf_len
    # 5  push 0
    # 7  push offset packet_buf
    # 12 call memset
    # 17 add esp, A
    # 20 call CRagConnection_instanceR
    # 25 mov ecx, eax
    # 27 call CConnection_Poll
    lenOffset = 1
    bufOffset = 8
    memsetOffset = 13
    instanceOffset = 21
    pollOffset = 28
    code = (
        b"\x68\xAB\xAB\xAB\x00" +
        b"\x6A\x00" +
        b"\x68\xAB\xAB\xAB\xAB" +
        b"\xE8\xAB\xAB\xAB\x00" +
        b"\x83\xAB\xAB" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xAB" +
        b"\xE8")
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.loginPollAddr,
                                   self.loginPollAddr + 0x50)
    if offset is False:
        # 2011-01-04
        # 0  push packet_buf_len
        # 5  xor ebx, ebx
        # 7  push ebx
        # 8  push offset packet_buf
        # 13  mov esi, ecx
        # 15 call memset
        # 20 add esp, A
        # 23 call CRagConnection_instanceR
        # 28 mov ecx, eax
        # 30 call CConnection_Poll
        lenOffset = 1
        bufOffset = 9
        memsetOffset = 16
        instanceOffset = 24
        pollOffset = 31
        code = (
            b"\x68\xAB\xAB\xAB\x00" +
            b"\x33\xAB" +
            b"\xAB" +
            b"\x68\xAB\xAB\xAB\xAB" +
            b"\xAB\xAB" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x83\xAB\xAB" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xAB" +
            b"\xE8")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.loginPollAddr,
                                       self.loginPollAddr + 0x3e)
    if offset is False:
        # 2015-01-07
        # 0  push packet_buf_len
        # 5  push ebx
        # 6  push offset packet_buf
        # 11 call memset
        # 16 add esp, A
        # 19 call CRagConnection_instanceR
        # 24 mov ecx, eax
        # 26 call CConnection_Poll
        lenOffset = 1
        bufOffset = 7
        memsetOffset = 12
        instanceOffset = 20
        pollOffset = 27
        code = (
            b"\x68\xAB\xAB\xAB\x00" +
            b"\xAB" +
            b"\x68\xAB\xAB\xAB\xAB" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x83\xAB\xAB" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xAB" +
            b"\xE8")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.loginPollAddr,
                                       self.loginPollAddr + 0x3e)
    if offset is False:
        # 2010-08-17
        # 0  mov ecx, packet_buf_len
        # 5  xor eax, eax
        # 7  mov edi, offset packet_buf
        # 12 rep stosd
        # 14 call CRagConnection_instanceR
        # 19 mov ecx, eax
        # 21 call CConnection_Poll
        lenOffset = 1
        bufOffset = 8
        memsetOffset = 0
        instanceOffset = 15
        pollOffset = 22
        code = (
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xAB\xAB" +
            b"\xBF\xAB\xAB\xAB\xAB" +
            b"\xF3\xAB" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xAB" +
            b"\xE8")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.loginPollAddr,
                                       self.loginPollAddr + 0x3e)
    if offset is False and self.exe.client_date < 20110000:
        # 2010-01-05
        # 0  mov ebx, ecx
        # 2  call CRagConnection_instanceR
        # 7  mov ecx, eax
        # 9  call CConnection_Poll
        lenOffset = 0
        bufOffset = 0
        memsetOffset = 0
        instanceOffset = 3
        pollOffset = 10
        code = (
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xAB" +
            b"\xE8")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.loginPollAddr,
                                       self.loginPollAddr + 0x3e)
    if offset is False:
        self.log("failed searchPacketBuf.")
        if errorExit is True:
            exit(1)
        return False

    if lenOffset > 0:
        self.packetBufLen = self.exe.readUInt(offset + lenOffset)
        self.showVaAddr("packet_buf size", self.packetBufLen)
    else:
        self.packetBufLen = 0
    if bufOffset > 0:
        packetBuf = self.exe.readUInt(offset + bufOffset)
        if self.packetBuf != 0:
            if self.packetBuf != packetBuf:
                self.log("Error: detected wrong packet_buf.")
                if errorExit is True:
                    exit(1)
        else:
            self.packetBuf = packetBuf
    if self.packetBuf != 0:
        if self.packetBufLen != 0:
            self.addVaArray("packet_buf",
                            self.packetBuf,
                            self.packetBufLen,
                            True)
        else:
            self.addVaVar("packet_buf", self.packetBuf)
    if memsetOffset > 0:
        self.memset = self.getAddr(offset, memsetOffset, memsetOffset + 4)
        self.addRawFunc("memset", self.memset)
    else:
        self.memset = 0
    instance = self.getAddr(offset, instanceOffset, instanceOffset + 4)
    if self.instanceR != 0:
        if instance != self.instanceR:
            self.log("Error: detected wrong CRagConnection_instanceR.")
            if errorExit is True:
                exit(1)
            return False
    else:
        self.instanceR = instance
        self.addRawFunc("CRagConnection::instanceR", self.instanceR)
    self.cconnectionPoll = self.getAddr(offset, pollOffset, pollOffset + 4)
    self.addRawFunc("CConnection::Poll", self.cconnectionPoll)
    return True
