#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchCharCreatePacket(self, errorExit):
    # search 0x67/0x290/0xa39 packet in CLoginMode_virt28 (case 10)
    # 2018-05-30
    # 0  movq xmm0, qword ptr g_charInfo.char_name
    # 8  mov eax, 0A39h
    # 13 mov [ebp+a39.packet_id], ax
    # 20 movzx eax, word ptr g_charInfo.hair_color
    # 27 mov [ebp+a39.hair_color], ax
    # 34 movzx eax, g_charInfo.hair_style
    # 41 mov [ebp+a39.hair_style], ax
    # 48 mov al, g_charInfo.slot
    # 53 mov [ebp+a39.slot], al
    # 59 movsx eax, g_charInfo.starting_job_id
    # 66 mov dword ptr [ebp+a39.starting_job_id], eax
    # 72 mov al, g_charInfo.sex
    # 77 movq qword ptr [ebp+a39.name], xmm0
    # 85 movq xmm0, qword ptr g_charInfo.char_name+8
    # 93 mov [ebp+a39.sex], al
    code = (
        b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 0
        b"\xB8\x39\x0A\x00\x00"          # 8
        b"\x66\x89\x85\xAB\xAB\xAB\xFF"  # 13
        b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"  # 20
        b"\x66\x89\x85\xAB\xAB\xAB\xFF"  # 27
        b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"  # 34
        b"\x66\x89\x85\xAB\xAB\xAB\xFF"  # 41
        b"\xA0\xAB\xAB\xAB\xAB"          # 48
        b"\x88\x85\xAB\xAB\xAB\xFF"      # 53
        b"\x0F\xBF\x05\xAB\xAB\xAB\xAB"  # 59
        b"\x89\x85\xAB\xAB\xAB\xFF"      # 66
        b"\xA0\xAB\xAB\xAB\xAB"          # 72
        b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xFF"  # 77
        b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 85
        b"\x88\x85\xAB\xAB\xAB\xFF"      # 93
    )
    # var = (readOffset, size, resultOffset)
    nameOffset1 = (4, 4, 0)
    nameOffset2 = (81, 4, 0)
    packetIdNumOffset = (1 + 8, 4, 0)
    packetIdOffset2 = (8 + 8, 4, 0)
    hairColorOffset1 = (15 + 8, 4, 0)
    hairColorOffset2 = (22 + 8, 4, 0)
    hairStyleOffset1 = (29 + 8, 4, 0)
    hairStyleOffset2 = (36 + 8, 4, 0)
    slotOffset1 = (41 + 8, 4, 0)
    slotOffset2 = (47 + 8, 4, 0)
    startJobIdOffset1 = (54 + 8, 4, 0)
    startJobIdOffset2 = (60 + 8, 4, 0)
    sexOffset1 = (65 + 8, 4, 0)
    sexOffset2 = (87 + 8, 4, 0)
    statsOffset1 = 0
    statsOffset2 = 0
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2019-02-13
        # 0  movups xmm0, xmmword ptr g_charInfo.char_name
        # 7  mov eax, 0A39h
        # 12 mov [ebp+p.packet_id], ax
        # 19 movzx eax, g_charInfo.hair_color
        # 26 mov [ebp+p.hair_color], ax
        # 33 movzx eax, g_charInfo.hair_style
        # 40 mov [ebp+p.hair_style], ax
        # 47 mov al, g_charInfo.slot
        # 52 mov [ebp+p.slot], al
        # 58 movsx eax, g_charInfo.class
        # 65 mov [ebp+p.class], eax
        # 71 mov al, g_charInfo.sex
        # 76 mov [ebp+p.sex], al
        # 82 lea eax, [ebp+p]
        # 88 movups xmmword ptr [ebp+p.name], xmm0
        code = (
            b"\x0F\x10\x05\xAB\xAB\xAB\xAB"    # 0 movups xmm0, xmmword ptr g_c
            b"\xB8\x39\x0A\x00\x00"            # 7 mov eax, 0A39h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 12 mov [ebp+p.packet_id], ax
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 19 movzx eax, g_charInfo.hair_
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 26 mov [ebp+p.hair_color], ax
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 33 movzx eax, g_charInfo.hair_
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 40 mov [ebp+p.hair_style], ax
            b"\xA0\xAB\xAB\xAB\xAB"            # 47 mov al, g_charInfo.slot
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 52 mov [ebp+p.slot], al
            b"\x0F\xBF\x05\xAB\xAB\xAB\xAB"    # 58 movsx eax, g_charInfo.class
            b"\x89\x85\xAB\xAB\xAB\xAB"        # 65 mov [ebp+p.class], eax
            b"\xA0\xAB\xAB\xAB\xAB"            # 71 mov al, g_charInfo.sex
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 76 mov [ebp+p.sex], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 82 lea eax, [ebp+p]
            b"\x0F\x11\x85"                    # 88 movups xmmword ptr [ebp+p.n
        )
        nameOffset1 = (3, 4, 0)
        nameOffset2 = (91, 4, 0)
        packetIdNumOffset = (8, 4, 0)
        packetIdOffset2 = (15, 4, 0)
        hairColorOffset1 = (22, 4, 0)
        hairColorOffset2 = (29, 4, 0)
        hairStyleOffset1 = (36, 4, 0)
        hairStyleOffset2 = (43, 4, 0)
        slotOffset1 = (48, 4, 0)
        slotOffset2 = (54, 4, 0)
        startJobIdOffset1 = (61, 4, 0)
        startJobIdOffset2 = (67, 4, 0)
        sexOffset1 = (72, 4, 0)
        sexOffset2 = (78, 4, 0)
        statsOffset1 = 0
        statsOffset2 = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2015-10-22
        # 0  movq xmm0, qword ptr g_charInfo.char_name
        # 8  mov eax, 970h
        # 13 mov [ebp+var_30.packet_id], ax
        # 17 movzx eax, g_charInfo.hair_color
        # 24 mov [ebp+var_30.hair_color], ax
        # 28 movzx eax, g_charInfo.hair_style
        # 35 mov [ebp+var_30.hair_style], ax
        # 39 mov al, g_charInfo.slot
        # 44 movq qword ptr [ebp+var_30.char_name], xmm0
        # 49 movq xmm0, qword ptr g_charInfo.char_name+8
        # 57 mov [ebp+var_30.slot], al
        code = (
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 0
            b"\xB8\x70\x09\x00\x00"            # 8
            b"\x66\x89\x45\xAB"                # 13
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 17
            b"\x66\x89\x45\xAB"                # 24
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 28
            b"\x66\x89\x45\xAB"                # 35
            b"\xA0\xAB\xAB\xAB\xAB"            # 39
            b"\x66\x0F\xD6\x45\xAB"            # 44
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 49
            b"\x88\x45\xAB"                    # 57
        )
        nameOffset1 = (4, 4, 0)
        nameOffset2 = (48, 1, 0)
        packetIdNumOffset = (9, 4, 0)
        packetIdOffset2 = (16, 1, 0)
        hairColorOffset1 = (20, 4, 0)
        hairColorOffset2 = (27, 1, 0)
        hairStyleOffset1 = (31, 4, 0)
        hairStyleOffset2 = (38, 1, 0)
        slotOffset1 = (40, 4, 0)
        slotOffset2 = (59, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = 0
        statsOffset2 = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2015-06-24
        # 0  mov eax, dword ptr g_charInfo.char_name
        # 5  mov ecx, dword ptr g_charInfo.char_name+4
        # 11 mov dword ptr [ebp+var_30.name], eax
        # 14 mov eax, dword ptr g_charInfo.char_name+0Ch
        # 19 mov edx, 970h
        # 24 mov [ebp+var_30.packet_id], dx
        # 28 mov edx, dword ptr g_charInfo.char_name+8
        # 34 mov dword ptr [ebp+var_30.name+0Ch], eax
        # 37 mov ax, g_charInfo.hair_color
        # 43 mov dword ptr [ebp+var_30.name+4], ecx
        # 46 mov ecx, dword ptr g_charInfo.char_name+10h
        # 52 mov dword ptr [ebp+var_30.name+8], edx
        # 55 mov edx, dword ptr g_charInfo.char_name+14h
        # 61 mov [ebp+var_30.hair_color], ax
        # 65 lea eax, [ebp+var_30]
        # 68 mov dword ptr [ebp+var_30.name+10h], ecx
        # 71 mov cx, g_charInfo.hair_style
        # 78 mov dword ptr [ebp+var_30.name+14h], edx
        # 81 mov dl, g_charInfo.slot
        # 87 push eax
        # 88 push 970h
        # 93 mov [ebp+var_30.hair_style], cx
        # 97 mov [ebp+var_30.slot], dl
        code = (
            b"\xA1\xAB\xAB\xAB\xAB"            # 0
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 5
            b"\x89\x45\xAB"                    # 11
            b"\xA1\xAB\xAB\xAB\xAB"            # 14
            b"\xBA\x70\x09\x00\x00"            # 19
            b"\x66\x89\x55\xAB"                # 24
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 28
            b"\x89\x45\xAB"                    # 34
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 37
            b"\x89\x4D\xAB"                    # 43
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 46
            b"\x89\x55\xAB"                    # 52
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 55
            b"\x66\x89\x45\xAB"                # 61
            b"\x8D\x45\xAB"                    # 65
            b"\x89\x4D\xAB"                    # 68
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 71
            b"\x89\x55\xAB"                    # 78
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 81
            b"\x50"                            # 87
            b"\x68\x70\x09\x00\x00"            # 88
            b"\x66\x89\x4D\xAB"                # 93
            b"\x88\x55\xAB"                    # 97
        )
        nameOffset1 = (1, 4, 0)
        nameOffset2 = (13, 1, 0)
        packetIdNumOffset = (20, 4, 0)
        packetIdOffset2 = (27, 1, 0)
        hairColorOffset1 = (39, 4, 0)
        hairColorOffset2 = (64, 1, 0)
        hairStyleOffset1 = (74, 4, 0)
        hairStyleOffset2 = (96, 1, 0)
        slotOffset1 = (83, 4, 0)
        slotOffset2 = (99, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = 0
        statsOffset2 = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2015-03-18
        # 0  movq xmm0, qword ptr g_charInfo.char_name
        # 8  mov eax, 970h
        # 13 mov [ebp+var_3FC.packet_id], ax
        # 20 movzx eax, g_charInfo.hair_color
        # 27 mov [ebp+var_3FC.hair_color], ax
        # 34 movzx eax, g_charInfo.hair_style
        # 41 mov [ebp+var_3FC.hair_style], ax
        # 48 mov al, g_charInfo.slot
        # 53 movq qword ptr [ebp+var_3FC.name], xmm0
        # 61 movq xmm0, qword ptr g_charInfo.char_name+8
        # 69 mov [ebp+var_3FC.slot], al
        code = (
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 0
            b"\xB8\x70\x09\x00\x00"            # 8
            b"\x66\x89\x85\xAB\xAB\xAB\xFF"    # 13
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 20
            b"\x66\x89\x85\xAB\xAB\xAB\xFF"    # 27
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 34
            b"\x66\x89\x85\xAB\xAB\xAB\xFF"    # 41
            b"\xA0\xAB\xAB\xAB\xAB"            # 48
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xFF"  # 53
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 61
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 69
        )
        nameOffset1 = (4, 4, 0)
        nameOffset2 = (57, 4, 0)
        packetIdNumOffset = (9, 4, 0)
        packetIdOffset2 = (16, 4, 0)
        hairColorOffset1 = (23, 4, 0)
        hairColorOffset2 = (30, 4, 0)
        hairStyleOffset1 = (37, 4, 0)
        hairStyleOffset2 = (44, 4, 0)
        slotOffset1 = (49, 4, 0)
        slotOffset2 = (71, 4, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = 0
        statsOffset2 = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2014-01-08
        # 0  mov edx, dword ptr g_charInfo.char_name
        # 6  mov eax, dword ptr g_charInfo.char_name+4
        # 11 mov dword ptr [ebp+var_30.name], edx
        # 14 mov edx, dword ptr g_charInfo.char_name+0Ch
        # 20 mov ecx, 970h
        # 25 mov [ebp+var_30.packet_id], cx
        # 29 mov ecx, dword ptr g_charInfo.char_name+8
        # 35 mov dword ptr [ebp+var_30.name+0Ch], edx
        # 38 mov dx, g_charInfo.hair_color
        # 45 mov dword ptr [ebp+var_30.name+4], eax
        # 48 mov eax, dword ptr g_charInfo.char_name+10h
        # 53 mov dword ptr [ebp+var_30.name+8], ecx
        # 56 mov ecx, dword ptr g_charInfo.char_name+14h
        # 62 mov [ebp+var_30.hair_color], dx
        # 66 lea edx, [ebp+var_30]
        # 69 mov dword ptr [ebp+var_30.name+10h], eax
        # 72 mov ax, g_charInfo.hair_style
        # 78 mov dword ptr [ebp+var_30.name+14h], ecx
        # 81 mov cl, g_charInfo.slot
        # 87 push edx
        # 88 push 970h
        # 93 mov [ebp+var_30.hair_style], ax
        # 97 mov [ebp+var_30.slot], cl
        code = (
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 0
            b"\xA1\xAB\xAB\xAB\xAB"            # 6
            b"\x89\x55\xAB"                    # 11
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 14
            b"\xB9\x70\x09\x00\x00"            # 20
            b"\x66\x89\x4D\xAB"                # 25
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 29
            b"\x89\x55\xAB"                    # 35
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 38
            b"\x89\x45\xAB"                    # 45
            b"\xA1\xAB\xAB\xAB\xAB"            # 48
            b"\x89\x4D\xAB"                    # 53
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 56
            b"\x66\x89\x55\xAB"                # 62
            b"\x8D\x55\xAB"                    # 66
            b"\x89\x45\xAB"                    # 69
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 72
            b"\x89\x4D\xAB"                    # 78
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 81
            b"\x52"                            # 87
            b"\x68\x70\x09\x00\x00"            # 88
            b"\x66\x89\x45\xAB"                # 93
            b"\x88\x4D\xAB"                    # 97
        )
        nameOffset1 = (2, 4, 0)
        nameOffset2 = (13, 1, 0)
        packetIdNumOffset = (21, 4, 0)
        packetIdOffset2 = (28, 1, 0)
        hairColorOffset1 = (41, 4, 0)
        hairColorOffset2 = (65, 1, 0)
        hairStyleOffset1 = (74, 4, 0)
        hairStyleOffset2 = (96, 1, 0)
        slotOffset1 = (83, 4, 0)
        slotOffset2 = (99, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = 0
        statsOffset2 = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2013-01-03
        # 0  mov edx, dword ptr g_charInfo.name
        # 6  mov eax, dword ptr g_charInfo.name+4
        # 11 mov dword ptr [esp+3ECh+var_284.name], edx
        # 18 mov edx, dword ptr g_charInfo.name+0Ch
        # 24 mov ecx, 970h
        # 29 mov [esp+3ECh+var_284.packet_id], cx
        # 37 mov ecx, dword ptr g_charInfo.name+8
        # 43 mov dword ptr [esp+3ECh+var_284.name+0Ch], edx
        # 50 mov dx, g_charInfo.hair_color
        # 57 mov dword ptr [esp+3ECh+var_284.name+4], eax
        # 64 mov eax, dword ptr g_charInfo.name+10h
        # 69 mov dword ptr [esp+3ECh+var_284.name+8], ecx
        # 76 mov ecx, dword ptr g_charInfo.name+14h
        # 82 mov [esp+3ECh+var_284.hair_color], dx
        # 90 lea edx, [esp+3ECh+var_284]
        # 97 mov dword ptr [esp+3ECh+var_284.name+10h], eax
        # 104 mov ax, g_charInfo.hair_style
        # 110 mov dword ptr [esp+3ECh+var_284.name+14h], ecx
        # 117 mov cl, g_charInfo.slot
        # 123 push edx
        # 124 push 970h
        # 129 mov [esp+3F4h+var_284.hair_style], ax
        # 137 mov [esp+3F4h+var_284.slot], cl
        code = (
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 0
            b"\xA1\xAB\xAB\xAB\xAB"            # 6
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"    # 11
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 18
            b"\xB9\x70\x09\x00\x00"            # 24
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 29
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 37
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"    # 43
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 50
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"    # 57
            b"\xA1\xAB\xAB\xAB\xAB"            # 64
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"    # 69
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 76
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 82
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"    # 90
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"    # 97
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 104
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"    # 110
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 117
            b"\x52"                            # 123
            b"\x68\x70\x09\x00\x00"            # 124
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 129
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 137
        )
        nameOffset1 = (2, 4, 0)
        nameOffset2 = (14, 4, 0)
        packetIdNumOffset = (25, 4, 0)
        packetIdOffset2 = (33, 4, 0)
        hairColorOffset1 = (53, 4, 0)
        hairColorOffset2 = (86, 4, 0)
        hairStyleOffset1 = (106, 4, 0)
        hairStyleOffset2 = (133, 4, -8)
        slotOffset1 = (119, 4, 0)
        slotOffset2 = (140, 4, -8)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = 0
        statsOffset2 = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2012-01-03
        # 0  mov edx, dword ptr g_charInfo.char_name
        # 6  mov eax, dword ptr g_charInfo.char_name+4
        # 11 mov dword ptr [esp+44Ch+char_name], edx
        # 15 mov edx, dword ptr g_charInfo.char_name+0Ch
        # 21 mov dword ptr [esp+44Ch+char_name+0Ch], edx
        # 25 movzx edx, g_charInfo.stat1
        # 32 mov ecx, 67h
        # 37 mov [esp+44Ch+Src], cx
        # 42 mov ecx, dword ptr g_charInfo.char_name+8
        # 48 mov dword ptr [esp+44Ch+char_name+4], eax
        # 52 mov eax, dword ptr g_charInfo.char_name+10h
        # 57 mov dword ptr [esp+44Ch+char_name+8], ecx
        # 61 mov ecx, dword ptr g_charInfo.char_name+14h
        # 67 mov byte ptr [esp+44Ch+stat1_4], dl
        # 71 movzx edx, g_charInfo.stat4
        # 78 mov dword ptr [esp+44Ch+char_name+10h], eax
        # 82 movzx eax, g_charInfo.stat2
        # 89 mov dword ptr [esp+44Ch+char_name+14h], ecx
        # 93 movzx ecx, g_charInfo.stat3
        # 100 mov byte ptr [esp+44Ch+stat1_4+3], dl
        # 107 movzx edx, byte ptr g_charInfo.slot
        # 114 mov byte ptr [esp+44Ch+stat1_4+1], al
        # 118 movzx eax, g_charInfo.stat5
        # 125 mov byte ptr [esp+44Ch+stat1_4+2], cl
        # 132 movzx ecx, byte ptr g_charInfo.stat6
        # 139 mov byte ptr [esp+44Ch+stat5_6_hair+2], dl
        # 146 lea edx, [esp+44Ch+Src]
        # 150 mov byte ptr [esp+44Ch+stat5_6_hair], al
        # 157 mov ax, g_charInfo.hair_color
        # 163 mov byte ptr [esp+44Ch+stat5_6_hair+1], cl
        # 170 mov cx, g_charInfo.hair_style
        # 177 push edx
        # 178 push 67h
        # 180 mov word ptr [esp+454h+stat5_6_hair+3], ax
        # 188 mov word ptr [esp+454h+stat5_6_hair+5], cx
        code = (
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 0
            b"\xA1\xAB\xAB\xAB\xAB"            # 6
            b"\x89\x54\x24\xAB"                # 11
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 15
            b"\x89\x54\x24\xAB"                # 21
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 25
            b"\xB9\x67\x00\x00\x00"            # 32
            b"\x66\x89\x4C\x24\xAB"            # 37
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 42
            b"\x89\x44\x24\xAB"                # 48
            b"\xA1\xAB\xAB\xAB\xAB"            # 52
            b"\x89\x4C\x24\xAB"                # 57
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 61
            b"\x88\x54\x24\xAB"                # 67
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 71
            b"\x89\x44\x24\xAB"                # 78
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 82
            b"\x89\x4C\x24\xAB"                # 89
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 93
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 100
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 107
            b"\x88\x44\x24\xAB"                # 114
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 118
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 125
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 132
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 139
            b"\x8D\x54\x24\xAB"                # 146
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 150
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 157
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 163
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 170
            b"\x52"                            # 177
            b"\x6A\x67"                        # 178
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 180
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 188
        )
        nameOffset1 = (2, 4, 0)
        nameOffset2 = (14, 1, 0)
        packetIdNumOffset = (33, 4, 0)
        packetIdOffset2 = (41, 1, 0)
        hairColorOffset1 = (159, 4, 0)
        hairColorOffset2 = (184, 4, -8)
        hairStyleOffset1 = (173, 4, 0)
        hairStyleOffset2 = (192, 4, -8)
        slotOffset1 = (110, 4, 0)
        slotOffset2 = (142, 4, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (28, 4, 0)
        statsOffset2 = (70, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2012-01-03 re
        # 0  mov edx, dword ptr g_charInfo.name
        # 6  mov eax, dword ptr g_charInfo.name+4
        # 11 mov dword ptr [esp+3F0h+Src.name], edx
        # 15 mov edx, dword ptr g_charInfo.name+0Ch
        # 21 mov dword ptr [esp+3F0h+Src.name+0Ch], edx
        # 25 movzx edx, g_charInfo.str
        # 32 mov ecx, 67h
        # 37 mov [esp+3F0h+Src.packet_id], cx
        # 42 mov ecx, dword ptr g_charInfo.name+8
        # 48 mov dword ptr [esp+3F0h+Src.name+4], eax
        # 52 mov eax, dword ptr g_charInfo.name+10h
        # 57 mov dword ptr [esp+3F0h+Src.name+8], ecx
        # 61 mov ecx, dword ptr g_charInfo.name+14h
        # 67 mov [esp+3F0h+Src.str], dl
        # 71 movzx edx, g_charInfo.int_
        # 78 mov dword ptr [esp+3F0h+Src.name+10h], eax
        # 82 movzx eax, g_charInfo.agi
        # 89 mov dword ptr [esp+3F0h+Src.name+14h], ecx
        # 93 movzx ecx, g_charInfo.vit
        # 100 mov [esp+3F0h+Src.int_], dl
        # 104 movzx edx, g_charInfo.slot
        # 111 mov [esp+3F0h+Src.agi], al
        # 115 movzx eax, g_charInfo.dex
        # 122 mov [esp+3F0h+Src.vit], cl
        # 126 movzx ecx, g_charInfo.luk
        # 133 mov [esp+3F0h+Src.slot], dl
        # 137 lea edx, [esp+3F0h+Src]
        # 141 mov [esp+3F0h+Src.dex], al
        # 145 mov ax, g_charInfo.hair_color
        # 151 mov [esp+3F0h+Src.luk], cl
        # 155 mov cx, g_charInfo.hair_style
        # 162 push edx
        # 163 push 67h
        # 165 mov [esp+3F8h+Src.hair_color], ax
        # 170 mov [esp+3F8h+Src.hair_style], cx
        code = (
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 0
            b"\xA1\xAB\xAB\xAB\xAB"            # 6
            b"\x89\x54\x24\xAB"                # 11
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 15
            b"\x89\x54\x24\xAB"                # 21
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 25
            b"\xB9\x67\x00\x00\x00"            # 32
            b"\x66\x89\x4C\x24\xAB"            # 37
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 42
            b"\x89\x44\x24\xAB"                # 48
            b"\xA1\xAB\xAB\xAB\xAB"            # 52
            b"\x89\x4C\x24\xAB"                # 57
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 61
            b"\x88\x54\x24\xAB"                # 67
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 71
            b"\x89\x44\x24\xAB"                # 78
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 82
            b"\x89\x4C\x24\xAB"                # 89
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 93
            b"\x88\x54\x24\xAB"                # 100
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 104
            b"\x88\x44\x24\xAB"                # 111
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 115
            b"\x88\x4C\x24\xAB"                # 122
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 126
            b"\x88\x54\x24\xAB"                # 133
            b"\x8D\x54\x24\xAB"                # 137
            b"\x88\x44\x24\xAB"                # 141
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 145
            b"\x88\x4C\x24\xAB"                # 151
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 155
            b"\x52"                            # 162
            b"\x6A\x67"                        # 163
            b"\x66\x89\x44\x24\xAB"            # 165
            b"\x66\x89\x4C\x24\xAB"            # 170
        )
        nameOffset1 = (2, 4, 0)
        nameOffset2 = (14, 1, 0)
        packetIdNumOffset = (33, 4, 0)
        packetIdOffset2 = (41, 1, 0)
        hairColorOffset1 = (147, 4, 0)
        hairColorOffset2 = (169, 1, -8)
        hairStyleOffset1 = (158, 4, 0)
        hairStyleOffset2 = (174, 1, -8)
        slotOffset1 = (107, 4, 0)
        slotOffset2 = (136, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (28, 4, 0)
        statsOffset2 = (70, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-01-04
        # 0  mov eax, dword ptr g_charInfo.char_name
        # 5  mov ecx, dword ptr g_charInfo.char_name+4
        # 11 mov dword ptr [esp+498h+Dst.name], eax
        # 15 mov eax, dword ptr g_charInfo.char_name+0Ch
        # 20 mov dword ptr [esp+498h+Dst.name+0Ch], eax
        # 24 movzx eax, g_charInfo.str
        # 31 mov edx, 67h
        # 36 mov [esp+498h+Dst.packet_id], dx
        # 41 mov edx, dword ptr g_charInfo.char_name+8
        # 47 mov dword ptr [esp+498h+Dst.name+4], ecx
        # 51 mov ecx, dword ptr g_charInfo.char_name+10h
        # 57 mov dword ptr [esp+498h+Dst.name+8], edx
        # 61 mov edx, dword ptr g_charInfo.char_name+14h
        # 67 mov [esp+498h+Dst.str], al
        # 74 movzx eax, g_charInfo.int_
        # 81 mov dword ptr [esp+498h+Dst.name+10h], ecx
        # 88 movzx ecx, g_charInfo.agi
        # 95 mov dword ptr [esp+498h+Dst.name+14h], edx
        # 102 movzx edx, g_charInfo.vit
        # 109 mov [esp+498h+Dst.int_], al
        # 116 movzx eax, g_charInfo.slot
        # 123 mov [esp+498h+Dst.agi], cl
        # 130 movzx ecx, g_charInfo.dex
        # 137 mov [esp+498h+Dst.vit], dl
        # 144 movzx edx, g_charInfo.luk
        # 151 mov [esp+498h+Dst.slot], al
        # 158 lea eax, [esp+498h+Dst]
        # 162 mov [esp+498h+Dst.dex], cl
        # 169 mov cx, g_charInfo.hair_color
        # 176 mov [esp+498h+Dst.luk], dl
        # 183 mov dx, g_charInfo.hair_style
        # 190 push eax
        # 191 push 67h
        # 193 mov [esp+4A0h+Dst.hair_color], cx
        # 201 mov [esp+4A0h+Dst.hair_style], dx
        code = (
            b"\xA1\xAB\xAB\xAB\xAB"            # 0
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 5
            b"\x89\x44\x24\xAB"                # 11
            b"\xA1\xAB\xAB\xAB\xAB"            # 15
            b"\x89\x44\x24\xAB"                # 20
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 24
            b"\xBA\x67\x00\x00\x00"            # 31
            b"\x66\x89\x54\x24\xAB"            # 36
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 41
            b"\x89\x4C\x24\xAB"                # 47
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 51
            b"\x89\x54\x24\xAB"                # 57
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 61
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 67
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 74
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"    # 81
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 88
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"    # 95
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 102
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 109
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 116
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 123
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 130
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 137
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 144
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 151
            b"\x8D\x44\x24\xAB"                # 158
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 162
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 169
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 176
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 183
            b"\x50"                            # 190
            b"\x6A\x67"                        # 191
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 193
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 201
        )
        nameOffset1 = (1, 4, 0)
        nameOffset2 = (14, 1, 0)
        packetIdNumOffset = (32, 4, 0)
        packetIdOffset2 = (40, 1, 0)
        hairColorOffset1 = (172, 4, 0)
        hairColorOffset2 = (197, 4, -8)
        hairStyleOffset1 = (186, 4, 0)
        hairStyleOffset2 = (205, 4, -8)
        slotOffset1 = (119, 4, 0)
        slotOffset2 = (154, 4, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (27, 4, 0)
        statsOffset2 = (70, 4, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-01-04 re
        # 0  mov eax, dword ptr g_charInfo.name
        # 5  mov ecx, dword ptr g_charInfo.name+4
        # 11 mov dword ptr [esp+5370h+buf.name], eax
        # 18 mov eax, dword ptr g_charInfo.name+0Ch
        # 23 mov dword ptr [esp+5370h+buf.name+0Ch], eax
        # 30 movzx eax, g_charInfo.str
        # 37 mov edx, 67h
        # 42 mov [esp+5370h+buf.packet_id], dx
        # 50 mov edx, dword ptr g_charInfo.name+8
        # 56 mov dword ptr [esp+5370h+buf.name+4], ecx
        # 63 mov ecx, dword ptr g_charInfo.name+10h
        # 69 mov dword ptr [esp+5370h+buf.name+8], edx
        # 76 mov edx, dword ptr g_charInfo.name+14h
        # 82 mov [esp+5370h+buf.str], al
        # 89 movzx eax, g_charInfo.int_
        # 96 mov dword ptr [esp+5370h+buf.name+10h], ecx
        # 103 movzx ecx, g_charInfo.agi
        # 110 mov dword ptr [esp+5370h+buf.name+14h], edx
        # 117 movzx edx, g_charInfo.vit
        # 124 mov [esp+5370h+buf.int_], al
        # 131 movzx eax, g_charInfo.slot
        # 138 mov [esp+5370h+buf.agi], cl
        # 145 movzx ecx, g_charInfo.dex
        # 152 mov [esp+5370h+buf.vit], dl
        # 159 movzx edx, g_charInfo.luk
        # 166 mov [esp+5370h+buf.slot], al
        # 173 lea eax, [esp+5370h+buf]
        # 180 mov [esp+5370h+buf.dex], cl
        # 187 mov cx, g_charInfo.hair_color
        # 194 mov [esp+5370h+buf.luk], dl
        # 201 mov dx, g_charInfo.hair_style
        # 208 push eax
        # 209 push 67h
        # 211 mov [esp+5378h+buf.hair_color], cx
        # 219 mov [esp+5378h+buf.hair_style], dx
        code = (
            b"\xA1\xAB\xAB\xAB\xAB"            # 0
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 5
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"    # 11
            b"\xA1\xAB\xAB\xAB\xAB"            # 18
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"    # 23
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 30
            b"\xBA\x67\x00\x00\x00"            # 37
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 42
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 50
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"    # 56
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 63
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"    # 69
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 76
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 82
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 89
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"    # 96
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 103
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"    # 110
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 117
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 124
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 131
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 138
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 145
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 152
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 159
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 166
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"    # 173
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 180
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 187
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 194
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 201
            b"\x50"                            # 208
            b"\x6A\x67"                        # 209
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 211
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 219
        )
        nameOffset1 = (1, 4, 0)
        nameOffset2 = (14, 4, 0)
        packetIdNumOffset = (38, 4, 0)
        packetIdOffset2 = (46, 4, 0)
        hairColorOffset1 = (190, 4, 0)
        hairColorOffset2 = (215, 4, -8)
        hairStyleOffset1 = (204, 4, 0)
        hairStyleOffset2 = (223, 4, -8)
        slotOffset1 = (134, 4, 0)
        slotOffset2 = (169, 4, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (33, 4, 0)
        statsOffset2 = (85, 4, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2011-03-08
        # 0  mov eax, dword ptr g_charInfo.char_name
        # 5  mov ecx, dword ptr g_charInfo.char_name+4
        # 11 mov dword ptr [esp+44Ch+Dst.name], eax
        # 15 mov eax, dword ptr g_charInfo.char_name+0Ch
        # 20 mov dword ptr [esp+44Ch+Dst.name+0Ch], eax
        # 24 movzx eax, g_charInfo.str
        # 31 mov edx, 67h
        # 36 mov [esp+44Ch+Dst.packet_id], dx
        # 41 mov edx, dword ptr g_charInfo.char_name+8
        # 47 mov dword ptr [esp+44Ch+Dst.name+4], ecx
        # 51 mov ecx, dword ptr g_charInfo.char_name+10h
        # 57 mov dword ptr [esp+44Ch+Dst.name+8], edx
        # 61 mov edx, dword ptr g_charInfo.char_name+14h
        # 67 mov [esp+44Ch+Dst.str], al
        # 71 movzx eax, g_charInfo.int_
        # 78 mov dword ptr [esp+44Ch+Dst.name+10h], ecx
        # 82 movzx ecx, g_charInfo.agi
        # 89 mov dword ptr [esp+44Ch+Dst.name+14h], edx
        # 93 movzx edx, g_charInfo.vit
        # 100 mov [esp+44Ch+Dst.int_], al
        # 107 movzx eax, g_charInfo.slot
        # 114 mov [esp+44Ch+Dst.agi], cl
        # 118 movzx ecx, g_charInfo.dex
        # 125 mov [esp+44Ch+Dst.vit], dl
        # 132 movzx edx, g_charInfo.luk
        # 139 mov [esp+44Ch+Dst.slot], al
        # 146 lea eax, [esp+44Ch+Dst]
        # 150 mov [esp+44Ch+Dst.dex], cl
        # 157 mov cx, g_charInfo.hair_color
        # 164 mov [esp+44Ch+Dst.luk], dl
        # 171 mov dx, g_charInfo.hair_style
        # 178 push eax
        # 179 push 67h
        # 181 mov [esp+454h+Dst.hair_color], cx
        # 189 mov [esp+454h+Dst.hair_style], dx
        code = (
            b"\xA1\xAB\xAB\xAB\xAB"            # 0
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 5
            b"\x89\x44\x24\xAB"                # 11
            b"\xA1\xAB\xAB\xAB\xAB"            # 15
            b"\x89\x44\x24\xAB"                # 20
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 24
            b"\xBA\x67\x00\x00\x00"            # 31
            b"\x66\x89\x54\x24\xAB"            # 36
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 41
            b"\x89\x4C\x24\xAB"                # 47
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 51
            b"\x89\x54\x24\xAB"                # 57
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 61
            b"\x88\x44\x24\xAB"                # 67
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 71
            b"\x89\x4C\x24\xAB"                # 78
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 82
            b"\x89\x54\x24\xAB"                # 89
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 93
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 100
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 107
            b"\x88\x4C\x24\xAB"                # 114
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 118
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 125
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 132
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 139
            b"\x8D\x44\x24\xAB"                # 146
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 150
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 157
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 164
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 171
            b"\x50"                            # 178
            b"\x6A\x67"                        # 179
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 181
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 189
        )
        nameOffset1 = (1, 4, 0)
        nameOffset2 = (14, 1, 0)
        packetIdNumOffset = (32, 4, 0)
        packetIdOffset2 = (40, 1, 0)
        hairColorOffset1 = (160, 4, 0)
        hairColorOffset2 = (185, 4, -8)
        hairStyleOffset1 = (174, 4, 0)
        hairStyleOffset2 = (193, 4, -8)
        slotOffset1 = (110, 4, 0)
        slotOffset2 = (142, 4, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (27, 4, 0)
        statsOffset2 = (70, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2010-01-05
        # 0  mov dl, g_charInfo.str
        # 6  mov al, g_charInfo.agi
        # 11 mov ecx, 6
        # 16 mov esi, offset g_charInfo.char_name
        # 21 lea edi, [ebp+var_98.name]
        # 27 mov [ebp+var_98.packet_id], 67h
        # 36 rep movsd
        # 38 mov cl, g_charInfo.vit
        # 44 mov [ebp+var_98.str], dl
        # 47 mov dl, g_charInfo.int_
        # 53 mov [ebp+var_98.agi], al
        # 56 mov al, g_charInfo.dex
        # 61 mov [ebp+var_98.int_], dl
        # 64 mov dl, g_charInfo.slot
        # 70 mov [ebp+var_98.vit], cl
        # 73 mov cl, g_charInfo.luk
        # 79 mov [ebp+var_98.slot], dl
        # 82 lea edx, [ebp+var_98]
        # 88 mov [ebp+var_98.dex], al
        # 91 mov ax, g_charInfo.hair_color
        # 97 mov [ebp+var_98.luk], cl
        # 100 mov cx, g_charInfo.hair_style
        # 107 push edx
        # 108 push 67h
        # 110 mov [ebp+var_98.hair_color], ax
        # 114 mov [ebp+var_98.hair_style], cx
        code = (
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 0
            b"\xA0\xAB\xAB\xAB\xAB"            # 6
            b"\xB9\xAB\x00\x00\x00"            # 11
            b"\xBE\xAB\xAB\xAB\xAB"            # 16
            b"\x8D\xBD\xAB\xAB\xAB\xAB"        # 21
            b"\x66\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB"  # 27
            b"\xF3\xA5"                        # 36
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 38
            b"\x88\x55\xAB"                    # 44
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 47
            b"\x88\x45\xAB"                    # 53
            b"\xA0\xAB\xAB\xAB\xAB"            # 56
            b"\x88\x55\xAB"                    # 61
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 64
            b"\x88\x4D\xAB"                    # 70
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 73
            b"\x88\x55\xAB"                    # 79
            b"\x8D\x95\xAB\xAB\xAB\xAB"        # 82
            b"\x88\x45\xAB"                    # 88
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 91
            b"\x88\x4D\xAB"                    # 97
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 100
            b"\x52"                            # 107
            b"\x6A\x67"                        # 108
            b"\x66\x89\x45\xAB"                # 110
            b"\x66\x89\x4D\xAB"                # 114
        )
        nameOffset1 = (17, 4, 0)
        nameOffset2 = (23, 4, 0)
        packetIdNumOffset = (34, 2, 0)
        packetIdOffset2 = (30, 4, 0)
        hairColorOffset1 = (93, 4, 0)
        hairColorOffset2 = (113, 1, 0)
        hairStyleOffset1 = (103, 4, 0)
        hairStyleOffset2 = (117, 1, 0)
        slotOffset1 = (66, 4, 0)
        slotOffset2 = (81, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (2, 4, 0)
        statsOffset2 = (46, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2010-08-03
        # 0  mov dl, g_charInfo.str
        # 6  mov al, g_charInfo.agi
        # 11 mov ecx, 6
        # 16 mov esi, offset g_charInfo.char_name
        # 21 lea edi, [ebp+var_58.name]
        # 24 mov [ebp+var_58.packet_id], 67h
        # 30 rep movsd
        # 32 mov cl, g_charInfo.vit
        # 38 mov [ebp+var_58.str], dl
        # 41 mov dl, g_charInfo.int_
        # 47 mov [ebp+var_58.agi], al
        # 50 mov al, g_charInfo.dex
        # 55 mov [ebp+var_58.int_], dl
        # 58 mov dl, g_charInfo.slot
        # 64 mov [ebp+var_58.vit], cl
        # 67 mov cl, g_charInfo.luk
        # 73 mov [ebp+var_58.slot], dl
        # 76 lea edx, [ebp+var_58]
        # 79 mov [ebp+var_58.dex], al
        # 82 mov ax, g_charInfo.hair_color
        # 88 mov [ebp+var_58.luk], cl
        # 91 mov cx, g_charInfo.hair_style
        # 98 push edx
        # 99 push 67h
        # 101 mov [ebp+var_58.hair_color], ax
        # 105 mov [ebp+var_58.hair_style], cx
        code = (
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 0
            b"\xA0\xAB\xAB\xAB\xAB"            # 6
            b"\xB9\xAB\x00\x00\x00"            # 11
            b"\xBE\xAB\xAB\xAB\xAB"            # 16
            b"\x8D\x7D\xAB"                    # 21
            b"\x66\xC7\x45\xAB\xAB\xAB"        # 24
            b"\xF3\xAB"                        # 30
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 32
            b"\x88\x55\xAB"                    # 38
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 41
            b"\x88\x45\xAB"                    # 47
            b"\xA0\xAB\xAB\xAB\xAB"            # 50
            b"\x88\x55\xAB"                    # 55
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 58
            b"\x88\x4D\xAB"                    # 64
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 67
            b"\x88\x55\xAB"                    # 73
            b"\x8D\x55\xAB"                    # 76
            b"\x88\x45\xAB"                    # 79
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 82
            b"\x88\x4D\xAB"                    # 88
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 91
            b"\x52"                            # 98
            b"\x6A\x67"                        # 99
            b"\x66\x89\x45\xAB"                # 101
            b"\x66\x89\x4D\xAB"                # 105
        )
        nameOffset1 = (17, 4, 0)
        nameOffset2 = (23, 1, 0)
        packetIdNumOffset = (28, 2, 0)
        packetIdOffset2 = (27, 1, 0)
        hairColorOffset1 = (84, 4, 0)
        hairColorOffset2 = (104, 1, 0)
        hairStyleOffset1 = (94, 4, 0)
        hairStyleOffset2 = (108, 1, 0)
        slotOffset1 = (60, 4, 0)
        slotOffset2 = (75, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (2, 4, 0)
        statsOffset2 = (40, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2010-10-05
        # 0  mov eax, dword ptr g_charInfo.char_name
        # 5  mov ecx, dword ptr g_charInfo.char_name+4
        # 11 mov dword ptr [esp+494h+buf.name], eax
        # 15 mov eax, dword ptr g_charInfo.char_name+0Ch
        # 20 mov dword ptr [esp+494h+buf.name+0Ch], eax
        # 24 movzx eax, g_charInfo.str
        # 31 mov edx, 67h
        # 36 mov [esp+494h+buf.packet_id], dx
        # 41 mov edx, dword ptr g_charInfo.char_name+8
        # 47 mov dword ptr [esp+494h+buf.name+4], ecx
        # 51 mov ecx, dword ptr g_charInfo.char_name+10h
        # 57 mov dword ptr [esp+494h+buf.name+8], edx
        # 61 mov edx, dword ptr g_charInfo.char_name+14h
        # 67 mov [esp+494h+buf.str], al
        # 74 movzx eax, g_charInfo.int_
        # 81 mov dword ptr [esp+494h+buf.name+10h], ecx
        # 85 movzx ecx, g_charInfo.agi
        # 92 mov dword ptr [esp+494h+buf.name+14h], edx
        # 99 movzx edx, g_charInfo.vit
        # 106 mov [esp+494h+buf.int_], al
        # 113 movzx eax, g_charInfo.slot
        # 120 mov [esp+494h+buf.agi], cl
        # 127 movzx ecx, g_charInfo.dex
        # 134 mov [esp+494h+buf.vit], dl
        # 141 movzx edx, g_charInfo.luk
        # 148 mov [esp+494h+buf.slot], al
        # 155 lea eax, [esp+494h+buf]
        # 159 mov [esp+494h+buf.dex], cl
        # 166 mov cx, g_charInfo.hair_color
        # 173 mov [esp+494h+buf.luk], dl
        # 180 mov dx, g_charInfo.hair_style
        # 187 push eax
        # 188 push 67h
        # 190 mov [esp+49Ch+buf.hair_color], cx
        # 198 mov [esp+49Ch+buf.hair_style], dx
        code = (
            b"\xA1\xAB\xAB\xAB\xAB"            # 0
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 5
            b"\x89\x44\x24\xAB"                # 11
            b"\xA1\xAB\xAB\xAB\xAB"            # 15
            b"\x89\x44\x24\xAB"                # 20
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 24
            b"\xBA\x67\x00\x00\x00"            # 31
            b"\x66\x89\x54\x24\xAB"            # 36
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 41
            b"\x89\x4C\x24\xAB"                # 47
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 51
            b"\x89\x54\x24\xAB"                # 57
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 61
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 67
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 74
            b"\x89\x4C\x24\xAB"                # 81
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 85
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"    # 92
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 99
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 106
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 113
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 120
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 127
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 134
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 141
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 148
            b"\x8D\x44\x24\xAB"                # 155
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 159
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 166
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 173
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 180
            b"\x50"                            # 187
            b"\x6A\x67"                        # 188
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 190
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 198
        )
        nameOffset1 = (1, 4, 0)
        nameOffset2 = (14, 1, 0)
        packetIdNumOffset = (32, 4, 0)
        packetIdOffset2 = (40, 1, 0)
        hairColorOffset1 = (169, 4, 0)
        hairColorOffset2 = (194, 4, -8)
        hairStyleOffset1 = (183, 4, 0)
        hairStyleOffset2 = (202, 4, -8)
        slotOffset1 = (116, 4, 0)
        slotOffset2 = (151, 4, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (27, 4, 0)
        statsOffset2 = (70, 4, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2010-10-19
        # 0  mov ecx, dword ptr g_charInfo.char_name
        # 6  mov edx, dword ptr g_charInfo.char_name+4
        # 12 mov dword ptr [esp+370h+name.name], ecx
        # 19 mov ecx, dword ptr g_charInfo.char_name+0Ch
        # 25 mov dword ptr [esp+370h+name.name+0Ch], ecx
        # 32 movzx ecx, g_charInfo.str
        # 39 mov eax, 67h
        # 44 mov [esp+370h+name.packet_id], ax
        # 52 mov eax, dword ptr g_charInfo.char_name+8
        # 57 mov dword ptr [esp+370h+name.name+4], edx
        # 64 mov edx, dword ptr g_charInfo.char_name+10h
        # 70 mov dword ptr [esp+370h+name.name+8], eax
        # 77 mov eax, dword ptr g_charInfo.char_name+14h
        # 82 mov [esp+370h+name.str], cl
        # 89 movzx ecx, g_charInfo.int_
        # 96 mov dword ptr [esp+370h+name.name+10h], edx
        # 103 movzx edx, g_charInfo.agi
        # 110 mov dword ptr [esp+370h+name.name+14h], eax
        # 117 movzx eax, g_charInfo.vit
        # 124 mov [esp+370h+name.int_], cl
        # 131 movzx ecx, g_charInfo.slot
        # 138 mov [esp+370h+name.agi], dl
        # 145 movzx edx, g_charInfo.dex
        # 152 mov [esp+370h+name.vit], al
        # 159 movzx eax, g_charInfo.luk
        # 166 mov [esp+370h+name.slot], cl
        # 173 lea ecx, [esp+370h+name]
        # 180 mov [esp+370h+name.dex], dl
        # 187 mov dx, g_charInfo.hair_color
        # 194 mov [esp+370h+name.luk], al
        # 201 mov ax, g_charInfo.hair_style
        # 207 push ecx
        # 208 push 67h
        # 210 mov [esp+378h+name.hair_color], dx
        # 218 mov [esp+378h+name.hair_style], ax
        code = (
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 0
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 6
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"    # 12
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 19
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"    # 25
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 32
            b"\xB8\x67\x00\x00\x00"            # 39
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 44
            b"\xA1\xAB\xAB\xAB\xAB"            # 52
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"    # 57
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 64
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"    # 70
            b"\xA1\xAB\xAB\xAB\xAB"            # 77
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 82
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 89
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"    # 96
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 103
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"    # 110
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 117
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 124
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 131
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 138
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 145
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 152
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 159
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"    # 166
            b"\x8D\x8C\x24\xAB\xAB\xAB\xAB"    # 173
            b"\x88\x94\x24\xAB\xAB\xAB\xAB"    # 180
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 187
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"    # 194
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 201
            b"\x51"                            # 207
            b"\x6A\x67"                        # 208
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 210
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 218
        )
        nameOffset1 = (2, 4, 0)
        nameOffset2 = (15, 4, 0)
        packetIdNumOffset = (40, 4, 0)
        packetIdOffset2 = (48, 4, 0)
        hairColorOffset1 = (190, 4, 0)
        hairColorOffset2 = (214, 4, -8)
        hairStyleOffset1 = (203, 4, 0)
        hairStyleOffset2 = (222, 4, -8)
        slotOffset1 = (134, 4, 0)
        slotOffset2 = (169, 4, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (35, 4, 0)
        statsOffset2 = (85, 4, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2009-01-13
        # 0  mov eax, dword ptr g_charInfo.char_name
        # 5  mov ecx, dword ptr g_charInfo.char_name+4
        # 11 mov dword ptr [esp+164h+Src.name], eax
        # 15 mov eax, dword ptr g_charInfo.char_name+0Ch
        # 20 mov dword ptr [esp+164h+Src.name+0Ch], eax
        # 24 movzx eax, g_charInfo.str
        # 31 mov edx, 67h
        # 36 mov [esp+164h+Src.packet_id], dx
        # 41 mov edx, dword ptr g_charInfo.char_name+8
        # 47 mov dword ptr [esp+164h+Src.name+4], ecx
        # 51 mov ecx, dword ptr g_charInfo.char_name+10h
        # 57 mov dword ptr [esp+164h+Src.name+8], edx
        # 61 mov edx, dword ptr g_charInfo.char_name+14h
        # 67 mov [esp+164h+Src.str], al
        # 71 movzx eax, g_charInfo.int_
        # 78 mov dword ptr [esp+164h+Src.name+10h], ecx
        # 82 movzx ecx, g_charInfo.agi
        # 89 mov dword ptr [esp+164h+Src.name+14h], edx
        # 93 movzx edx, g_charInfo.vit
        # 100 mov [esp+164h+Src.int_], al
        # 104 movzx eax, g_charInfo.slot
        # 111 mov [esp+164h+Src.agi], cl
        # 115 movzx ecx, g_charInfo.dex
        # 122 mov [esp+164h+Src.vit], dl
        # 126 movzx edx, g_charInfo.luk
        # 133 mov [esp+164h+Src.slot], al
        # 137 lea eax, [esp+164h+Src]
        # 141 mov [esp+164h+Src.dex], cl
        # 145 mov cx, g_charInfo.hair_color
        # 152 mov [esp+164h+Src.luk], dl
        # 156 mov dx, g_charInfo.hair_style
        # 163 push eax
        # 164 push 67h
        # 166 mov [esp+16Ch+Src.hair_color], cx
        # 171 mov [esp+16Ch+Src.hair_style], dx
        code = (
            b"\xA1\xAB\xAB\xAB\xAB"            # 0
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 5
            b"\x89\x44\x24\xAB"                # 11
            b"\xA1\xAB\xAB\xAB\xAB"            # 15
            b"\x89\x44\x24\xAB"                # 20
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 24
            b"\xBA\x67\x00\x00\x00"            # 31
            b"\x66\x89\x54\x24\xAB"            # 36
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 41
            b"\x89\x4C\x24\xAB"                # 47
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 51
            b"\x89\x54\x24\xAB"                # 57
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 61
            b"\x88\x44\x24\xAB"                # 67
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 71
            b"\x89\x4C\x24\xAB"                # 78
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 82
            b"\x89\x54\x24\xAB"                # 89
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 93
            b"\x88\x44\x24\xAB"                # 100
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 104
            b"\x88\x4C\x24\xAB"                # 111
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 115
            b"\x88\x54\x24\xAB"                # 122
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 126
            b"\x88\x44\x24\xAB"                # 133
            b"\x8D\x44\x24\xAB"                # 137
            b"\x88\x4C\x24\xAB"                # 141
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 145
            b"\x88\x54\x24\xAB"                # 152
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 156
            b"\x50"                            # 163
            b"\x6A\x67"                        # 164
            b"\x66\x89\x4C\x24\xAB"            # 166
            b"\x66\x89\x54\x24\xAB"            # 171
        )
        nameOffset1 = (1, 4, 0)
        nameOffset2 = (14, 1, 0)
        packetIdNumOffset = (32, 4, 0)
        packetIdOffset2 = (40, 1, 0)
        hairColorOffset1 = (148, 4, 0)
        hairColorOffset2 = (170, 1, -8)
        hairStyleOffset1 = (159, 4, 0)
        hairStyleOffset2 = (175, 1, -8)
        slotOffset1 = (107, 4, 0)
        slotOffset2 = (136, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (27, 4, 0)
        statsOffset2 = (70, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2006-01-02
        # 0  mov dl, g_charInfo.agi
        # 6  mov al, g_charInfo.vit
        # 11 mov ecx, 6
        # 16 mov esi, offset g_charInfo.char_name
        # 21 lea edi, [ebp+var_40.name]
        # 24 mov [ebp+var_40.packet_id], 67h
        # 30 rep movsd
        # 32 mov cl, g_charInfo.str
        # 38 mov [ebp+var_40.agi], dl
        # 41 mov dl, g_charInfo.slot
        # 47 mov [ebp+var_40.str], cl
        # 50 mov cl, g_charInfo.int_
        # 56 mov [ebp+var_40.vit], al
        # 59 mov eax, dword ptr g_charInfo.dex
        # 64 mov [ebp+var_40.slot], dl
        # 67 lea edx, [ebp+var_40]
        # 70 mov [ebp+var_40.int_], cl
        # 73 mov cx, g_charInfo.hair_style
        # 80 mov [ebp+var_40.dex], al
        # 83 mov [ebp+var_40.luk], ah
        # 86 mov ax, g_charInfo.hair_color
        # 92 push edx
        # 93 push 67h
        # 95 mov [ebp+var_40.hair_color], ax
        # 99 mov [ebp+var_40.hair_style], cx
        code = (
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 0
            b"\xA0\xAB\xAB\xAB\xAB"            # 6
            b"\xB9\xAB\xAB\x00\x00"            # 11
            b"\xBE\xAB\xAB\xAB\xAB"            # 16
            b"\x8D\x7D\xAB"                    # 21
            b"\x66\xC7\x45\xAB\xAB\xAB"        # 24
            b"\xF3\xA5"                        # 30
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 32
            b"\x88\x55\xAB"                    # 38
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 41
            b"\x88\x4D\xAB"                    # 47
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 50
            b"\x88\x45\xAB"                    # 56
            b"\xA1\xAB\xAB\xAB\xAB"            # 59
            b"\x88\x55\xAB"                    # 64
            b"\x8D\x55\xAB"                    # 67
            b"\x88\x4D\xAB"                    # 70
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 73
            b"\x88\x45\xAB"                    # 80
            b"\x88\x65\xAB"                    # 83
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 86
            b"\x52"                            # 92
            b"\x6A\x67"                        # 93
            b"\x66\x89\x45\xAB"                # 95
            b"\x66\x89\x4D\xAB"                # 99
        )
        nameOffset1 = (17, 4, 0)
        nameOffset2 = (23, 1, 0)
        packetIdNumOffset = (28, 2, 0)
        packetIdOffset2 = (27, 1, 0)
        hairColorOffset1 = (88, 4, 0)
        hairColorOffset2 = (98, 1, 0)
        hairStyleOffset1 = (76, 4, 0)
        hairStyleOffset2 = (102, 1, 0)
        slotOffset1 = (43, 4, 0)
        slotOffset2 = (66, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (34, 4, 0)
        statsOffset2 = (49, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2006-03-24
        # 0  mov al, g_charInfo.agi
        # 5  mov dl, g_charInfo.str
        # 11 mov ecx, 6
        # 16 mov esi, offset g_charInfo.char_name
        # 21 lea edi, [ebp+var_2C.name]
        # 24 mov [ebp+var_2C.packet_id], 67h
        # 30 rep movsd
        # 32 mov cl, g_charInfo.vit
        # 38 mov [ebp+var_2C.agi], al
        # 41 mov eax, dword ptr g_charInfo.dex
        # 46 mov [ebp+var_2C.str], dl
        # 49 mov dl, g_charInfo.int_
        # 55 mov [ebp+var_2C.dex], al
        # 58 mov al, g_charInfo.slot
        # 63 mov [ebp+var_2C.luk], ah
        # 66 mov [ebp+var_2C.slot], al
        # 69 lea eax, [ebp+var_2C]
        # 72 mov [ebp+var_2C.vit], cl
        # 75 mov cx, g_charInfo.hair_color
        # 82 mov [ebp+var_2C.int_], dl
        # 85 mov dx, g_charInfo.hair_style
        # 92 push eax
        # 93 push 67h
        # 95 mov [ebp+var_2C.hair_color], cx
        # 99 mov [ebp+var_2C.hair_style], dx
        code = (
            b"\xA0\xAB\xAB\xAB\xAB"            # 0
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 5
            b"\xB9\xAB\xAB\xAB\xAB"            # 11
            b"\xBE\xAB\xAB\xAB\xAB"            # 16
            b"\x8D\x7D\xAB"                    # 21
            b"\x66\xC7\x45\xAB\xAB\xAB"        # 24
            b"\xF3\xA5"                        # 30
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 32
            b"\x88\x45\xAB"                    # 38
            b"\xA1\xAB\xAB\xAB\xAB"            # 41
            b"\x88\x55\xAB"                    # 46
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 49
            b"\x88\x45\xAB"                    # 55
            b"\xA0\xAB\xAB\xAB\xAB"            # 58
            b"\x88\x65\xAB"                    # 63
            b"\x88\x45\xAB"                    # 66
            b"\x8D\x45\xAB"                    # 69
            b"\x88\x4D\xAB"                    # 72
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 75
            b"\x88\x55\xAB"                    # 82
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 85
            b"\x50"                            # 92
            b"\x6A\x67"                        # 93
            b"\x66\x89\x4D\xAB"                # 95
            b"\x66\x89\x55\xAB"                # 99
        )
        nameOffset1 = (17, 4, 0)
        nameOffset2 = (23, 1, 0)
        packetIdNumOffset = (28, 2, 0)
        packetIdOffset2 = (27, 1, 0)
        hairColorOffset1 = (78, 4, 0)
        hairColorOffset2 = (98, 1, 0)
        hairStyleOffset1 = (88, 4, 0)
        hairStyleOffset2 = (102, 1, 0)
        slotOffset1 = (59, 4, 0)
        slotOffset2 = (68, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (7, 4, 0)
        statsOffset2 = (48, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2004-01-07
        # 0  mov al, g_charInfo.agi
        # 5  mov dl, g_charInfo.str
        # 11 mov ecx, 6
        # 16 mov esi, offset g_charInfo.char_name
        # 21 lea edi, [ebp+var_2C.name]
        # 24 mov [ebp+var_2C.packet_id], 67h
        # 30 rep movsd
        # 32 mov cl, g_charInfo.vit
        # 38 mov [ebp+var_2C.agi], al
        # 41 mov eax, dword ptr g_charInfo.dex
        # 46 mov [ebp+var_2C.str], dl
        # 49 mov dl, g_charInfo.int_
        # 55 mov [ebp+var_2C.dex], al
        # 58 mov al, g_charInfo.slot
        # 63 mov [ebp+var_2C.vit], cl
        # 66 mov cx, g_charInfo.hair_color
        # 73 mov [ebp+var_2C.luk], ah
        # 76 mov [ebp+var_2C.slot], al
        # 79 lea eax, [ebp+var_2C]
        # 82 mov [ebp+var_2C.int_], dl
        # 85 mov dx, g_charInfo.hair_style
        # 92 mov [ebp+var_2C.hair_color], cx
        # 96 push eax
        # 97 push 67h
        # 99 mov ecx, offset g_instanceR
        # 104 mov [ebp+var_2C.hair_style], dx
        code = (
            b"\xA0\xAB\xAB\xAB\xAB"            # 0
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 5
            b"\xB9\xAB\xAB\x00\x00"            # 11
            b"\xBE\xAB\xAB\xAB\xAB"            # 16
            b"\x8D\x7D\xAB"                    # 21
            b"\x66\xC7\x45\xAB\xAB\xAB"        # 24
            b"\xF3\xA5"                        # 30
            b"\x8A\x0D\xAB\xAB\xAB\xAB"        # 32
            b"\x88\x45\xAB"                    # 38
            b"\xA1\xAB\xAB\xAB\xAB"            # 41
            b"\x88\x55\xAB"                    # 46
            b"\x8A\x15\xAB\xAB\xAB\xAB"        # 49
            b"\x88\x45\xAB"                    # 55
            b"\xA0\xAB\xAB\xAB\xAB"            # 58
            b"\x88\x4D\xAB"                    # 63
            b"\x66\x8B\x0D\xAB\xAB\xAB\xAB"    # 66
            b"\x88\x65\xAB"                    # 73
            b"\x88\x45\xAB"                    # 76
            b"\x8D\x45\xAB"                    # 79
            b"\x88\x55\xAB"                    # 82
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 85
            b"\x66\x89\x4D\xAB"                # 92
            b"\x50"                            # 96
            b"\x6A\x67"                        # 97
            b"\xB9\xAB\xAB\xAB\xAB"            # 99
            b"\x66\x89\x55\xAB"                # 104
        )
        nameOffset1 = (17, 4, 0)
        nameOffset2 = (23, 1, 0)
        packetIdNumOffset = (28, 2, 0)
        packetIdOffset2 = (27, 1, 0)
        hairColorOffset1 = (69, 4, 0)
        hairColorOffset2 = (95, 1, 0)
        hairStyleOffset1 = (88, 4, 0)
        hairStyleOffset2 = (107, 1, 0)
        slotOffset1 = (59, 4, 0)
        slotOffset2 = (78, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (7, 4, 0)
        statsOffset2 = (48, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # aro
        # 0  mov ecx, dword ptr g_charInfo.name
        # 6  mov edx, dword ptr g_charInfo.name+4
        # 12 mov dword ptr [esp+16Ch+p.name], ecx
        # 16 mov ecx, dword ptr g_charInfo.name+0Ch
        # 22 mov dword ptr [esp+16Ch+p.name+0Ch], ecx
        # 26 movzx ecx, g_charInfo.str
        # 33 mov eax, 67h
        # 38 mov [esp+16Ch+p.packet_id], ax
        # 43 mov eax, dword ptr g_charInfo.name+8
        # 48 mov dword ptr [esp+16Ch+p.name+4], edx
        # 52 mov edx, dword ptr g_charInfo.name+10h
        # 58 mov dword ptr [esp+16Ch+p.name+8], eax
        # 62 mov eax, dword ptr g_charInfo.name+14h
        # 67 mov [esp+16Ch+p.str], cl
        # 71 movzx ecx, g_charInfo.int_
        # 78 mov dword ptr [esp+16Ch+p.name+10h], edx
        # 82 movzx edx, g_charInfo.agi
        # 89 mov dword ptr [esp+16Ch+p.name+14h], eax
        # 93 movzx eax, g_charInfo.vit
        # 100 mov [esp+16Ch+p.int_], cl
        # 104 movzx ecx, byte ptr g_charInfo.slot
        # 111 mov [esp+16Ch+p.agi], dl
        # 115 movzx edx, g_charInfo.dex
        # 122 mov [esp+16Ch+p.vit], al
        # 126 movzx eax, g_charInfo.luk
        # 133 mov [esp+16Ch+p.slot], cl
        # 137 lea ecx, [esp+16Ch+p]
        # 141 mov [esp+16Ch+p.dex], dl
        # 145 mov dx, word ptr g_charInfo.hair_color
        # 152 mov [esp+16Ch+p.luk], al
        # 156 mov ax, g_charInfo.hair_style
        # 162 push ecx
        # 163 push 67h
        # 165 mov [esp+174h+p.hair_color], dx
        # 170 mov [esp+174h+p.hair_style], ax
        code = (
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 0
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 6
            b"\x89\x4C\x24\xAB"                # 12
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 16
            b"\x89\x4C\x24\xAB"                # 22
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 26
            b"\xB8\xAB\xAB\x00\x00"            # 33
            b"\x66\x89\x44\x24\xAB"            # 38
            b"\xA1\xAB\xAB\xAB\xAB"            # 43
            b"\x89\x54\x24\x1E"                # 48
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 52
            b"\x89\x44\x24\xAB"                # 58
            b"\xA1\xAB\xAB\xAB\xAB"            # 62
            b"\x88\x4C\x24\xAB"                # 67
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 71
            b"\x89\x54\x24\xAB"                # 78
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 82
            b"\x89\x44\x24\xAB"                # 89
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 93
            b"\x88\x4C\x24\xAB"                # 100
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 104
            b"\x88\x54\x24\xAB"                # 111
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 115
            b"\x88\x44\x24\xAB"                # 122
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 126
            b"\x88\x4C\x24\xAB"                # 133
            b"\x8D\x4C\x24\xAB"                # 137
            b"\x88\x54\x24\xAB"                # 141
            b"\x66\x8B\x15\xAB\xAB\xAB\xAB"    # 145
            b"\x88\x44\x24\xAB"                # 152
            b"\x66\xA1\xAB\xAB\xAB\xAB"        # 156
            b"\x51"                            # 162
            b"\x6A\xAB"                        # 163
            b"\x66\x89\x54\x24\xAB"            # 165
            b"\x66\x89\x44\x24\xAB"            # 170
        )
        nameOffset1 = (2, 4, 0)
        nameOffset2 = (15, 1, 0)
        packetIdNumOffset = (34, 4, 0)
        packetIdOffset2 = (42, 1, 0)
        hairColorOffset1 = (148, 4, 0)
        hairColorOffset2 = (169, 1, -8)
        hairStyleOffset1 = (158, 4, 0)
        hairStyleOffset2 = (174, 1, -8)
        slotOffset1 = (107, 4, 0)
        slotOffset2 = (136, 1, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (29, 4, 0)
        statsOffset2 = (70, 1, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2018-07-13 ruro
        # 0  movq xmm0, qword ptr g_charInfo.char_name
        # 8  mov eax, 67h
        # 13 mov [ebp+packet.packet_id], ax
        # 20 mov al, g_charInfo.str
        # 25 mov [ebp+packet.str], al
        # 31 mov al, g_charInfo.agi
        # 36 mov [ebp+packet.agi], al
        # 42 mov al, g_charInfo.vit
        # 47 mov [ebp+packet.vit], al
        # 53 mov al, g_charInfo.int_
        # 58 mov [ebp+packet.int_], al
        # 64 mov al, g_charInfo.dex
        # 69 mov [ebp+packet.dex], al
        # 75 mov al, g_charInfo.luk
        # 80 mov [ebp+packet.luk], al
        # 86 mov al, g_charInfo.slot
        # 91 mov [ebp+packet.slot], al
        # 97 movzx eax, g_charInfo.hair_color
        # 104 mov [ebp+packet.hair_color], ax
        # 111 movzx eax, g_charInfo.hair_style
        # 118 movq qword ptr [ebp+packet.name], xmm0
        # 126 movq xmm0, qword ptr g_charInfo.char_name+8
        # 134 mov [ebp+packet.hair_style], ax
        # 141 lea eax, [ebp+packet]
        code = (
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 0
            b"\xB8\xAB\xAB\x00\x00"            # 8
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 13
            b"\xA0\xAB\xAB\xAB\xAB"            # 20
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 25
            b"\xA0\xAB\xAB\xAB\xAB"            # 31
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 36
            b"\xA0\xAB\xAB\xAB\xAB"            # 42
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 47
            b"\xA0\xAB\xAB\xAB\xAB"            # 53
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 58
            b"\xA0\xAB\xAB\xAB\xAB"            # 64
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 69
            b"\xA0\xAB\xAB\xAB\xAB"            # 75
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 80
            b"\xA0\xAB\xAB\xAB\xAB"            # 86
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 91
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 97
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 104
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 111
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 118
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 126
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 134
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 141
        )
        nameOffset1 = (4, 4, 0)
        nameOffset2 = (122, 4, 0)
        packetIdNumOffset = (9, 4, 0)
        packetIdOffset2 = (16, 4, 0)
        hairColorOffset1 = (100, 4, 0)
        hairColorOffset2 = (107, 4, 0)
        hairStyleOffset1 = (114, 4, 0)
        hairStyleOffset2 = (137, 4, 0)
        slotOffset1 = (87, 4, 0)
        slotOffset2 = (93, 4, 0)
        startJobIdOffset1 = 0
        startJobIdOffset2 = 0
        sexOffset1 = 0
        sexOffset2 = 0
        statsOffset1 = (21, 4, 0)
        statsOffset2 = (27, 4, 0)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2018-08-13 ruro
        # 0  movq xmm0, qword ptr g_charInfo.char_name
        # 8  xor eax, eax
        # 10 mov g_charInfo.class, ax
        # 16 mov eax, 0A39h
        # 21 mov [ebp+packet.packetId], ax
        # 28 movzx eax, g_charInfo.hair_color
        # 35 mov [ebp+packet.hair_color], ax
        # 42 movzx eax, g_charInfo.hair_style
        # 49 mov [ebp+packet.hair_style], ax
        # 56 mov al, g_charInfo.slot
        # 61 mov [ebp+packet.slot], al
        # 67 mov al, g_charInfo.sex
        # 72 movq qword ptr [ebp+packet.name], xmm0
        # 80 movq xmm0, qword ptr g_charInfo.char_name+8
        # 88 mov [ebp+packet.sex], al
        # 94 lea eax, [ebp+packet]
        # 100 movq qword ptr [ebp+packet.name+8], xmm0
        # 108 movq xmm0, qword ptr g_charInfo.char_name+10h
        # 116 push eax
        # 117 push 0A39h
        # 122 movq qword ptr [ebp+packet.name+10h], xmm0
        # 130 mov [ebp+packet.class], 0
        code = (
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 0
            b"\x33\xC0"                        # 8
            b"\x66\xA3\xAB\xAB\xAB\xAB"        # 10
            b"\xB8\xAB\xAB\x00\x00"            # 16
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 21
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 28
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 35
            b"\x0F\xB7\x05\xAB\xAB\xAB\xAB"    # 42
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 49
            b"\xA0\xAB\xAB\xAB\xAB"            # 56
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 61
            b"\xA0\xAB\xAB\xAB\xAB"            # 67
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 72
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 80
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 88
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 94
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 100
            b"\xF3\x0F\x7E\x05\xAB\xAB\xAB\xAB"  # 108
            b"\x50"                            # 116
            b"\x68\xAB\xAB\x00\x00"            # 117
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 122
            b"\xC7\x85\xAB\xAB\xAB\xAB\x00\x00\x00\x00"  # 130
        )
        nameOffset1 = (4, 4, 0)
        nameOffset2 = (76, 4, 0)
        packetIdNumOffset = (17, 4, 0)
        packetIdOffset2 = (24, 4, 0)
        hairColorOffset1 = (31, 4, 0)
        hairColorOffset2 = (38, 4, 0)
        hairStyleOffset1 = (45, 4, 0)
        hairStyleOffset2 = (52, 4, 0)
        slotOffset1 = (57, 4, 0)
        slotOffset2 = (63, 4, 0)
        startJobIdOffset1 = (12, 4, 0)
        startJobIdOffset2 = (132, 4, 0)
        sexOffset1 = (68, 4, 0)
        sexOffset2 = (90, 4, 0)
        statsOffset1 = 0
        statsOffset2 = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        self.log("Error: failed in seach char create packet")
        if errorExit is True and self.clientType not in ("mro", "iro"):
            exit(1)
        return

    if packetIdNumOffset[1] == 4:
        charCreatePacketId = self.exe.readUInt(offset + packetIdNumOffset[0])
    elif packetIdNumOffset[1] == 2:
        charCreatePacketId = self.exe.readUWord(offset + packetIdNumOffset[0])
    else:
        print("Error: packetIdNumOffset is wrong: {0}".
              format(packetIdNumOffset[2]))
        exit(1)

    self.showVaAddr("Char create packet id", charCreatePacketId)

    name1 = self.exe.readUInt(offset + nameOffset1[0]) - \
        self.gcharInfo + nameOffset1[2]
    hairColor1 = self.exe.readUInt(offset + hairColorOffset1[0]) - \
        self.gcharInfo + hairColorOffset1[2]
    hairStyle1 = self.exe.readUInt(offset + hairStyleOffset1[0]) - \
        self.gcharInfo + hairStyleOffset1[2]
    slot1 = self.exe.readUInt(offset + slotOffset1[0]) - \
        self.gcharInfo + slotOffset1[2]
    if startJobIdOffset1 != 0:
        startJobId1 = self.exe.readUInt(offset + startJobIdOffset1[0]) - \
            self.gcharInfo + startJobIdOffset1[2]
    if sexOffset1 != 0:
        sex1 = self.exe.readUInt(offset + sexOffset1[0]) - \
            self.gcharInfo + sexOffset1[2]
    if statsOffset1 != 0:
        stats1 = self.exe.readUInt(offset + statsOffset1[0]) - \
            self.gcharInfo + statsOffset1[2]

    packetId2 = self.getVarAddr(offset, packetIdOffset2)
    name2 = self.getVarAddr(offset, nameOffset2) - packetId2 + nameOffset2[2]
    hairColor2 = self.getVarAddr(offset, hairColorOffset2) - packetId2 + \
        hairColorOffset2[2]
    hairStyle2 = self.getVarAddr(offset, hairStyleOffset2) - packetId2 + \
        hairStyleOffset2[2]
    slot2 = self.getVarAddr(offset, slotOffset2) - packetId2 + slotOffset2[2]
    if startJobIdOffset2 != 0:
        startJobId2 = self.getVarAddr(offset, startJobIdOffset2) - \
            packetId2 + startJobIdOffset2[2]
    if sexOffset2 != 0:
        sex2 = self.getVarAddr(offset, sexOffset2) - packetId2 + sexOffset2[2]
    if statsOffset2 != 0:
        stats2 = self.getVarAddr(offset, statsOffset2) - packetId2 + \
            statsOffset2[2]

    self.addStruct("CHARACTER_INFO")
    self.addStructMember("char_name", name1, 24, True)
    self.setStructMemberType(name1, "char[24]")
    self.addStructMember("hair_color", hairColor1, 2, True)
    self.addStructMember("hair_style", hairStyle1, 2, True)
    self.addStructMember("slot", slot1, 1, True)
    if startJobIdOffset1 != 0:
        self.addStructMember("class", startJobId1, 2, True)
    if sexOffset1 != 0:
        self.addStructMember("sex", sex1, 1, True)
    if statsOffset1 != 0:
        self.addStructMember("str", stats1, 1, True)
        self.addStructMember("agi", stats1 + 1, 1, True)
        self.addStructMember("vit", stats1 + 2, 1, True)
        self.addStructMember("int_", stats1 + 3, 1, True)
        self.addStructMember("dex", stats1 + 4, 1, True)
        self.addStructMember("luk", stats1 + 5, 1, True)

    debug = False
    self.addStruct("struct_packet_{0}".format(hex(charCreatePacketId)[2:]))
    self.addStructMember("packet_id", 0, 2, debug)
    self.addStructMember("name", name2, 24, debug)
    self.setStructMemberType(name2, "char[24]")
    self.addStructMember("slot", slot2, 1, debug)
    self.addStructMember("hair_color", hairColor2, 2, debug)
    self.addStructMember("hair_style", hairStyle2, 2, debug)
    if startJobIdOffset2 != 0:
        self.addStructMember("class", startJobId2, 4, debug)
    if sexOffset2 != 0:
        self.addStructMember("sex", sex2, 1, debug)
    if statsOffset2 != 0:
        self.addStructMember("str", stats2, 1, debug)
        self.addStructMember("agi", stats2 + 1, 1, debug)
        self.addStructMember("vit", stats2 + 2, 1, debug)
        self.addStructMember("int_", stats2 + 3, 1, debug)
        self.addStructMember("dex", stats2 + 4, 1, debug)
        self.addStructMember("luk", stats2 + 5, 1, debug)
