#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSkinMgr(self, errorExit):
    offset, section = self.exe.string(b"m_bAutoOpenDetailWindowIfLowMemory")
    if offset is False:
        self.log("failed in search m_bAutoOpenDetailWindowIfLowMemory")
        exit(1)
        return
    strAddr = section.rawToVa(offset)
    # search in CSession::WriteOptionToRegistry
    # 2016 - 2018 +
    # 0  push 4
    # 2  push 0
    # 4  push offset aM_bautoopendet
    # 9  push [ebp+phkResult]
    # 15 call ebx
    # 17 mov ecx, offset g_skinMgr
    # 22 call CSkinMgr_GetCurrentSkinName
    # 27 push eax
    code = (
        b"\x6A\x04"                           # 0
        b"\x6A\x00"                           # 2
        b"\x68" + self.exe.toHex(strAddr, 4) +  # 4
        b"\xFF\xB5\xAB\xAB\xAB\xAB"           # 9
        b"\xFF\xD3"                           # 15
        b"\xB9\xAB\xAB\xAB\xAB"               # 17
        b"\xE8\xAB\xAB\xAB\xFF"               # 22
        b"\x50"                               # 27
    )
    skinMgrOffset = 18
    skinNameOffset = 23
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2014 - 2015
        # 0  push 4
        # 2  push 0
        # 4  mov ecx, [ebp+phkResult]
        # 10 push offset aM_bautoopendet
        # 15 push ecx
        # 16 call ebx
        # 18 mov ecx, offset g_skinMgr
        # 23 call CSkinMgr_GetCurrentSkinName
        # 28 push eax
        code = (
            b"\x6A\x04"                        # 0
            b"\x6A\x00"                        # 2
            b"\x8B\x8D\xAB\xAB\xAB\xAB"        # 4
            b"\x68" + self.exe.toHex(strAddr, 4) +  # 4
            b"\x51"                            # 15
            b"\xFF\xD3"                        # 16
            b"\xB9\xAB\xAB\xAB\xAB"            # 18
            b"\xE8\xAB\xAB\xAB\xFF"            # 23
            b"\x50"                            # 28
        )
        skinMgrOffset = 19
        skinNameOffset = 24
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2008 - 2013
        # 0  push 4
        # 2  push 0
        # 4  push offset aM_bautoopendet
        # 9  push edx
        # 10 call ebx
        # 12 mov ecx, offset g_skinMgr
        # 17 call CSkinMgr_GetCurrentSkinName
        # 22 push eax
        code = (
            b"\x6A\x04"                        # 0
            b"\x6A\x00"                        # 2
            b"\x68" + self.exe.toHex(strAddr, 4) +  # 4
            b"\xAB"                            # 9
            b"\xFF\xD3"                        # 10
            b"\xB9\xAB\xAB\xAB\xAB"            # 12
            b"\xE8\xAB\xAB\xAB\xFF"            # 17
            b"\x50"                            # 22
        )
        skinMgrOffset = 13
        skinNameOffset = 18
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2007
        # 0  push 4
        # 2  mov ecx, [ebp+phkResult]
        # 5  push 0
        # 7  push offset aM_bautoopendet
        # 12 push ecx
        # 13 call ebx
        # 15 mov ecx, offset g_skinMgr
        # 20 call CSkinMgr_GetCurrentSkinName
        # 25 push eax
        code = (
            b"\x6A\x04"                        # 0
            b"\x8B\x4D\xAB"                    # 2
            b"\x6A\x00"                        # 5
            b"\x68" + self.exe.toHex(strAddr, 4) +  # 4
            b"\x51"                            # 12
            b"\xFF\xD3"                        # 13
            b"\xB9\xAB\xAB\xAB\xAB"            # 15
            b"\xE8\xAB\xAB\xAB\xFF"            # 20
            b"\x50"                            # 25
        )
        skinMgrOffset = 16
        skinNameOffset = 21
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search SkinMgr.")
        if errorExit is True:
            exit(1)
        return

    self.gSkinMgr = self.exe.read(offset + skinMgrOffset, 4, "V")
    self.CSkinMgrGetCurrentSkinName = self.getAddr(offset,
                                                   skinNameOffset,
                                                   skinNameOffset + 4)
    self.addVaVar("g_skinMgr", self.gSkinMgr)
    self.addRawFunc("CSkinMgr::GetCurrentSkinName",
                    self.CSkinMgrGetCurrentSkinName)
