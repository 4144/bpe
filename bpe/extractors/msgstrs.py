#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.calls import searchCallsToCall
from bpe.blocks.blocks import searchBlocks, blocksToLabels, showBlocksInfo


def searchMsgStrs(self):
    if self.msgStrings is None:
        self.log("Error: skip extract msg strings")
        return
    showInfo = False
    msgStrVa = self.exe.rawToVa(self.MsgStr)
    offsets = searchCallsToCall(self, msgStrVa)
    relVars = {}
    from bpe.blocks.msgstr import blocks
    found, errors = searchBlocks(self,
                                 "MsgStrs",
                                 offsets,
                                 blocks,
                                 relVars,
                                 "msgId")
    self.msgStrBlocks = found
    self.msgStrStats = (len(found), len(errors))
    for f in found:
        msgId = f[2]
        if msgId not in self.msgStrings:
            # need extract real max msg
            if msgId > 0x1000:
                print("Error: found wrong msgId: "
                      "{0}: {1}, {2}".format(hex(msgId),
                                             hex(self.exe.rawToVa(f[3])),
                                             f[0][0].encode("hex")))
                exit(1)
            else:
                print("Error: found possible wrong msgId: "
                      "{0}: {1}".format(hex(msgId),
                                        f[0][0].encode("hex")))
    if showInfo is True:
        showBlocksInfo(self, offsets, found, errors)


def addMsgStrLabels(self):
    if self.msgStrBlocks is None:
        self.log("Error: skip add msgStr labels")
        return
    name = "MsgStr_{0}"
    blocksToLabels(self, self.msgStrBlocks, name)
