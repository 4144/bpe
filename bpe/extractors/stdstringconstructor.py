#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2019 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchStdStringConstructor(self):
    # 2017-11-01
    # 0  mov [ecx+struct_std_string.m_allocated_len], 0Fh
    # 7  mov [ecx+struct_std_string.m_len], 0
    # 14 mov byte ptr [ecx+struct_std_string.m_cstr], 0
    # 17 mov eax, ecx
    # 19 ret retn
    code = (
        b"\xC7\x41\x14\x0F\x00\x00\x00"    # 0 mov [ecx+struct_std_string.m_all
        b"\xC7\x41\x10\x00\x00\x00\x00"    # 7 mov [ecx+struct_std_string.m_len
        b"\xC6\x01\x00"                    # 14 mov byte ptr [ecx+struct_std_st
        b"\x8B\xC1"                        # 17 mov eax, ecx
        b"\xC3"                            # 19 ret retn
    )
    offsets = self.exe.codes(code, -1)

    if offsets is False:
        # 2019-03-27
        # 0  mov [ecx+struct_std_string.m_len], 0
        # 7  mov eax, ecx
        # 9  mov [ecx+struct_std_string.m_allocated_len], 0Fh
        # 16 mov byte ptr [ecx+struct_std_string.m_cstr], 0
        # 19 ret retn
        code = (
            b"\xC7\x41\x10\x00\x00\x00\x00"    # 0 mov [ecx+struct_std_string.m
            b"\x8B\xC1"                        # 7 mov eax, ecx
            b"\xC7\x41\x14\x0F\x00\x00\x00"    # 9 mov [ecx+struct_std_string.m
            b"\xC6\x01\x00"                    # 16 mov byte ptr [ecx+struct_st
            b"\xC3"                            # 19 ret retn
        )
        offsets = self.exe.codes(code, -1)

    if offsets is False:
        # 2015-01-07
        # 0  mov eax, ecx
        # 2  mov [eax+struct_std_string.m_allocated_len], 0Fh
        # 9  mov [eax+struct_std_string.m_len], 0
        # 16 mov byte ptr [eax+struct_std_string.m_cstr], 0
        # 19 ret retn
        code = (
            b"\x8B\xC1"                        # 0 mov eax, ecx
            b"\xC7\x40\x14\x0F\x00\x00\x00"    # 2 mov [eax+struct_std_string.m
            b"\xC7\x40\x10\x00\x00\x00\x00"    # 9 mov [eax+struct_std_string.m
            b"\xC6\x00\x00"                    # 16 mov byte ptr [eax+struct_st
            b"\xC3"                            # 19 ret retn
        )
        offsets = self.exe.codes(code, -1)

    if offsets is False:
        self.log("Error: std::string constructor not found")
        if self.packetVersion >= "20140000" and self.clientType != "iro":
            exit(1)
        return
    sz = len(offsets)
    if sz > 1:
        self.log("Error: found more than one std::string constructor:"
                 "{0}".format(sz))
        exit(1)
    self.stdstring_constructor = offsets[0]
    self.addRawFunc("std::string::constructor", self.stdstring_constructor)

    debug = False
    self.addStruct("struct_std_string")
    self.addStructMember("m_cstr", 0, 4, debug)
    self.addStructMember("m_len", 0x10, 4, debug)
    self.addStructMember("m_allocated_len", 0x14, 4, debug)
