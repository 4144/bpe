#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# search in recv_packet_294


def searchLanguage(self):
    offset, section = self.exe.halfString(b"%sbook\\%s.txt")
    if offset is False:
        self.log("failed in search %sbook\\%s.txt")
        if self.packetVersion < "20080000":
            return
        exit(1)
    strAddr = section.rawToVa(offset)
    strHex = self.exe.toHex(strAddr, 4)
    gServiceTypeHex = self.exe.toHex(self.gServiceType, 4)

    # 0  cmp g_serviceType, 0Ah
    # 7  jnz short loc_A1AFF8
    # 9  mov eax, g_language
    # 14 add eax, 9Ch
    # 19 cmp dword ptr [eax+14h], 10h
    # 23 jb short loc_A1AFDC
    # 25 mov eax, [eax]
    # 27 lea ecx, [ebp+DstBuf]
    # 30 push ecx
    # 31 push eax
    # 32 lea eax, [ebp+FileName]
    # 38 push offset aSbookS_txt
    # 43 push eax
    # 44 call sprintf
    code = (
        b"\x83\x3D" + gServiceTypeHex + b"\x0A"  # 0 cmp g_serviceType, 0Ah
        b"\x75\xAB"                        # 7 jnz short loc_A1AFF8
        b"\xA1\xAB\xAB\xAB\xAB"            # 9 mov eax, g_language
        b"\x05\xAB\xAB\xAB\x00"            # 14 add eax, 9Ch
        b"\x83\x78\xAB\x10"                # 19 cmp dword ptr [eax+14h], 10h
        b"\x72\x02"                        # 23 jb short loc_A1AFDC
        b"\x8B\x00"                        # 25 mov eax, [eax]
        b"\x8D\x4D\xAB"                    # 27 lea ecx, [ebp+DstBuf]
        b"\x51"                            # 30 push ecx
        b"\x50"                            # 31 push eax
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 32 lea eax, [ebp+FileName]
        b"\x68" + strHex +                 # 38 push offset aSbookS_txt
        b"\x50"                            # 43 push eax
        b"\xFF\x15"                        # 44 call sprintf
    )
    gLanguageOffset = 10
    langPathOffset = (15, 4)
    allocLenOffset = (21, 1)
    allocLenOffset2 = 0
    sprintfOffset = (46, True)
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  cmp g_serviceType, 0Ah
        # 7  jnz short loc_98DFF8
        # 9  mov eax, g_language
        # 14 add eax, 9Ch
        # 19 cmp dword ptr [eax+14h], 10h
        # 23 jb short loc_98DFDC
        # 25 mov eax, [eax]
        # 27 lea ecx, [ebp+var_1C]
        # 30 push ecx
        # 31 push eax
        # 32 lea eax, [ebp+var_120]
        # 38 push offset aSbookS_txt
        # 43 push eax
        # 44 no nop
        # 45 call near ptr 35B8620h
        code = (
            b"\x83\x3D" + gServiceTypeHex + b"\x0A"  # 0 cmp g_serviceType, 0Ah
            b"\x75\xAB"                        # 7 jnz short loc_98DFF8
            b"\xA1\xAB\xAB\xAB\xAB"            # 9 mov eax, g_language
            b"\x05\xAB\xAB\xAB\x00"            # 14 add eax, 9Ch
            b"\x83\x78\xAB\x10"                # 19 cmp dword ptr [eax+14h], 10
            b"\x72\x02"                        # 23 jb short loc_98DFDC
            b"\x8B\x00"                        # 25 mov eax, [eax]
            b"\x8D\x4D\xAB"                    # 27 lea ecx, [ebp+var_1C]
            b"\x51"                            # 30 push ecx
            b"\x50"                            # 31 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 32 lea eax, [ebp+var_120]
            b"\x68" + strHex +                 # 38 push offset aSbookS_txt
            b"\x50"                            # 43 push eax
            b"\x90"                            # 44 no nop
            b"\xE8"                            # 45 call near ptr 35B8620h
        )
        gLanguageOffset = 10
        langPathOffset = (15, 4)
        allocLenOffset = (21, 1)
        allocLenOffset2 = 0
        sprintfOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  cmp g_serviceType, 0Ah
        # 7  jnz short loc_84EC79
        # 9  mov eax, g_language
        # 14 add eax, 0B0h
        # 19 cmp dword ptr [eax+14h], 10h
        # 23 jb short loc_84EC5D
        # 25 mov eax, [eax]
        # 27 lea edx, [ebp+DstBuf]
        # 30 push edx
        # 31 push eax
        # 32 lea eax, [ebp+FileName]
        # 38 push offset aSbookS_txt
        # 43 push eax
        # 44 call sprintf
        code = (
            b"\x83\x3D" + gServiceTypeHex + b"\x0A"  # 0 cmp g_serviceType, 0Ah
            b"\x75\xAB"                        # 7 jnz short loc_84EC79
            b"\xA1\xAB\xAB\xAB\xAB"            # 9 mov eax, g_language
            b"\x05\xAB\xAB\xAB\x00"            # 14 add eax, 0B0h
            b"\x83\x78\xAB\x10"                # 19 cmp dword ptr [eax+14h], 10
            b"\x72\x02"                        # 23 jb short loc_84EC5D
            b"\x8B\x00"                        # 25 mov eax, [eax]
            b"\x8D\x55\xAB"                    # 27 lea edx, [ebp+DstBuf]
            b"\x52"                            # 30 push edx
            b"\x50"                            # 31 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 32 lea eax, [ebp+FileName]
            b"\x68" + strHex +                 # 38 push offset aSbookS_txt
            b"\x50"                            # 43 push eax
            b"\xFF\x15"                        # 44 call sprintf
        )
        gLanguageOffset = 10
        langPathOffset = (15, 4)
        allocLenOffset = (21, 1)
        allocLenOffset2 = 0
        sprintfOffset = (46, True)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  cmp g_serviceType, 0Ah
        # 7  jnz short loc_7DC13B
        # 9  mov eax, g_language
        # 14 cmp dword ptr [eax+0D0h], 10h
        # 21 jb short loc_7DC115
        # 23 mov eax, [eax+0BCh]
        # 29 jmp short loc_7DC11A
        # 31 add eax, 0BCh
        # 36 lea edx, [esp+1B8h+DstBuf]
        # 43 push edx
        # 44 push eax
        # 45 lea eax, [esp+1C0h+FileName]
        # 52 push offset aSbookS_txt
        # 57 push eax
        # 58 call sprintf
        code = (
            b"\x83\x3D" + gServiceTypeHex + b"\x0A"  # 0 cmp g_serviceType, 0Ah
            b"\x75\xAB"                        # 7 jnz short loc_7DC13B
            b"\xA1\xAB\xAB\xAB\xAB"            # 9 mov eax, g_language
            b"\x83\xB8\xAB\xAB\xAB\x00\x10"    # 14 cmp dword ptr [eax+0D0h], 1
            b"\x72\x08"                        # 21 jb short loc_7DC115
            b"\x8B\x80\xAB\xAB\xAB\x00"        # 23 mov eax, [eax+0BCh]
            b"\xEB\x05"                        # 29 jmp short loc_7DC11A
            b"\x05\xAB\xAB\xAB\x00"            # 31 add eax, 0BCh
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"    # 36 lea edx, [esp+1B8h+DstBuf]
            b"\x52"                            # 43 push edx
            b"\x50"                            # 44 push eax
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"    # 45 lea eax, [esp+1C0h+FileName
            b"\x68" + strHex +                 # 52 push offset aSbookS_txt
            b"\x50"                            # 57 push eax
            b"\xFF\x15"                        # 58 call sprintf
        )
        gLanguageOffset = 10
        langPathOffset = (25, 4)
        allocLenOffset = 0
        allocLenOffset2 = (16, 4)
        sprintfOffset = (60, True)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov eax, g_serviceType
        # 5  add esp, 0Ch
        # 8  cmp eax, 0Ah
        # 11 jnz short loc_5D323E
        # 13 mov edx, g_language
        # 19 mov eax, [edx+0A8h]
        # 25 test eax, eax
        # 27 jnz short loc_5D3223
        # 29 mov eax, offset byte_72F844
        # 34 lea ecx, [ebp+var_18]
        # 37 lea edx, [ebp+var_1AC]
        # 43 push ecx
        # 44 push eax
        # 45 push offset aSbookS_txt
        # 50 push edx
        # 51 call sprintf
        code = (
            b"\xA1" + gServiceTypeHex +        # 0 mov eax, g_serviceType
            b"\x83\xC4\x0C"                    # 5 add esp, 0Ch
            b"\x83\xF8\x0A"                    # 8 cmp eax, 0Ah
            b"\x75\xAB"                        # 11 jnz short loc_5D323E
            b"\x8B\x15\xAB\xAB\xAB\xAB"        # 13 mov edx, g_language
            b"\x8B\x82\xAB\xAB\xAB\xAB"        # 19 mov eax, [edx+0A8h]
            b"\x85\xC0"                        # 25 test eax, eax
            b"\x75\x05"                        # 27 jnz short loc_5D3223
            b"\xB8\xAB\xAB\xAB\xAB"            # 29 mov eax, offset byte_72F844
            b"\x8D\x4D\xAB"                    # 34 lea ecx, [ebp+var_18]
            b"\x8D\x95\xAB\xAB\xAB\xAB"        # 37 lea edx, [ebp+var_1AC]
            b"\x51"                            # 43 push ecx
            b"\x50"                            # 44 push eax
            b"\x68" + strHex +                 # 45 push offset aSbookS_txt
            b"\x52"                            # 50 push edx
            b"\xE8"                            # 51 call sprintf
        )
        gLanguageOffset = 15
        langPathOffset = (21, 4)
        allocLenOffset = 0
        allocLenOffset2 = 0
        sprintfOffset = (52, False)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  cmp g_serviceType, 0Ah
        # 7  jnz short loc_5EC6B9
        # 9  mov eax, g_language
        # 14 cmp dword ptr [eax+0D8h], 10h
        # 21 jb short loc_5EC694
        # 23 mov eax, [eax+0C4h]
        # 29 jmp short loc_5EC699
        # 31 add eax, 0C4h
        # 36 lea edx, [esp+1B8h+var_120]
        # 43 push edx
        # 44 push eax
        # 45 lea eax, [esp+1C0h+FileName]
        # 52 push offset aSbookS_txt
        # 57 push eax
        # 58 call _sprintf
        code = (
            b"\x83\x3D" + gServiceTypeHex + b"\x0A"  # 0 cmp g_serviceType, 0Ah
            b"\x75\xAB"                        # 7 jnz short loc_5EC6B9
            b"\xA1\xAB\xAB\xAB\xAB"            # 9 mov eax, g_language
            b"\x83\xB8\xAB\xAB\xAB\x00\x10"    # 14 cmp dword ptr [eax+0D8h], 1
            b"\x72\x08"                        # 21 jb short loc_5EC694
            b"\x8B\x80\xAB\xAB\xAB\x00"        # 23 mov eax, [eax+0C4h]
            b"\xEB\x05"                        # 29 jmp short loc_5EC699
            b"\x05\xAB\xAB\xAB\x00"            # 31 add eax, 0C4h
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"    # 36 lea edx, [esp+1B8h+var_120]
            b"\x52"                            # 43 push edx
            b"\x50"                            # 44 push eax
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"    # 45 lea eax, [esp+1C0h+FileName
            b"\x68" + strHex +                 # 52 push offset aSbookS_txt
            b"\x50"                            # 57 push eax
            b"\xE8"                            # 58 call _sprintf
        )
        gLanguageOffset = 10
        langPathOffset = (25, 4)
        allocLenOffset = 0
        allocLenOffset2 = (16, 4)
        sprintfOffset = (59, False)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  cmp g_serviceType, 0Ah
        # 7  jnz short loc_A04B67
        # 9  mov eax, g_language
        # 14 add eax, 9Ch
        # 19 cmp dword ptr [eax+14h], 10h
        # 23 jb short loc_A04B4C
        # 25 mov eax, [eax]
        # 27 lea ecx, [ebp+DstBuf]
        # 30 push ecx
        # 31 push eax
        # 32 lea eax, [ebp+FileName]
        # 38 push offset aSbookS_txt
        # 43 push eax
        # 44 call sprintf
        code = (
            b"\x83\x3D" + gServiceTypeHex + b"\x0A"  # 0 cmp g_serviceType, 0Ah
            b"\x75\xAB"                        # 7 jnz short loc_A04B67
            b"\xA1\xAB\xAB\xAB\xAB"            # 9 mov eax, g_language
            b"\x05\xAB\xAB\xAB\x00"            # 14 add eax, 9Ch
            b"\x83\x78\xAB\x10"                # 19 cmp dword ptr [eax+14h], 10
            b"\x72\x02"                        # 23 jb short loc_A04B4C
            b"\x8B\x00"                        # 25 mov eax, [eax]
            b"\x8D\x4D\xAB"                    # 27 lea ecx, [ebp+DstBuf]
            b"\x51"                            # 30 push ecx
            b"\x50"                            # 31 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 32 lea eax, [ebp+FileName]
            b"\x68" + strHex +                 # 38 push offset aSbookS_txt
            b"\x50"                            # 43 push eax
            b"\xE8"                            # 44 call sprintf
        )
        gLanguageOffset = 10
        langPathOffset = (15, 4)
        allocLenOffset = (21, 1)
        allocLenOffset2 = 0
        sprintfOffset = (45, False)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("Error: g_language not found")
        if self.packetVersion < "20080000":
            return
        exit(1)

    self.gLanguage = self.exe.readUInt(offset + gLanguageOffset)
    self.addVaVar("g_language", self.gLanguage)
    if sprintfOffset != 0:
        if sprintfOffset[1] is True:
            self.sprintf = self.exe.readUInt(offset + sprintfOffset[0])
            self.addVaFunc("sprintf", self.sprintf)
        else:
            self.sprintf = self.getAddr(offset,
                                        sprintfOffset[0],
                                        sprintfOffset[0] + 4)
            self.addRawFunc("sprintf", self.sprintf)


    langPath = self.getVarAddr(offset, langPathOffset)
    self.addStruct("CLanguage")
    self.addStructMember("lang_path", langPath, 4, True)
    self.setStructMemberType(langPath, "struct_std_string")
    if allocLenOffset != 0:
        allocLen = self.getVarAddr(offset, allocLenOffset)
    elif allocLenOffset2 != 0:
        allocLen = self.getVarAddr(offset, allocLenOffset2) - langPath
    else:
        allocLen = -1
    if allocLen != -1 and allocLen != self.stdstringSize - 4:
        self.log("Error: found wrong std::string offset in g_language")
        exit(1)
