#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# search inside CGameMode::WriteChat


def searchCFile(self):
    sessionHex = self.exe.toHex(self.session, 4)

    # 2017+
    # 0  mov esi, ecx
    # 2  mov edi, [ebp+lpBuffer]
    # 5  push 7
    # 7  mov ecx, offset g_session
    # 12 call CSession_IsMasterAid
    # 17 test eax, eax
    # 19 jz loc_9D16CE
    # 25 inc [esi+CGameMode.m_autoSaveChatCnt]
    # 31 lea ecx, [ebp+fp]
    # 34 call CFile_CFile
    # 39 mov [ebp+var_4], 0
    # 46 push 0
    # 48 push 1
    # 50 push 0
    # 52 push offset autosaveFileName
    # 57 lea ecx, [ebp+fp]
    # 60 call CFile_open
    # 65 mov esi, [ebp+fp.m_size]
    # 68 push esi
    # 69 call j_??_U@YAPAXI@Z
    # 74 add esp, 4
    # 77 mov ebx, eax
    # 79 push esi
    # 80 push ebx
    # 81 lea ecx, [ebp+fp]
    # 84 call CFile_read
    # 89 lea ecx, [ebp+fp]
    # 92 call CFile_close
    # 97 push 0
    # 99 push 1
    # 101 push 1001h
    # 106 push offset autosaveFileName
    # 111 mov ecx, offset saveChatFp
    # 116 call CFile_open
    # 121 push esi
    # 122 push ebx
    # 123 mov ecx, offset saveChatFp
    # 128 call CFile_write
    # 133 mov eax, edi
    # 135 lea edx, [eax+1]
    # 138 mov cl, [eax]
    # 140 inc eax
    # 141 test cl, cl
    # 143 jnz short loc_9D1685
    # 145 sub eax, edx
    # 147 inc eax
    # 148 push eax
    # 149 push edi
    # 150 mov ecx, offset saveChatFp
    # 155 call CFile_write
    # 160 push 2
    # 162 push offset endchar
    # 167 mov ecx, offset saveChatFp
    # 172 call CFile_write
    # 177 mov ecx, offset saveChatFp
    # 182 call CFile_close
    # 187 push ebx
    # 188 call j_??_V@YAXPAX@Z
    # 193 add esp, 4
    # 196 mov [ebp+var_4], 0FFFFFFFFh
    # 203 lea ecx, [ebp+fp]
    # 206 call CFile_destructor
    code = (
        b"\x8B\xF1"                        # 0 mov esi, ecx
        b"\x8B\x7D\xAB"                    # 2 mov edi, [ebp+lpBuffer]
        b"\x6A\x07"                        # 5 push 7
        b"\xB9" + sessionHex +             # 7 mov ecx, offset g_session
        b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CSession_IsMasterAid
        b"\x85\xC0"                        # 17 test eax, eax
        b"\x0F\x84\xBA\x00\x00\x00"        # 19 jz loc_9D16CE
        b"\xFF\x86\xAB\xAB\x00\x00"        # 25 inc [esi+CGameMode.m_autoSaveCh
        b"\x8D\x4D\xAB"                    # 31 lea ecx, [ebp+fp]
        b"\xE8\xAB\xAB\xAB\xAB"            # 34 call CFile_CFile
        b"\xC7\x45\xAB\x00\x00\x00\x00"    # 39 mov [ebp+var_4], 0
        b"\x6A\x00"                        # 46 push 0
        b"\x6A\x01"                        # 48 push 1
        b"\x6A\x00"                        # 50 push 0
        b"\x68\xAB\xAB\xAB\xAB"            # 52 push offset autosaveFileName
        b"\x8D\x4D\xAB"                    # 57 lea ecx, [ebp+fp]
        b"\xE8\xAB\xAB\xAB\xAB"            # 60 call CFile_open
        b"\x8B\x75\xAB"                    # 65 mov esi, [ebp+fp.m_size]
        b"\x56"                            # 68 push esi
        b"\xE8\xAB\xAB\xAB\xAB"            # 69 call j_??_U@YAPAXI@Z
        b"\x83\xC4\x04"                    # 74 add esp, 4
        b"\x8B\xD8"                        # 77 mov ebx, eax
        b"\x56"                            # 79 push esi
        b"\x53"                            # 80 push ebx
        b"\x8D\x4D\xAB"                    # 81 lea ecx, [ebp+fp]
        b"\xE8\xAB\xAB\xAB\xAB"            # 84 call CFile_read
        b"\x8D\x4D\xAB"                    # 89 lea ecx, [ebp+fp]
        b"\xE8\xAB\xAB\xAB\xAB"            # 92 call CFile_close
        b"\x6A\x00"                        # 97 push 0
        b"\x6A\x01"                        # 99 push 1
        b"\x68\x01\x10\x00\x00"            # 101 push 1001h
        b"\x68\xAB\xAB\xAB\xAB"            # 106 push offset autosaveFileName
        b"\xB9\xAB\xAB\xAB\xAB"            # 111 mov ecx, offset saveChatFp
        b"\xE8\xAB\xAB\xAB\xAB"            # 116 call CFile_open
        b"\x56"                            # 121 push esi
        b"\x53"                            # 122 push ebx
        b"\xB9\xAB\xAB\xAB\xAB"            # 123 mov ecx, offset saveChatFp
        b"\xE8\xAB\xAB\xAB\xAB"            # 128 call CFile_write
        b"\x8B\xC7"                        # 133 mov eax, edi
        b"\x8D\x50\x01"                    # 135 lea edx, [eax+1]
        b"\x8A\x08"                        # 138 mov cl, [eax]
        b"\x40"                            # 140 inc eax
        b"\x84\xC9"                        # 141 test cl, cl
        b"\x75\xF9"                        # 143 jnz short loc_9D1685
        b"\x2B\xC2"                        # 145 sub eax, edx
        b"\x40"                            # 147 inc eax
        b"\x50"                            # 148 push eax
        b"\x57"                            # 149 push edi
        b"\xB9\xAB\xAB\xAB\xAB"            # 150 mov ecx, offset saveChatFp
        b"\xE8\xAB\xAB\xAB\xAB"            # 155 call CFile_write
        b"\x6A\x02"                        # 160 push 2
        b"\x68\xAB\xAB\xAB\xAB"            # 162 push offset endchar
        b"\xB9\xAB\xAB\xAB\xAB"            # 167 mov ecx, offset saveChatFp
        b"\xE8\xAB\xAB\xAB\xAB"            # 172 call CFile_write
        b"\xB9\xAB\xAB\xAB\xAB"            # 177 mov ecx, offset saveChatFp
        b"\xE8\xAB\xAB\xAB\xAB"            # 182 call CFile_close
        b"\x53"                            # 187 push ebx
        b"\xE8\xAB\xAB\xAB\xAB"            # 188 call j_??_V@YAXPAX@Z
        b"\x83\xC4\x04"                    # 193 add esp, 4
        b"\xC7\x45\xFC\xFF\xFF\xFF\xFF"    # 196 mov [ebp+var_4], 0FFFFFFFFh
        b"\x8D\x4D\xAB"                    # 203 lea ecx, [ebp+fp]
        b"\xE8"                            # 206 call CFile_destructor
    )
    isMasterAidOffset = 13
    autoSaveChatCntOffset = (27, 4)
    fpOffset = (33, 1)
    CFileOffset = 35
    autosaveFileNameOffset1 = (53, 4)
    autosaveFileNameOffset2 = (107, 4)
    openOffsets = (61, 117)
    fpSizeOffset = (67, 1)
    readOffset = 85
    closeOffsets = (93, 183)
    saveChatFpOffsets = (112, 124, 151, 168, 178)
    writeOffsets = (129, 156, 173)
    endCharOffset = (163, 4)
    destructorOffset = 207
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov esi, ecx
        # 2  mov edi, [ebp+lpBuffer]
        # 5  mov ecx, offset g_session
        # 10 push 7
        # 12 call CSession_IsMasterAid
        # 17 test eax, eax
        # 19 jz loc_9C10AE
        # 25 inc dword ptr [esi+5E8h]
        # 31 lea ecx, [ebp+var_3C]
        # 34 call CFile_CFile
        # 39 mov [ebp+var_4], 0
        # 46 lea ecx, [ebp+var_3C]
        # 49 push 0
        # 51 push 1
        # 53 push 0
        # 55 push offset autosaveFileName
        # 60 call CFile_open
        # 65 mov esi, [ebp+var_3C.m_size]
        # 68 push esi
        # 69 call new3
        # 74 add esp, 4
        # 77 lea ecx, [ebp+var_3C]
        # 80 mov ebx, eax
        # 82 push esi
        # 83 push ebx
        # 84 call CFile_read
        # 89 lea ecx, [ebp+var_3C]
        # 92 call CFile_close
        # 97 push 0
        # 99 push 1
        # 101 push 1001h
        # 106 push offset autosaveFileName
        # 111 mov ecx, offset saveChatFp
        # 116 call CFile_open
        # 121 push esi
        # 122 push ebx
        # 123 mov ecx, offset saveChatFp
        # 128 call CFile_write
        # 133 mov eax, edi
        # 135 lea edx, [eax+1]
        # 138 mov cl, [eax]
        # 140 inc eax
        # 141 test cl, cl
        # 143 jnz short loc_9C1065
        # 145 sub eax, edx
        # 147 mov ecx, offset saveChatFp
        # 152 inc eax
        # 153 push eax
        # 154 push edi
        # 155 call CFile_write
        # 160 push 2
        # 162 push offset asc_CEBFB8
        # 167 mov ecx, offset saveChatFp
        # 172 call CFile_write
        # 177 mov ecx, offset saveChatFp
        # 182 call CFile_close
        # 187 push ebx
        # 188 call j_j_free
        # 193 add esp, 4
        # 196 mov [ebp+var_4], 0FFFFFFFFh
        # 203 lea ecx, [ebp+var_3C]
        # 206 call CFile_destructor
        code = (
            b"\x8B\xF1"                        # 0 mov esi, ecx
            b"\x8B\x7D\xAB"                    # 2 mov edi, [ebp+lpBuffer]
            b"\xB9" + sessionHex +             # 5 mov ecx, offset g_session
            b"\x6A\x07"                        # 10 push 7
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CSession_IsMasterAid
            b"\x85\xC0"                        # 17 test eax, eax
            b"\x0F\x84\xBA\x00\x00\x00"        # 19 jz loc_9C10AE
            b"\xFF\x86\xAB\xAB\xAB\x00"        # 25 inc dword ptr [esi+CGameMod
            b"\x8D\x4D\xAB"                    # 31 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 34 call CFile_CFile
            b"\xC7\x45\xAB\x00\x00\x00\x00"    # 39 mov [ebp+var_4], 0
            b"\x8D\x4D\xAB"                    # 46 lea ecx, [ebp+fp]
            b"\x6A\x00"                        # 49 push 0
            b"\x6A\x01"                        # 51 push 1
            b"\x6A\x00"                        # 53 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 55 push offset autosaveFileNam
            b"\xE8\xAB\xAB\xAB\xAB"            # 60 call CFile_open
            b"\x8B\x75\xAB"                    # 65 mov esi, [ebp+fp.m_size]
            b"\x56"                            # 68 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 69 call new3
            b"\x83\xC4\x04"                    # 74 add esp, 4
            b"\x8D\x4D\xAB"                    # 77 lea ecx, [ebp+fp]
            b"\x8B\xD8"                        # 80 mov ebx, eax
            b"\x56"                            # 82 push esi
            b"\x53"                            # 83 push ebx
            b"\xE8\xAB\xAB\xAB\xAB"            # 84 call CFile_read
            b"\x8D\x4D\xAB"                    # 89 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 92 call CFile_close
            b"\x6A\x00"                        # 97 push 0
            b"\x6A\x01"                        # 99 push 1
            b"\x68\x01\x10\x00\x00"            # 101 push 1001h
            b"\x68\xAB\xAB\xAB\xAB"            # 106 push offset autosaveFileNa
            b"\xB9\xAB\xAB\xAB\xAB"            # 111 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 116 call CFile_open
            b"\x56"                            # 121 push esi
            b"\x53"                            # 122 push ebx
            b"\xB9\xAB\xAB\xAB\xAB"            # 123 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 128 call CFile_write
            b"\x8B\xC7"                        # 133 mov eax, edi
            b"\x8D\x50\x01"                    # 135 lea edx, [eax+1]
            b"\x8A\x08"                        # 138 mov cl, [eax]
            b"\x40"                            # 140 inc eax
            b"\x84\xC9"                        # 141 test cl, cl
            b"\x75\xF9"                        # 143 jnz short loc_9C1065
            b"\x2B\xC2"                        # 145 sub eax, edx
            b"\xB9\xAB\xAB\xAB\xAB"            # 147 mov ecx, offset saveChatFp
            b"\x40"                            # 152 inc eax
            b"\x50"                            # 153 push eax
            b"\x57"                            # 154 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 155 call CFile_write
            b"\x6A\x02"                        # 160 push 2
            b"\x68\xAB\xAB\xAB\xAB"            # 162 push offset asc_CEBFB8
            b"\xB9\xAB\xAB\xAB\xAB"            # 167 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 172 call CFile_write
            b"\xB9\xAB\xAB\xAB\xAB"            # 177 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 182 call CFile_close
            b"\x53"                            # 187 push ebx
            b"\xE8\xAB\xAB\xAB\xAB"            # 188 call j_j_free
            b"\x83\xC4\x04"                    # 193 add esp, 4
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"    # 196 mov [ebp+var_4], 0FFFFFFFF
            b"\x8D\x4D\xAB"                    # 203 lea ecx, [ebp+fp]
            b"\xE8"                            # 206 call CFile_destructor
        )
        isMasterAidOffset = 13
        autoSaveChatCntOffset = (27, 4)
        fpOffset = (33, 1)
        CFileOffset = 35
        autosaveFileNameOffset1 = (56, 4)
        autosaveFileNameOffset2 = (107, 4)
        openOffsets = (61, 117)
        fpSizeOffset = (67, 1)
        readOffset = 85
        closeOffsets = (93, 183)
        saveChatFpOffsets = (112, 124, 148, 168, 178)
        writeOffsets = (129, 156, 173)
        endCharOffset = (163, 4)
        destructorOffset = 207
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2016-01-06
        # 0  mov esi, ecx
        # 2  mov edi, [ebp+lpBuffer]
        # 5  push 7
        # 7  mov ecx, offset g_session
        # 12 call CSession_IsMasterAid
        # 17 test eax, eax
        # 19 jz loc_8A80D3
        # 25 inc dword ptr [esi+5C8h]
        # 31 lea ecx, [ebp+fp]
        # 37 call CFile_CFile
        # 42 mov [ebp+var_4], 0
        # 49 push 0
        # 51 push 1
        # 53 push 0
        # 55 push offset autosaveFileName
        # 60 lea ecx, [ebp+fp]
        # 66 call CFile_open
        # 71 mov esi, [ebp+size]
        # 77 push esi
        # 78 call new2
        # 83 add esp, 4
        # 86 mov ebx, eax
        # 88 push esi
        # 89 push ebx
        # 90 lea ecx, [ebp+fp]
        # 96 call CFile_read
        # 101 lea ecx, [ebp+fp]
        # 107 call CFile_close
        # 112 push 0
        # 114 push 1
        # 116 push 1001h
        # 121 push offset autosaveFileName
        # 126 mov ecx, offset saveChatFp
        # 131 call CFile_open
        # 136 push esi
        # 137 push ebx
        # 138 mov ecx, offset saveChatFp
        # 143 call CFile_write
        # 148 mov eax, edi
        # 150 lea edx, [eax+1]
        # 153 mov cl, [eax]
        # 155 inc eax
        # 156 test cl, cl
        # 158 jnz short loc_8A8087
        # 160 sub eax, edx
        # 162 inc eax
        # 163 push eax
        # 164 push edi
        # 165 mov ecx, offset saveChatFp
        # 170 call CFile_write
        # 175 push 2
        # 177 push offset asc_BF1270
        # 182 mov ecx, offset saveChatFp
        # 187 call CFile_write
        # 192 mov ecx, offset saveChatFp
        # 197 call CFile_close
        # 202 push ebx
        # 203 call j_??_V@YAXPAX@Z
        # 208 add esp, 4
        # 211 mov [ebp+var_4], 0FFFFFFFFh
        # 218 lea ecx, [ebp+fp]
        # 224 call CFile_destructor
        code = (
            b"\x8B\xF1"                        # 0 mov esi, ecx
            b"\x8B\x7D\xAB"                    # 2 mov edi, [ebp+lpBuffer]
            b"\x6A\x07"                        # 5 push 7
            b"\xB9" + sessionHex +             # 7 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CSession_IsMasterAid
            b"\x85\xC0"                        # 17 test eax, eax
            b"\x0F\x84\xCC\x00\x00\x00"        # 19 jz loc_8A80D3
            b"\xFF\x86\xAB\xAB\x00\x00"        # 25 inc dword ptr [esi+5C8h]
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 31 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 37 call CFile_CFile
            b"\xC7\x45\xAB\x00\x00\x00\x00"    # 42 mov [ebp+var_4], 0
            b"\x6A\x00"                        # 49 push 0
            b"\x6A\x01"                        # 51 push 1
            b"\x6A\x00"                        # 53 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 55 push offset autosaveFileNam
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 60 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 66 call CFile_open
            b"\x8B\xB5\xAB\xAB\xAB\xAB"        # 71 mov esi, [ebp+size]
            b"\x56"                            # 77 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 78 call new2
            b"\x83\xC4\x04"                    # 83 add esp, 4
            b"\x8B\xD8"                        # 86 mov ebx, eax
            b"\x56"                            # 88 push esi
            b"\x53"                            # 89 push ebx
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 90 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 96 call CFile_read
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 101 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 107 call CFile_close
            b"\x6A\x00"                        # 112 push 0
            b"\x6A\x01"                        # 114 push 1
            b"\x68\x01\x10\x00\x00"            # 116 push 1001h
            b"\x68\xAB\xAB\xAB\xAB"            # 121 push offset autosaveFileNa
            b"\xB9\xAB\xAB\xAB\xAB"            # 126 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 131 call CFile_open
            b"\x56"                            # 136 push esi
            b"\x53"                            # 137 push ebx
            b"\xB9\xAB\xAB\xAB\xAB"            # 138 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 143 call CFile_write
            b"\x8B\xC7"                        # 148 mov eax, edi
            b"\x8D\x50\x01"                    # 150 lea edx, [eax+1]
            b"\x8A\x08"                        # 153 mov cl, [eax]
            b"\x40"                            # 155 inc eax
            b"\x84\xC9"                        # 156 test cl, cl
            b"\x75\xF9"                        # 158 jnz short loc_8A8087
            b"\x2B\xC2"                        # 160 sub eax, edx
            b"\x40"                            # 162 inc eax
            b"\x50"                            # 163 push eax
            b"\x57"                            # 164 push edi
            b"\xB9\xAB\xAB\xAB\xAB"            # 165 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 170 call CFile_write
            b"\x6A\x02"                        # 175 push 2
            b"\x68\xAB\xAB\xAB\xAB"            # 177 push offset asc_BF1270
            b"\xB9\xAB\xAB\xAB\xAB"            # 182 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 187 call CFile_write
            b"\xB9\xAB\xAB\xAB\xAB"            # 192 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 197 call CFile_close
            b"\x53"                            # 202 push ebx
            b"\xE8\xAB\xAB\xAB\xAB"            # 203 call j_??_V@YAXPAX@Z
            b"\x83\xC4\x04"                    # 208 add esp, 4
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"    # 211 mov [ebp+var_4], 0FFFFFFFF
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 218 lea ecx, [ebp+fp]
            b"\xE8"                            # 224 call CFile_destructor
        )
        isMasterAidOffset = 13
        autoSaveChatCntOffset = (27, 4)
        fpOffset = (33, 4)
        CFileOffset = 38
        autosaveFileNameOffset1 = (56, 4)
        autosaveFileNameOffset2 = (122, 4)
        openOffsets = (67, 132)
        fpSizeOffset = 0
        readOffset = 97
        closeOffsets = (108, 198)
        saveChatFpOffsets = (127, 139, 166, 183, 193)
        writeOffsets = (144, 171, 188)
        endCharOffset = (178, 4)
        destructorOffset = 225
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2015-01-07
        # 0  mov ebx, [ebp+lpBuffer]
        # 3  mov esi, ecx
        # 5  push 7
        # 7  mov ecx, offset g_session
        # 12 call CSession_IsMasterAid
        # 17 test eax, eax
        # 19 jz loc_812463
        # 25 inc [esi+CGameMode.m_autoSaveChatCnt]
        # 31 lea ecx, [ebp+fp]
        # 37 call CFile_CFile
        # 42 mov [ebp+var_4], 0
        # 49 push 0
        # 51 push 1
        # 53 push 0
        # 55 push offset autosaveFileName
        # 60 lea ecx, [ebp+fp]
        # 66 call CFile_open
        # 71 mov edi, [ebp+size]
        # 77 push edi
        # 78 call new2
        # 83 add esp, 4
        # 86 mov esi, eax
        # 88 push edi
        # 89 push esi
        # 90 lea ecx, [ebp+fp]
        # 96 call CFile_read
        # 101 lea ecx, [ebp+fp]
        # 107 call CFile_close
        # 112 push 0
        # 114 push 1
        # 116 push 1001h
        # 121 push offset autosaveFileName
        # 126 mov ecx, offset saveChatFp
        # 131 call CFile_open
        # 136 push edi
        # 137 push esi
        # 138 mov ecx, offset saveChatFp
        # 143 call CFile_write
        # 148 mov eax, ebx
        # 150 lea edi, [eax+1]
        # 153 mov cl, [eax]
        # 155 inc eax
        # 156 test cl, cl
        # 158 jnz short loc_812417
        # 160 sub eax, edi
        # 162 inc eax
        # 163 push eax
        # 164 push ebx
        # 165 mov ecx, offset saveChatFp
        # 170 call CFile_write
        # 175 push 2
        # 177 push offset asc_AC37A0
        # 182 mov ecx, offset saveChatFp
        # 187 call CFile_write
        # 192 mov ecx, offset saveChatFp
        # 197 call CFile_close
        # 202 push esi
        # 203 call j_??_V@YAXPAX@Z
        # 208 add esp, 4
        # 211 mov [ebp+var_4], 0FFFFFFFFh
        # 218 lea ecx, [ebp+fp]
        # 224 call CFile_destructor
        code = (
            b"\x8B\x5D\xAB"                    # 0 mov ebx, [ebp+lpBuffer]
            b"\x8B\xF1"                        # 3 mov esi, ecx
            b"\x6A\x07"                        # 5 push 7
            b"\xB9" + sessionHex +             # 7 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CSession_IsMasterAid
            b"\x85\xC0"                        # 17 test eax, eax
            b"\x0F\x84\xCC\x00\x00\x00"        # 19 jz loc_812463
            b"\xFF\x86\xAB\xAB\x00\x00"        # 25 inc [esi+CGameMode.m_autoSa
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 31 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 37 call CFile_CFile
            b"\xC7\x45\xAB\x00\x00\x00\x00"    # 42 mov [ebp+var_4], 0
            b"\x6A\x00"                        # 49 push 0
            b"\x6A\x01"                        # 51 push 1
            b"\x6A\x00"                        # 53 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 55 push offset autosaveFileNam
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 60 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 66 call CFile_open
            b"\x8B\xBD\xAB\xAB\xAB\xAB"        # 71 mov edi, [ebp+size]
            b"\x57"                            # 77 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 78 call new2
            b"\x83\xC4\x04"                    # 83 add esp, 4
            b"\x8B\xF0"                        # 86 mov esi, eax
            b"\x57"                            # 88 push edi
            b"\x56"                            # 89 push esi
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 90 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 96 call CFile_read
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 101 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 107 call CFile_close
            b"\x6A\x00"                        # 112 push 0
            b"\x6A\x01"                        # 114 push 1
            b"\x68\x01\x10\x00\x00"            # 116 push 1001h
            b"\x68\xAB\xAB\xAB\xAB"            # 121 push offset autosaveFileNa
            b"\xB9\xAB\xAB\xAB\xAB"            # 126 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 131 call CFile_open
            b"\x57"                            # 136 push edi
            b"\x56"                            # 137 push esi
            b"\xB9\xAB\xAB\xAB\xAB"            # 138 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 143 call CFile_write
            b"\x8B\xC3"                        # 148 mov eax, ebx
            b"\x8D\x78\x01"                    # 150 lea edi, [eax+1]
            b"\x8A\x08"                        # 153 mov cl, [eax]
            b"\x40"                            # 155 inc eax
            b"\x84\xC9"                        # 156 test cl, cl
            b"\x75\xF9"                        # 158 jnz short loc_812417
            b"\x2B\xC7"                        # 160 sub eax, edi
            b"\x40"                            # 162 inc eax
            b"\x50"                            # 163 push eax
            b"\x53"                            # 164 push ebx
            b"\xB9\xAB\xAB\xAB\xAB"            # 165 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 170 call CFile_write
            b"\x6A\x02"                        # 175 push 2
            b"\x68\xAB\xAB\xAB\xAB"            # 177 push offset asc_AC37A0
            b"\xB9\xAB\xAB\xAB\xAB"            # 182 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 187 call CFile_write
            b"\xB9\xAB\xAB\xAB\xAB"            # 192 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 197 call CFile_close
            b"\x56"                            # 202 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 203 call j_??_V@YAXPAX@Z
            b"\x83\xC4\x04"                    # 208 add esp, 4
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"    # 211 mov [ebp+var_4], 0FFFFFFFF
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 218 lea ecx, [ebp+fp]
            b"\xE8"                            # 224 call CFile_destructor
        )
        isMasterAidOffset = 13
        autoSaveChatCntOffset = (27, 4)
        fpOffset = (33, 4)
        CFileOffset = 38
        autosaveFileNameOffset1 = (56, 4)
        autosaveFileNameOffset2 = (122, 4)
        openOffsets = (67, 132)
        fpSizeOffset = 0
        readOffset = 97
        closeOffsets = (108, 198)
        saveChatFpOffsets = (127, 139, 166, 183, 193)
        writeOffsets = (144, 171, 188)
        endCharOffset = (178, 4)
        destructorOffset = 225
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2013-01-03
        # 0  mov ebx, [esp+0B0h+lpBuffer]
        # 7  mov esi, ecx
        # 9  push 7
        # 11 mov ecx, offset g_session
        # 16 call CSession_IsMasterAid
        # 21 test eax, eax
        # 23 jz loc_79F38F
        # 29 inc dword ptr [esi+654h]
        # 35 lea ecx, [esp+0B0h+fp]
        # 39 call CFile_CFile
        # 44 mov [esp+0B0h+var_4], 0
        # 55 push 0
        # 57 push 1
        # 59 push 0
        # 61 push offset autosaveFileName
        # 66 lea ecx, [esp+0C0h+fp]
        # 70 call CFile_open
        # 75 mov edi, [esp+0B0h+size]
        # 79 push edi
        # 80 call new2
        # 85 add esp, 4
        # 88 mov esi, eax
        # 90 push edi
        # 91 push esi
        # 92 lea ecx, [esp+0B8h+fp]
        # 96 call CFile_read
        # 101 lea ecx, [esp+0B0h+fp]
        # 105 call CFile_close
        # 110 push 0
        # 112 push 1
        # 114 push 1001h
        # 119 push offset autosaveFileName
        # 124 mov ecx, offset saveChatFp
        # 129 call CFile_open
        # 134 push edi
        # 135 push esi
        # 136 mov ecx, offset saveChatFp
        # 141 call CFile_write
        # 146 mov eax, ebx
        # 148 lea edi, [eax+1]
        # 151 mov cl, [eax]
        # 153 inc eax
        # 154 test cl, cl
        # 156 jnz short loc_79F341
        # 158 sub eax, edi
        # 160 inc eax
        # 161 push eax
        # 162 push ebx
        # 163 mov ecx, offset saveChatFp
        # 168 call CFile_write
        # 173 push 2
        # 175 push offset asc_9CD254
        # 180 mov ecx, offset saveChatFp
        # 185 call CFile_write
        # 190 mov ecx, offset saveChatFp
        # 195 call CFile_close
        # 200 push esi
        # 201 call j_??_V@YAXPAX@Z
        # 206 add esp, 4
        # 209 mov [esp+0B0h+var_4], 0FFFFFFFFh
        # 220 lea ecx, [esp+0B0h+fp]
        # 224 call CFile_destructor
        code = (
            b"\x8B\x9C\x24\xAB\xAB\xAB\xAB"    # 0 mov ebx, [esp+0B0h+lpBuffer]
            b"\x8B\xF1"                        # 7 mov esi, ecx
            b"\x6A\x07"                        # 9 push 7
            b"\xB9" + sessionHex +             # 11 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 16 call CSession_IsMasterAid
            b"\x85\xC0"                        # 21 test eax, eax
            b"\x0F\x84\xC8\x00\x00\x00"        # 23 jz loc_79F38F
            b"\xFF\x86\xAB\xAB\x00\x00"        # 29 inc dword ptr [esi+654h]
            b"\x8D\x4C\x24\xAB"                # 35 lea ecx, [esp+0B0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 39 call CFile_CFile
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\x00\x00\x00\x00"  # 44 mov [esp+0B0h
            b"\x6A\x00"                        # 55 push 0
            b"\x6A\x01"                        # 57 push 1
            b"\x6A\x00"                        # 59 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 61 push offset autosaveFileNam
            b"\x8D\x4C\x24\xAB"                # 66 lea ecx, [esp+0C0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 70 call CFile_open
            b"\x8B\x7C\x24\xAB"                # 75 mov edi, [esp+0B0h+size]
            b"\x57"                            # 79 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 80 call new2
            b"\x83\xC4\x04"                    # 85 add esp, 4
            b"\x8B\xF0"                        # 88 mov esi, eax
            b"\x57"                            # 90 push edi
            b"\x56"                            # 91 push esi
            b"\x8D\x4C\x24\xAB"                # 92 lea ecx, [esp+0B8h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 96 call CFile_read
            b"\x8D\x4C\x24\xAB"                # 101 lea ecx, [esp+0B0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 105 call CFile_close
            b"\x6A\x00"                        # 110 push 0
            b"\x6A\x01"                        # 112 push 1
            b"\x68\x01\x10\x00\x00"            # 114 push 1001h
            b"\x68\xAB\xAB\xAB\xAB"            # 119 push offset autosaveFileNa
            b"\xB9\xAB\xAB\xAB\xAB"            # 124 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 129 call CFile_open
            b"\x57"                            # 134 push edi
            b"\x56"                            # 135 push esi
            b"\xB9\xAB\xAB\xAB\xAB"            # 136 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 141 call CFile_write
            b"\x8B\xC3"                        # 146 mov eax, ebx
            b"\x8D\x78\x01"                    # 148 lea edi, [eax+1]
            b"\x8A\x08"                        # 151 mov cl, [eax]
            b"\x40"                            # 153 inc eax
            b"\x84\xC9"                        # 154 test cl, cl
            b"\x75\xF9"                        # 156 jnz short loc_79F341
            b"\x2B\xC7"                        # 158 sub eax, edi
            b"\x40"                            # 160 inc eax
            b"\x50"                            # 161 push eax
            b"\x53"                            # 162 push ebx
            b"\xB9\xAB\xAB\xAB\xAB"            # 163 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 168 call CFile_write
            b"\x6A\x02"                        # 173 push 2
            b"\x68\xAB\xAB\xAB\xAB"            # 175 push offset asc_9CD254
            b"\xB9\xAB\xAB\xAB\xAB"            # 180 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 185 call CFile_write
            b"\xB9\xAB\xAB\xAB\xAB"            # 190 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 195 call CFile_close
            b"\x56"                            # 200 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 201 call j_??_V@YAXPAX@Z
            b"\x83\xC4\x04"                    # 206 add esp, 4
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 209 mov [esp+0B0
            b"\x8D\x4C\x24\xAB"                # 220 lea ecx, [esp+0B0h+fp]
            b"\xE8"                            # 224 call CFile_destructor
        )
        isMasterAidOffset = 17
        autoSaveChatCntOffset = (31, 4)
        fpOffset = (38, 1)
        CFileOffset = 40
        autosaveFileNameOffset1 = (62, 4)
        autosaveFileNameOffset2 = (120, 4)
        openOffsets = (71, 130)
        fpSizeOffset = 0
        readOffset = 97
        closeOffsets = (106, 196)
        saveChatFpOffsets = (125, 137, 164, 181, 191)
        writeOffsets = (142, 169)
        endCharOffset = (176, 4)
        destructorOffset = 225
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2011-01-04
        # 0  mov ebx, [esp+0B0h+lpBuffer]
        # 7  mov esi, ecx
        # 9  push 7
        # 11 mov ecx, offset g_session
        # 16 call CSession_IsMasterAid
        # 21 test eax, eax
        # 23 jz loc_5B8FEE
        # 29 inc [esi+CGameMode.m_autoSaveChatCnt]
        # 35 lea ecx, [esp+0B0h+var_A0]
        # 39 call CFile_CFile
        # 44 mov [esp+0B0h+var_4], 0
        # 55 push 0
        # 57 push offset autosaveFileName
        # 62 lea ecx, [esp+0B8h+var_A0]
        # 66 call CFile_open
        # 71 mov edi, [esp+0B0h+size]
        # 75 push edi
        # 76 call new2
        # 81 add esp, 4
        # 84 mov esi, eax
        # 86 push edi
        # 87 push esi
        # 88 lea ecx, [esp+0B8h+var_A0]
        # 92 call CFile_read
        # 97 lea ecx, [esp+0B0h+var_A0]
        # 101 call CFile_close
        # 106 push 1001h
        # 111 push offset autosaveFileName
        # 116 mov ecx, offset saveChatFp
        # 121 call CFile_open
        # 126 push edi
        # 127 push esi
        # 128 mov ecx, offset saveChatFp
        # 133 call CFile_write
        # 138 mov eax, ebx
        # 140 lea edi, [eax+1]
        # 143 lea esp, [esp+0]
        # 150 mov cl, [eax]
        # 152 inc eax
        # 153 test cl, cl
        # 155 jnz short loc_5B8FA0
        # 157 sub eax, edi
        # 159 inc eax
        # 160 push eax
        # 161 push ebx
        # 162 mov ecx, offset saveChatFp
        # 167 call CFile_write
        # 172 push 2
        # 174 push offset asc_7DA444
        # 179 mov ecx, offset saveChatFp
        # 184 call CFile_write
        # 189 mov ecx, offset saveChatFp
        # 194 call CFile_close
        # 199 push esi
        # 200 call ??_V@YAXPAX@Z
        # 205 add esp, 4
        # 208 mov [esp+0B0h+var_4], 0FFFFFFFFh
        # 219 lea ecx, [esp+0B0h+var_A0]
        # 223 call CFile_destructor
        code = (
            b"\x8B\x9C\x24\xAB\xAB\xAB\xAB"    # 0 mov ebx, [esp+0B0h+lpBuffer]
            b"\x8B\xF1"                        # 7 mov esi, ecx
            b"\x6A\x07"                        # 9 push 7
            b"\xB9" + sessionHex +             # 11 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 16 call CSession_IsMasterAid
            b"\x85\xC0"                        # 21 test eax, eax
            b"\x0F\x84\xC7\x00\x00\x00"        # 23 jz loc_5B8FEE
            b"\xFF\x86\xAB\xAB\x00\x00"        # 29 inc [esi+CGameMode.m_autoSa
            b"\x8D\x4C\x24\xAB"                # 35 lea ecx, [esp+0B0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 39 call CFile_CFile
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\x00\x00\x00\x00"  # 44 mov [esp+0B0h
            b"\x6A\x00"                        # 55 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 57 push offset autosaveFileNam
            b"\x8D\x4C\x24\xAB"                # 62 lea ecx, [esp+0B8h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 66 call CFile_open
            b"\x8B\x7C\x24\xAB"                # 71 mov edi, [esp+0B0h+size]
            b"\x57"                            # 75 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 76 call new2
            b"\x83\xC4\x04"                    # 81 add esp, 4
            b"\x8B\xF0"                        # 84 mov esi, eax
            b"\x57"                            # 86 push edi
            b"\x56"                            # 87 push esi
            b"\x8D\x4C\x24\xAB"                # 88 lea ecx, [esp+0B8h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 92 call CFile_read
            b"\x8D\x4C\x24\xAB"                # 97 lea ecx, [esp+0B0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 101 call CFile_close
            b"\x68\x01\x10\x00\x00"            # 106 push 1001h
            b"\x68\xAB\xAB\xAB\xAB"            # 111 push offset autosaveFileNa
            b"\xB9\xAB\xAB\xAB\xAB"            # 116 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 121 call CFile_open
            b"\x57"                            # 126 push edi
            b"\x56"                            # 127 push esi
            b"\xB9\xAB\xAB\xAB\xAB"            # 128 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 133 call CFile_write
            b"\x8B\xC3"                        # 138 mov eax, ebx
            b"\x8D\x78\x01"                    # 140 lea edi, [eax+1]
            b"\x8D\xA4\x24\x00\x00\x00\x00"    # 143 lea esp, [esp+0]
            b"\x8A\x08"                        # 150 mov cl, [eax]
            b"\x40"                            # 152 inc eax
            b"\x84\xC9"                        # 153 test cl, cl
            b"\x75\xF9"                        # 155 jnz short loc_5B8FA0
            b"\x2B\xC7"                        # 157 sub eax, edi
            b"\x40"                            # 159 inc eax
            b"\x50"                            # 160 push eax
            b"\x53"                            # 161 push ebx
            b"\xB9\xAB\xAB\xAB\xAB"            # 162 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 167 call CFile_write
            b"\x6A\x02"                        # 172 push 2
            b"\x68\xAB\xAB\xAB\xAB"            # 174 push offset asc_7DA444
            b"\xB9\xAB\xAB\xAB\xAB"            # 179 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 184 call CFile_write
            b"\xB9\xAB\xAB\xAB\xAB"            # 189 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 194 call CFile_close
            b"\x56"                            # 199 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 200 call ??_V@YAXPAX@Z
            b"\x83\xC4\x04"                    # 205 add esp, 4
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 208 mov [esp+0B0
            b"\x8D\x4C\x24\xAB"                # 219 lea ecx, [esp+0B0h+fp]
            b"\xE8"                            # 223 call CFile_destructor
        )
        isMasterAidOffset = 17
        autoSaveChatCntOffset = (31, 4)
        fpOffset = (38, 1)
        CFileOffset = 40
        autosaveFileNameOffset1 = (58, 4)
        autosaveFileNameOffset2 = (112, 4)
        openOffsets = (67, 122)
        fpSizeOffset = 0
        readOffset = 93
        closeOffsets = (102, 195)
        saveChatFpOffsets = (117, 129, 163, 180, 190)
        writeOffsets = (134, 168)
        endCharOffset = (175, 4)
        destructorOffset = 224
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2011-06-07
        # 0  mov ebx, [esp+0B0h+lpBuffer]
        # 7  mov esi, ecx
        # 9  push 7
        # 11 mov ecx, offset g_session
        # 16 call CSession_IsMasterAid
        # 21 test eax, eax
        # 23 jz loc_680A9E
        # 29 inc dword ptr [esi+63Ch]
        # 35 lea ecx, [esp+0B0h+fp]
        # 39 call CFile_CFile
        # 44 mov [esp+0B0h+var_4], 0
        # 55 push 1
        # 57 push 0
        # 59 push offset autosaveFileName
        # 64 lea ecx, [esp+0BCh+fp]
        # 68 call CFile_open
        # 73 mov edi, [esp+0B0h+size]
        # 77 push edi
        # 78 call new2
        # 83 add esp, 4
        # 86 mov esi, eax
        # 88 push edi
        # 89 push esi
        # 90 lea ecx, [esp+0B8h+fp]
        # 94 call CFile_read
        # 99 lea ecx, [esp+0B0h+fp]
        # 103 call CFile_close
        # 108 push 1
        # 110 push 1001h
        # 115 push offset autosaveFileName
        # 120 mov ecx, offset saveChatFp
        # 125 call CFile_open
        # 130 push edi
        # 131 push esi
        # 132 mov ecx, offset saveChatFp
        # 137 call CFile_write
        # 142 mov eax, ebx
        # 144 lea edi, [eax+1]
        # 147 lea ecx, [ecx+0]
        # 150 mov cl, [eax]
        # 152 inc eax
        # 153 test cl, cl
        # 155 jnz short loc_680A50
        # 157 sub eax, edi
        # 159 inc eax
        # 160 push eax
        # 161 push ebx
        # 162 mov ecx, offset saveChatFp
        # 167 call CFile_write
        # 172 push 2
        # 174 push offset dword_820C94
        # 179 mov ecx, offset saveChatFp
        # 184 call CFile_write
        # 189 mov ecx, offset saveChatFp
        # 194 call CFile_close
        # 199 push esi
        # 200 call ??_V@YAXPAX@Z
        # 205 add esp, 4
        # 208 mov [esp+0B0h+var_4], 0FFFFFFFFh
        # 219 lea ecx, [esp+0B0h+fp]
        # 223 call CFile_destructor
        code = (
            b"\x8B\x9C\x24\xAB\xAB\xAB\xAB"    # 0 mov ebx, [esp+0B0h+lpBuffer]
            b"\x8B\xF1"                        # 7 mov esi, ecx
            b"\x6A\x07"                        # 9 push 7
            b"\xB9" + sessionHex +             # 11 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 16 call CSession_IsMasterAid
            b"\x85\xC0"                        # 21 test eax, eax
            b"\x0F\x84\xC7\x00\x00\x00"        # 23 jz loc_680A9E
            b"\xFF\x86\xAB\xAB\x00\x00"        # 29 inc dword ptr [esi+63Ch]
            b"\x8D\x4C\x24\xAB"                # 35 lea ecx, [esp+0B0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 39 call CFile_CFile
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\x00\x00\x00\x00"  # 44 mov [esp+0B0h
            b"\x6A\x01"                        # 55 push 1
            b"\x6A\x00"                        # 57 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 59 push offset autosaveFileNam
            b"\x8D\x4C\x24\xAB"                # 64 lea ecx, [esp+0BCh+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 68 call CFile_open
            b"\x8B\x7C\x24\xAB"                # 73 mov edi, [esp+0B0h+size]
            b"\x57"                            # 77 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 78 call new2
            b"\x83\xC4\x04"                    # 83 add esp, 4
            b"\x8B\xF0"                        # 86 mov esi, eax
            b"\x57"                            # 88 push edi
            b"\x56"                            # 89 push esi
            b"\x8D\x4C\x24\xAB"                # 90 lea ecx, [esp+0B8h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 94 call CFile_read
            b"\x8D\x4C\x24\xAB"                # 99 lea ecx, [esp+0B0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 103 call CFile_close
            b"\x6A\x01"                        # 108 push 1
            b"\x68\x01\x10\x00\x00"            # 110 push 1001h
            b"\x68\xAB\xAB\xAB\xAB"            # 115 push offset autosaveFileNa
            b"\xB9\xAB\xAB\xAB\xAB"            # 120 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 125 call CFile_open
            b"\x57"                            # 130 push edi
            b"\x56"                            # 131 push esi
            b"\xB9\xAB\xAB\xAB\xAB"            # 132 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 137 call CFile_write
            b"\x8B\xC3"                        # 142 mov eax, ebx
            b"\x8D\x78\x01"                    # 144 lea edi, [eax+1]
            b"\x8D\x49\x00"                    # 147 lea ecx, [ecx+0]
            b"\x8A\x08"                        # 150 mov cl, [eax]
            b"\x40"                            # 152 inc eax
            b"\x84\xC9"                        # 153 test cl, cl
            b"\x75\xF9"                        # 155 jnz short loc_680A50
            b"\x2B\xC7"                        # 157 sub eax, edi
            b"\x40"                            # 159 inc eax
            b"\x50"                            # 160 push eax
            b"\x53"                            # 161 push ebx
            b"\xB9\xAB\xAB\xAB\xAB"            # 162 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 167 call CFile_write
            b"\x6A\x02"                        # 172 push 2
            b"\x68\xAB\xAB\xAB\xAB"            # 174 push offset dword_820C94
            b"\xB9\xAB\xAB\xAB\xAB"            # 179 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 184 call CFile_write
            b"\xB9\xAB\xAB\xAB\xAB"            # 189 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 194 call CFile_close
            b"\x56"                            # 199 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 200 call ??_V@YAXPAX@Z
            b"\x83\xC4\x04"                    # 205 add esp, 4
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 208 mov [esp+0B0
            b"\x8D\x4C\x24\xAB"                # 219 lea ecx, [esp+0B0h+fp]
            b"\xE8"                            # 223 call CFile_destructor
        )
        isMasterAidOffset = 17
        autoSaveChatCntOffset = (31, 4)
        fpOffset = (38, 1)
        CFileOffset = 40
        autosaveFileNameOffset1 = (60, 4)
        autosaveFileNameOffset2 = (116, 4)
        openOffsets = (69, 126)
        fpSizeOffset = 0
        readOffset = 95
        closeOffsets = (104, 195)
        saveChatFpOffsets = (121, 133, 163, 180, 190)
        writeOffsets = (138, 168)
        endCharOffset = (175, 4)
        destructorOffset = 224
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2010-01-05
        # 0  push esi
        # 1  mov esi, ecx
        # 3  push 7
        # 5  mov ecx, offset g_session
        # 10 call CSession_IsMasterAid
        # 15 test eax, eax
        # 17 jz loc_5A66E1
        # 23 mov eax, [esi+528h]
        # 29 push edi
        # 30 inc eax
        # 31 lea ecx, [ebp+fp]
        # 37 mov [esi+528h], eax
        # 43 call CFile_CFile
        # 48 push 0
        # 50 push offset autosaveFileName
        # 55 lea ecx, [ebp+fp]
        # 61 mov [ebp+var_4], 0
        # 68 call CFile_open
        # 73 mov edi, [ebp+size]
        # 79 push edi
        # 80 call ??2@YAPAXI@Z
        # 85 add esp, 4
        # 88 mov esi, eax
        # 90 lea ecx, [ebp+fp]
        # 96 push edi
        # 97 push esi
        # 98 call CFile_read
        # 103 lea ecx, [ebp+fp]
        # 109 call CFile_close
        # 114 push 1001h
        # 119 push offset autosaveFileName
        # 124 mov ecx, offset saveChatFp
        # 129 call CFile_open
        # 134 push edi
        # 135 push esi
        # 136 mov ecx, offset saveChatFp
        # 141 call CFile_write
        # 146 mov edx, [ebp+lpBuffer]
        # 149 or ecx, 0FFFFFFFFh
        # 152 mov edi, edx
        # 154 xor eax, eax
        # 156 repne scasb
        # 158 not ecx
        # 160 push ecx
        # 161 push edx
        # 162 mov ecx, offset saveChatFp
        # 167 call CFile_write
        # 172 push 2
        # 174 push offset asc_782D44
        # 179 mov ecx, offset saveChatFp
        # 184 call CFile_write
        # 189 mov ecx, offset saveChatFp
        # 194 call CFile_close
        # 199 push esi
        # 200 call ??3@YAXPAX@Z
        # 205 add esp, 4
        # 208 lea ecx, [ebp+fp]
        # 214 mov [ebp+var_4], 0FFFFFFFFh
        # 221 call CFile_destructor
        # 226 pop edi
        code = (
            b"\x56"                            # 0 push esi
            b"\x8B\xF1"                        # 1 mov esi, ecx
            b"\x6A\x07"                        # 3 push 7
            b"\xB9" + sessionHex +             # 5 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 10 call CSession_IsMasterAid
            b"\x85\xC0"                        # 15 test eax, eax
            b"\x0F\x84\xCC\x00\x00\x00"        # 17 jz loc_5A66E1
            b"\x8B\x86\xAB\xAB\x00\x00"        # 23 mov eax, [esi+528h]
            b"\x57"                            # 29 push edi
            b"\x40"                            # 30 inc eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 31 lea ecx, [ebp+fp]
            b"\x89\x86\xAB\xAB\x00\x00"        # 37 mov [esi+528h], eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 43 call CFile_CFile
            b"\x6A\x00"                        # 48 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 50 push offset autosaveFileNam
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 55 lea ecx, [ebp+fp]
            b"\xC7\x45\xAB\x00\x00\x00\x00"    # 61 mov [ebp+var_4], 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 68 call CFile_open
            b"\x8B\xBD\xAB\xAB\xAB\xAB"        # 73 mov edi, [ebp+size]
            b"\x57"                            # 79 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 80 call ??2@YAPAXI@Z
            b"\x83\xC4\x04"                    # 85 add esp, 4
            b"\x8B\xF0"                        # 88 mov esi, eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 90 lea ecx, [ebp+fp]
            b"\x57"                            # 96 push edi
            b"\x56"                            # 97 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 98 call CFile_read
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 103 lea ecx, [ebp+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 109 call CFile_close
            b"\x68\x01\x10\x00\x00"            # 114 push 1001h
            b"\x68\xAB\xAB\xAB\xAB"            # 119 push offset autosaveFileNa
            b"\xB9\xAB\xAB\xAB\xAB"            # 124 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 129 call CFile_open
            b"\x57"                            # 134 push edi
            b"\x56"                            # 135 push esi
            b"\xB9\xAB\xAB\xAB\xAB"            # 136 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 141 call CFile_write
            b"\x8B\x55\xAB"                    # 146 mov edx, [ebp+lpBuffer]
            b"\x83\xC9\xFF"                    # 149 or ecx, 0FFFFFFFFh
            b"\x8B\xFA"                        # 152 mov edi, edx
            b"\x33\xC0"                        # 154 xor eax, eax
            b"\xF2\xAE"                        # 156 repne scasb
            b"\xF7\xD1"                        # 158 not ecx
            b"\x51"                            # 160 push ecx
            b"\x52"                            # 161 push edx
            b"\xB9\xAB\xAB\xAB\xAB"            # 162 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 167 call CFile_write
            b"\x6A\x02"                        # 172 push 2
            b"\x68\xAB\xAB\xAB\xAB"            # 174 push offset asc_782D44
            b"\xB9\xAB\xAB\xAB\xAB"            # 179 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 184 call CFile_write
            b"\xB9\xAB\xAB\xAB\xAB"            # 189 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 194 call CFile_close
            b"\x56"                            # 199 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 200 call ??3@YAXPAX@Z
            b"\x83\xC4\x04"                    # 205 add esp, 4
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 208 lea ecx, [ebp+fp]
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"    # 214 mov [ebp+var_4], 0FFFFFFFF
            b"\xE8\xAB\xAB\xAB\xAB"            # 221 call CFile_destructor
            b"\x5F"                            # 226 pop edi
        )
        isMasterAidOffset = 11
        autoSaveChatCntOffset = (25, 4)
        fpOffset = (33, 4)
        CFileOffset = 44
        autosaveFileNameOffset1 = (51, 4)
        autosaveFileNameOffset2 = (120, 4)
        openOffsets = (69, 130)
        fpSizeOffset = 0
        readOffset = 99
        closeOffsets = (110, 195)
        saveChatFpOffsets = (125, 137, 163, 180, 190)
        writeOffsets = (142, 168, 185)
        endCharOffset = (175, 4)
        destructorOffset = 222
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2009-01-13
        # 0  mov ebx, [esp+0B0h+lpBuffer]
        # 7  mov esi, ecx
        # 9  push 7
        # 11 mov ecx, offset g_session
        # 16 call CSession_IsMasterAid
        # 21 test eax, eax
        # 23 jz loc_5771AE
        # 29 inc dword ptr [esi+610h]
        # 35 lea ecx, [esp+0B0h+fp]
        # 39 call CFile_CFile
        # 44 push 0
        # 46 push offset autosaveFileName
        # 51 lea ecx, [esp+0B8h+fp]
        # 55 mov [esp+0B8h+var_4], 0
        # 66 call CFile_open
        # 71 mov edi, [esp+0B0h+size]
        # 75 push edi
        # 76 call new2
        # 81 add esp, 4
        # 84 mov esi, eax
        # 86 push edi
        # 87 push esi
        # 88 lea ecx, [esp+0B8h+fp]
        # 92 call CFile_read
        # 97 lea ecx, [esp+0B0h+fp]
        # 101 call CFile_close
        # 106 push 1001h
        # 111 push offset autosaveFileName
        # 116 mov ecx, offset saveChatFp
        # 121 call CFile_open
        # 126 push edi
        # 127 push esi
        # 128 mov ecx, offset saveChatFp
        # 133 call CFile_write
        # 138 mov eax, ebx
        # 140 lea edi, [eax+1]
        # 143 lea esp, [esp+0]
        # 150 mov cl, [eax]
        # 152 inc eax
        # 153 test cl, cl
        # 155 jnz short loc_577160
        # 157 sub eax, edi
        # 159 inc eax
        # 160 push eax
        # 161 push ebx
        # 162 mov ecx, offset saveChatFp
        # 167 call CFile_write
        # 172 push 2
        # 174 push offset asc_7402BC
        # 179 mov ecx, offset saveChatFp
        # 184 call CFile_write
        # 189 mov ecx, offset saveChatFp
        # 194 call CFile_close
        # 199 push esi
        # 200 call ??_V@YAXPAX@Z
        # 205 add esp, 4
        # 208 lea ecx, [esp+0B0h+fp]
        # 212 mov [esp+0B0h+var_4], 0FFFFFFFFh
        # 223 call CFile_destructor
        code = (
            b"\x8B\x9C\x24\xAB\xAB\xAB\xAB"    # 0 mov ebx, [esp+0B0h+lpBuffer]
            b"\x8B\xF1"                        # 7 mov esi, ecx
            b"\x6A\x07"                        # 9 push 7
            b"\xB9" + sessionHex +             # 11 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 16 call CSession_IsMasterAid
            b"\x85\xC0"                        # 21 test eax, eax
            b"\x0F\x84\xC7\x00\x00\x00"        # 23 jz loc_5771AE
            b"\xFF\x86\xAB\xAB\x00\x00"        # 29 inc dword ptr [esi+610h]
            b"\x8D\x4C\x24\xAB"                # 35 lea ecx, [esp+0B0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 39 call CFile_CFile
            b"\x6A\x00"                        # 44 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 46 push offset autosaveFileNam
            b"\x8D\x4C\x24\xAB"                # 51 lea ecx, [esp+0B8h+fp]
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\x00\x00\x00\x00"  # 55 mov [esp+0B8h
            b"\xE8\xAB\xAB\xAB\xAB"            # 66 call CFile_open
            b"\x8B\x7C\x24\xAB"                # 71 mov edi, [esp+0B0h+size]
            b"\x57"                            # 75 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 76 call new2
            b"\x83\xC4\x04"                    # 81 add esp, 4
            b"\x8B\xF0"                        # 84 mov esi, eax
            b"\x57"                            # 86 push edi
            b"\x56"                            # 87 push esi
            b"\x8D\x4C\x24\xAB"                # 88 lea ecx, [esp+0B8h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 92 call CFile_read
            b"\x8D\x4C\x24\xAB"                # 97 lea ecx, [esp+0B0h+fp]
            b"\xE8\xAB\xAB\xAB\xAB"            # 101 call CFile_close
            b"\x68\x01\x10\x00\x00"            # 106 push 1001h
            b"\x68\xAB\xAB\xAB\xAB"            # 111 push offset autosaveFileNa
            b"\xB9\xAB\xAB\xAB\xAB"            # 116 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 121 call CFile_open
            b"\x57"                            # 126 push edi
            b"\x56"                            # 127 push esi
            b"\xB9\xAB\xAB\xAB\xAB"            # 128 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 133 call CFile_write
            b"\x8B\xC3"                        # 138 mov eax, ebx
            b"\x8D\x78\x01"                    # 140 lea edi, [eax+1]
            b"\x8D\xA4\x24\x00\x00\x00\x00"    # 143 lea esp, [esp+0]
            b"\x8A\x08"                        # 150 mov cl, [eax]
            b"\x40"                            # 152 inc eax
            b"\x84\xC9"                        # 153 test cl, cl
            b"\x75\xF9"                        # 155 jnz short loc_577160
            b"\x2B\xC7"                        # 157 sub eax, edi
            b"\x40"                            # 159 inc eax
            b"\x50"                            # 160 push eax
            b"\x53"                            # 161 push ebx
            b"\xB9\xAB\xAB\xAB\xAB"            # 162 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 167 call CFile_write
            b"\x6A\x02"                        # 172 push 2
            b"\x68\xAB\xAB\xAB\xAB"            # 174 push offset asc_7402BC
            b"\xB9\xAB\xAB\xAB\xAB"            # 179 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 184 call CFile_write
            b"\xB9\xAB\xAB\xAB\xAB"            # 189 mov ecx, offset saveChatFp
            b"\xE8\xAB\xAB\xAB\xAB"            # 194 call CFile_close
            b"\x56"                            # 199 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 200 call ??_V@YAXPAX@Z
            b"\x83\xC4\x04"                    # 205 add esp, 4
            b"\x8D\x4C\x24\xAB"                # 208 lea ecx, [esp+0B0h+fp]
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 212 mov [esp+0B0
            b"\xE8"                            # 223 call CFile_destructor
        )
        isMasterAidOffset = 17
        autoSaveChatCntOffset = (31, 4)
        fpOffset = (38, 1)
        CFileOffset = 40
        autosaveFileNameOffset1 = (47, 4)
        autosaveFileNameOffset2 = (112, 4)
        openOffsets = (67, 122)
        fpSizeOffset = 0
        readOffset = 93
        closeOffsets = (102, 195)
        saveChatFpOffsets = (117, 129, 163, 180, 190)
        writeOffsets = (134, 168, 185)
        endCharOffset = (175, 4)
        destructorOffset = 224
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("Error: failed in seach CFile")
        exit(1)

    endChar = self.getVarAddr(offset, endCharOffset)
    tmpAddr, _ = self.exe.vaToRawUnknown(endChar)
    chars = self.exe.readUWord(tmpAddr)
    if chars != 0x0a0d:
        self.log("Error: found wrong end char: {0}".format(hex(chars)))
        exit(1)

    self.CSession_IsMasterAid = self.getAddr(offset,
                                             isMasterAidOffset,
                                             isMasterAidOffset + 4)
    self.addRawFunc("CSession::IsMasterAid", self.CSession_IsMasterAid)
    chatCnt = self.getVarAddr(offset, autoSaveChatCntOffset)
    self.addStruct("CGameMode")
    self.addStructMember("m_autoSaveChatCnt", chatCnt, 4, True)
    fp = self.getVarAddr(offset, fpOffset)
    self.CFile_CFile = self.getAddr(offset,
                                    CFileOffset,
                                    CFileOffset + 4)
    self.addRawFunc("CFile::CFile", self.CFile_CFile)
    autosaveFileName1 = self.getVarAddr(offset, autosaveFileNameOffset1)
    autosaveFileName2 = self.getVarAddr(offset, autosaveFileNameOffset2)
    if autosaveFileName1 != autosaveFileName2:
        self.log("Error: found different autosaveFileName")
        exit(1)
    self.autoSaveFileName = autosaveFileName1
    self.addVaVar("autoSaveFileName", self.autoSaveFileName)

    addr = self.getAddrList(offset, openOffsets)
    if addr is False:
        self.log("Error: found different CFile::open")
        exit(1)
    self.CFile_open = addr
    self.CFile_openVa = self.exe.rawToVa(self.CFile_open)
    self.addRawFunc("CFile::open", self.CFile_open)
    if fpSizeOffset != 0:
        fpSize = self.getVarAddr(offset, fpSizeOffset) - fp
        self.addStruct("CFile")
        self.addStructMember("m_size", fpSize, 4, True)
    self.CFile_read = self.getAddr(offset,
                                   readOffset,
                                   readOffset + 4)
    self.addRawFunc("CFile::read", self.CFile_read)
    addr = self.getAddrList(offset, closeOffsets)
    if addr is False:
        self.log("Error: found different CFile::close")
        exit(1)
    self.CFile_close = addr
    self.addRawFunc("CFile::close", self.CFile_close)
    saveChatFp1 = 0
    for saveChatFpOffset in saveChatFpOffsets:
        saveChatFp2 = self.exe.readUInt(offset + saveChatFpOffset)
        if saveChatFp1 == 0:
            saveChatFp1 = saveChatFp2
        elif saveChatFp1 != saveChatFp2:
            self.log("Error: found different saveChatFp")
            exit(1)
    self.saveChatFp = saveChatFp1
    self.addVaVar("saveChatFp", self.saveChatFp)
    addr = self.getAddrList(offset, writeOffsets)
    if addr is False:
        self.log("Error: found different CFile::write")
        exit(1)
    self.CFile_write = addr
    self.addRawFunc("CFile::write", self.CFile_write)
    self.CFile_destructor = self.getAddr(offset,
                                         destructorOffset,
                                         destructorOffset + 4)
    if self.CFile_destructor == self.CFile_close and \
       self.packetVersion < "20050000":
        # exception for old clients
        self.CFile_destructor = 0
    else:
        self.addRawFunc("CFile::destructor", self.CFile_destructor)
