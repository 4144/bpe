#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# search in mainWndProc


def searchConstructArray(self):
    # for < 2014 no stdstring_constructor function (using imports)
    if self.stdstring_constructor == 0:
        self.log("Skipping search construct_array")
        return

    strConstHex = self.exe.toHex(self.exe.rawToVa(self.stdstring_constructor),
                                 4)
    langHex = self.exe.toHex(self.gLanguage, 4)

    # 2019
    # 0  jz loc_AC2D0F
    # 6  mov ecx, g_language
    # 12 cmp [ecx+CLanguage.m_candidateCount], 0
    # 16 jz loc_AC2CC3
    # 22 push offset std_string_destructor
    # 27 push offset std_string_constructor
    # 32 push 9
    # 34 push 18h
    # 36 lea eax, [ebp+strings]
    # 42 push eax
    # 43 call construct_array
    code = (
        b"\x0F\x84\xAB\xAB\xAB\xAB"        # 0 jz loc_AC2D0F
        b"\x8B\x0D" + langHex +            # 6 mov ecx, g_language
        b"\x83\x79\xAB\x00"                # 12 cmp [ecx+CLanguage.m_candidateC
        b"\x0F\x84\xAB\xAB\xAB\xAB"        # 16 jz loc_AC2CC3
        b"\x68\xAB\xAB\xAB\xAB"            # 22 push offset std_string_destruct
        b"\x68" + strConstHex +            # 27 push offset std_string_construc
        b"\x6A\x09"                        # 32 push 9
        b"\x6A\xAB"                        # 34 push 18h
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 36 lea eax, [ebp+strings]
        b"\x50"                            # 42 push eax
        b"\xE8"                            # 43 call construct_array
    )
    candidatesCountOffset = (14, 1)
    strSizeOffset = (35, 1)
    arrayOffset = 44
    destructorOffset = 23
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2015-01-07
        # 0  jz loc_91EC5F
        # 6  mov ecx, g_language
        # 12 cmp [ecx+CLanguage.m_candidateCount], 0
        # 16 jz loc_91EC0F
        # 22 push offset std_string_destructor
        # 27 push offset std_string_constructor
        # 32 push 9
        # 34 push 1Ch
        # 36 lea edx, [ebp+strings]
        # 42 push edx
        # 43 call construct_array
        code = (
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 0 jz loc_91EC5F
            b"\x8B\x0D" + langHex +            # 6 mov ecx, g_language
            b"\x83\x79\xAB\x00"                # 12 cmp [ecx+CLanguage.m_candid
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 16 jz loc_91EC0F
            b"\x68\xAB\xAB\xAB\xAB"            # 22 push offset std_string_dest
            b"\x68" + strConstHex +            # 27 push offset std_string_cons
            b"\x6A\x09"                        # 32 push 9
            b"\x6A\xAB"                        # 34 push 1Ch
            b"\x8D\x95\xAB\xAB\xAB\xAB"        # 36 lea edx, [ebp+strings]
            b"\x52"                            # 42 push edx
            b"\xE8"                            # 43 call construct_array
        )
        candidatesCountOffset = (14, 1)
        strSizeOffset = (35, 1)
        arrayOffset = 44
        destructorOffset = 23
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("Error: construct_array not found")
        exit(1)

    strSize = self.getVarAddr(offset, strSizeOffset)
    if self.stdstringSize != -1 and self.stdstringSize != strSize:
        self.log("Error: found two different std::string struct sizes: "
                 "{0} vs {1}".format(self.stdstringSize, strSize))
    self.stdstringSize = strSize
    self.constructArray = self.getAddr(offset, arrayOffset, arrayOffset + 4)
    self.addRawFuncType("construct_array",
                        self.constructArray,
                        "signed int __stdcall construct_array(void *object, "
                        "int bytes_step, int members_count, "
                        "void (__thiscall *constructor)(void *), "
                        "void (__thiscall *destructor)(void *))")
    self.stdstring_destructor = self.exe.readUInt(offset + destructorOffset)
    self.addVaFunc("std::string::destructor", self.stdstring_destructor)
    candidatesCount = self.getVarAddr(offset, candidatesCountOffset)
    self.addStruct("CLanguage")
    self.addStructMember("m_candidateCount", candidatesCount, 4, True)

    strEnd = strSize - 4
    if self.stdstringAllocLen != strEnd:
        self.addStruct("struct_std_string")
        self.addStructMember("padding_end", strEnd, 4, False)
