#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchErrorMsg(self, errorExit):
    # seach for "Failed to load Winsock library!"
    offset, section = self.exe.string(b"Failed to load Winsock library!")
    if offset is False:
        self.log("failed in search 'Failed to load Winsock library!'")
        if errorExit is True:
            exit(1)
        return

    errorMsgStr = section.rawToVa(offset)

    # 0 push offset "Failed to load Winsock library!"
    # 5 call ErrorMsg
    code = (
        b"\x68" + self.exe.toHex(errorMsgStr, 4) +
        b"\xE8"
    )

    offset = self.exe.code(code)
    if offset is False:
        self.log("failed in search ErrorMsg")
        if errorExit is True and self.exe.asprotect is False:
            exit(1)
        return
    self.errorMsg = self.getAddr(offset, 6, 6 + 4)
    self.addRawFunc("ErrorMsg", self.errorMsg)
