#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchRenderer(self, errorExit):
    soundMgrHex = self.exe.toHex(self.soundMgr, 4)

    # search first block in CMode::RunFadeOut

    # 0  mov ecx, g_soundMgr
    # 6  push esi
    # 7  call CSoundMgr_fadeOut
    # 12 mov esi, timeGetTime
    # 18 call esi ; timeGetTime
    # 20 mov ecx, g_renderer
    # 26 mov dwFadeStart, eax
    # 31 call CRenderer_BackupFrame
    # 36 call esi ; timeGetTime
    # 38 sub eax, dwFadeStart
    # 44 cmp eax, 0FFh
    # 49 jnb loc_72005C
    code = (
        b"\x8B\x0D" + soundMgrHex +        # 0 mov ecx, g_soundMgr
        b"\x56"                            # 6 push esi
        b"\xE8\xAB\xAB\xAB\xAB"            # 7 call CSoundMgr_fadeOut
        b"\x8B\x35\xAB\xAB\xAB\xAB"        # 12 mov esi, timeGetTime
        b"\xFF\xD6"                        # 18 call esi ; timeGetTime
        b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 20 mov ecx, g_renderer
        b"\xA3\xAB\xAB\xAB\xAB"            # 26 mov dwFadeStart, eax
        b"\xE8\xAB\xAB\xAB\xAB"            # 31 call CRenderer_BackupFrame
        b"\xFF\xD6"                        # 36 call esi ; timeGetTime
        b"\x2B\x05\xAB\xAB\xAB\xAB"        # 38 sub eax, dwFadeStart
        b"\x3D\xFF\x00\x00\x00"            # 44 cmp eax, 0FFh
        b"\x0F\x83\xAB\xAB\x00\x00"        # 49 jnb loc_72005C
    )
    g_rendererOffset = 22
    CSoundMgr_fadeOutOffset = 8
    dwFadeStartOffset1 = 27
    dwFadeStartOffset2 = 40
    CRenderer_BacFrameOffset = 32
    timeGetTimeOffset = 14
    FadeBgmOffset = 0
    fadeoutFlagOffset = 0

    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2015-01-07
        # 0  lea eax, [ebp+hFadeBgmThread]
        # 3  push eax
        # 4  push 0
        # 6  push 0
        # 8  push offset FadeBgm
        # 13 push 0
        # 15 push 0
        # 17 call _beginthreadex
        # 23 mov esi, timeGetTime
        # 29 add esp, 18h
        # 32 call esi
        # 34 mov ecx, g_renderer
        # 40 mov dwFadeStart, eax
        # 45 call CRenderer_BackupFrame
        # 50 call esi
        # 52 sub eax, dwFadeStart
        # 58 cmp eax, 0FFh
        # 63 jnb loc_61227E
        code = (
            b"\x8D\x45\xAB"                    # 0 lea eax, [ebp+hFadeBgmThread
            b"\x50"                            # 3 push eax
            b"\x6A\x00"                        # 4 push 0
            b"\x6A\x00"                        # 6 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 8 push offset FadeBgm
            b"\x6A\x00"                        # 13 push 0
            b"\x6A\x00"                        # 15 push 0
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 17 call _beginthreadex
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 23 mov esi, timeGetTime
            b"\x83\xC4\x18"                    # 29 add esp, 18h
            b"\xFF\xD6"                        # 32 call esi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 34 mov ecx, g_renderer
            b"\xA3\xAB\xAB\xAB\xAB"            # 40 mov dwFadeStart, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call CRenderer_BackupFrame
            b"\xFF\xD6"                        # 50 call esi
            b"\x2B\x05\xAB\xAB\xAB\xAB"        # 52 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 58 cmp eax, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 63 jnb loc_61227E
        )
        g_rendererOffset = 36
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 41
        dwFadeStartOffset2 = 54
        CRenderer_BacFrameOffset = 46
        timeGetTimeOffset = 25
        FadeBgmOffset = 9
        fadeoutFlagOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov esi, g_soundMgr
        # 6  push edi
        # 7  mov edi, ecx
        # 9  mov ecx, esi
        # 11 call CSoundMgr_sub_437520
        # 16 push eax
        # 17 mov ecx, esi
        # 19 call CSoundMgr_sub_437C30
        # 24 lea eax, [ebp+var_4]
        # 27 push eax
        # 28 push 0
        # 30 push 0
        # 32 push offset FadeBgm
        # 37 push 0
        # 39 push 0
        # 41 call _beginthreadex
        # 47 mov ebx, timeGetTime
        # 53 add esp, 18h
        # 56 call ebx
        # 58 mov dwFadeStart, eax
        # 63 mov [edi+CMode.fadeoutFlag], 1
        # 67 call ebx
        # 69 mov esi, eax
        # 71 sub esi, dwFadeStart
        # 77 cmp esi, 0FFh
        # 83 jnb loc_6294BC
        code = (
            b"\x8B\x35" + soundMgrHex +        # 0 mov esi, g_soundMgr
            b"\x57"                            # 6 push edi
            b"\x8B\xF9"                        # 7 mov edi, ecx
            b"\x8B\xCE"                        # 9 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 11 call CSoundMgr_sub_437520
            b"\x50"                            # 16 push eax
            b"\x8B\xCE"                        # 17 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 19 call CSoundMgr_sub_437C30
            b"\x8D\x45\xAB"                    # 24 lea eax, [ebp+var_4]
            b"\x50"                            # 27 push eax
            b"\x6A\x00"                        # 28 push 0
            b"\x6A\x00"                        # 30 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 32 push offset FadeBgm
            b"\x6A\x00"                        # 37 push 0
            b"\x6A\x00"                        # 39 push 0
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 41 call _beginthreadex
            b"\x8B\x1D\xAB\xAB\xAB\xAB"        # 47 mov ebx, timeGetTime
            b"\x83\xC4\x18"                    # 53 add esp, 18h
            b"\xFF\xD3"                        # 56 call ebx
            b"\xA3\xAB\xAB\xAB\xAB"            # 58 mov dwFadeStart, eax
            b"\xC6\x47\xAB\x01"                # 63 mov [edi+CMode.fadeoutFlag]
            b"\xFF\xD3"                        # 67 call ebx
            b"\x8B\xF0"                        # 69 mov esi, eax
            b"\x2B\x35\xAB\xAB\xAB\xAB"        # 71 sub esi, dwFadeStart
            b"\x81\xFE\xFF\x00\x00\x00"        # 77 cmp esi, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 83 jnb loc_6294BC
        )
        g_rendererOffset = 0
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 59
        dwFadeStartOffset2 = 73
        CRenderer_BacFrameOffset = 0
        timeGetTimeOffset = 49
        FadeBgmOffset = 33
        fadeoutFlagOffset = 65
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov ecx, g_soundMgr
        # 6  mov esi, ecx
        # 8  call CSoundMgr_sub_431350
        # 13 push eax
        # 14 mov ecx, esi
        # 16 call CSoundMgr_sub_431280
        # 21 lea eax, [ebp+var_4]
        # 24 push eax
        # 25 push 0
        # 27 push 0
        # 29 push offset FadeBgm
        # 34 push 0
        # 36 push 0
        # 38 call _beginthreadex
        # 44 mov ebx, timeGetTime
        # 50 add esp, 18h
        # 53 call ebx
        # 55 mov dwFadeStart, eax
        # 60 mov [edi+CMode.fadeoutFlag], 1
        # 64 call ebx
        # 66 mov esi, eax
        # 68 sub esi, dwFadeStart
        # 74 cmp esi, 0FFh
        # 80 jnb loc_60D560
        code = (
            b"\x8B\x0D" + soundMgrHex +        # 0 mov ecx, g_soundMgr
            b"\x8B\xF1"                        # 6 mov esi, ecx
            b"\xE8\xAB\xAB\xAB\xAB"            # 8 call CSoundMgr_sub_431350
            b"\x50"                            # 13 push eax
            b"\x8B\xCE"                        # 14 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 16 call CSoundMgr_sub_431280
            b"\x8D\x45\xAB"                    # 21 lea eax, [ebp+var_4]
            b"\x50"                            # 24 push eax
            b"\x6A\x00"                        # 25 push 0
            b"\x6A\x00"                        # 27 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 29 push offset FadeBgm
            b"\x6A\x00"                        # 34 push 0
            b"\x6A\x00"                        # 36 push 0
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 38 call _beginthreadex
            b"\x8B\x1D\xAB\xAB\xAB\xAB"        # 44 mov ebx, timeGetTime
            b"\x83\xC4\x18"                    # 50 add esp, 18h
            b"\xFF\xD3"                        # 53 call ebx
            b"\xA3\xAB\xAB\xAB\xAB"            # 55 mov dwFadeStart, eax
            b"\xC6\x47\xAB\x01"                # 60 mov [edi+CMode.fadeoutFlag]
            b"\xFF\xD3"                        # 64 call ebx
            b"\x8B\xF0"                        # 66 mov esi, eax
            b"\x2B\x35\xAB\xAB\xAB\xAB"        # 68 sub esi, dwFadeStart
            b"\x81\xFE\xFF\x00\x00\x00"        # 74 cmp esi, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 80 jnb loc_60D560
        )
        g_rendererOffset = 0
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 56
        dwFadeStartOffset2 = 70
        CRenderer_BacFrameOffset = 0
        timeGetTimeOffset = 46
        FadeBgmOffset = 30
        fadeoutFlagOffset = 62
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov ecx, g_soundMgr
        # 6  push edi
        # 7  call CSoundMgr_fadeOut
        # 12 mov edi, timeGetTime
        # 18 call edi
        # 20 mov ecx, g_renderer
        # 26 mov dwFadeStart, eax
        # 31 call CRenderer_BackupFrame
        # 36 call edi
        # 38 sub eax, dwFadeStart
        # 44 cmp eax, 0FFh
        # 49 jnb loc_60D76E
        code = (
            b"\x8B\x0D" + soundMgrHex +        # 0 mov ecx, g_soundMgr
            b"\x57"                            # 6 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 7 call CSoundMgr_fadeOut
            b"\x8B\x3D\xAB\xAB\xAB\xAB"        # 12 mov edi, timeGetTime
            b"\xFF\xD7"                        # 18 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 20 mov ecx, g_renderer
            b"\xA3\xAB\xAB\xAB\xAB"            # 26 mov dwFadeStart, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 31 call CRenderer_BackupFrame
            b"\xFF\xD7"                        # 36 call edi
            b"\x2B\x05\xAB\xAB\xAB\xAB"        # 38 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 44 cmp eax, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 49 jnb loc_60D76E
        )
        g_rendererOffset = 22
        CSoundMgr_fadeOutOffset = 8
        dwFadeStartOffset1 = 27
        dwFadeStartOffset2 = 40
        CRenderer_BacFrameOffset = 32
        timeGetTimeOffset = 14
        FadeBgmOffset = 0
        fadeoutFlagOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  lea eax, [esp+10h+hFadeBgmThread]
        # 4  push eax
        # 5  push 0
        # 7  push 0
        # 9  push offset FadeBgm
        # 14 push 0
        # 16 push 0
        # 18 mov edi, ecx
        # 20 call _beginthreadex
        # 26 mov ebp, timeGetTime
        # 32 add esp, 18h
        # 35 call ebp
        # 37 mov dwFadeStart, eax
        # 42 mov byte ptr [edi+14h], 1
        # 46 call ebp
        # 48 mov esi, eax
        # 50 sub esi, dwFadeStart
        # 56 cmp esi, 0FFh
        # 62 jnb loc_594E3F
        code = (
            b"\x8D\x44\x24\xAB"                # 0 lea eax, [esp+10h+hFadeBgmTh
            b"\x50"                            # 4 push eax
            b"\x6A\x00"                        # 5 push 0
            b"\x6A\x00"                        # 7 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 9 push offset FadeBgm
            b"\x6A\x00"                        # 14 push 0
            b"\x6A\x00"                        # 16 push 0
            b"\x8B\xF9"                        # 18 mov edi, ecx
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 20 call _beginthreadex
            b"\x8B\x2D\xAB\xAB\xAB\xAB"        # 26 mov ebp, timeGetTime
            b"\x83\xC4\x18"                    # 32 add esp, 18h
            b"\xFF\xD5"                        # 35 call ebp
            b"\xA3\xAB\xAB\xAB\xAB"            # 37 mov dwFadeStart, eax
            b"\xC6\x47\xAB\x01"                # 42 mov byte ptr [edi+14h], 1
            b"\xFF\xD5"                        # 46 call ebp
            b"\x8B\xF0"                        # 48 mov esi, eax
            b"\x2B\x35\xAB\xAB\xAB\xAB"        # 50 sub esi, dwFadeStart
            b"\x81\xFE\xFF\x00\x00\x00"        # 56 cmp esi, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 62 jnb loc_594E3F
        )
        g_rendererOffset = 0
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 38
        dwFadeStartOffset2 = 52
        CRenderer_BacFrameOffset = 0
        timeGetTimeOffset = 28
        FadeBgmOffset = 10
        fadeoutFlagOffset = 44
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  lea eax, [esp+8+var_4]
        # 4  push eax
        # 5  push 0
        # 7  push 0
        # 9  push offset FadeBgm
        # 14 push 0
        # 16 push 0
        # 18 call _beginthreadex
        # 24 mov esi, timeGetTime
        # 30 add esp, 18h
        # 33 call esi
        # 35 mov ecx, g_renderer
        # 41 mov dwFadeStart, eax
        # 46 call CRenderer_BackupFrame
        # 51 call esi
        # 53 sub eax, dwFadeStart
        # 59 cmp eax, 0FFh
        # 64 jnb loc_590A7E
        code = (
            b"\x8D\x44\x24\xAB"                # 0 lea eax, [esp+8+var_4]
            b"\x50"                            # 4 push eax
            b"\x6A\x00"                        # 5 push 0
            b"\x6A\x00"                        # 7 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 9 push offset FadeBgm
            b"\x6A\x00"                        # 14 push 0
            b"\x6A\x00"                        # 16 push 0
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 18 call _beginthreadex
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 24 mov esi, timeGetTime
            b"\x83\xC4\x18"                    # 30 add esp, 18h
            b"\xFF\xD6"                        # 33 call esi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 35 mov ecx, g_renderer
            b"\xA3\xAB\xAB\xAB\xAB"            # 41 mov dwFadeStart, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 46 call CRenderer_BackupFrame
            b"\xFF\xD6"                        # 51 call esi
            b"\x2B\x05\xAB\xAB\xAB\xAB"        # 53 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 59 cmp eax, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 64 jnb loc_590A7E
        )
        g_rendererOffset = 37
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 42
        dwFadeStartOffset2 = 55
        CRenderer_BacFrameOffset = 47
        timeGetTimeOffset = 26
        FadeBgmOffset = 10
        fadeoutFlagOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  lea eax, [ebp+var_4]
        # 3  push eax
        # 4  push 0
        # 6  push 0
        # 8  push offset FadeBgm
        # 13 push 0
        # 15 push 0
        # 17 mov edi, ecx
        # 19 call _beginthreadex
        # 25 mov ebx, timeGetTime
        # 31 add esp, 18h
        # 34 call ebx
        # 36 mov dwFadeStart, eax
        # 41 mov byte ptr [edi+14h], 1
        # 45 call ebx
        # 47 mov esi, eax
        # 49 sub esi, dwFadeStart
        # 55 cmp esi, 0FFh
        # 61 jnb loc_5B9E00
        code = (
            b"\x8D\x45\xAB"                    # 0 lea eax, [ebp+var_4]
            b"\x50"                            # 3 push eax
            b"\x6A\x00"                        # 4 push 0
            b"\x6A\x00"                        # 6 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 8 push offset FadeBgm
            b"\x6A\x00"                        # 13 push 0
            b"\x6A\x00"                        # 15 push 0
            b"\x8B\xF9"                        # 17 mov edi, ecx
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 19 call _beginthreadex
            b"\x8B\x1D\xAB\xAB\xAB\xAB"        # 25 mov ebx, timeGetTime
            b"\x83\xC4\x18"                    # 31 add esp, 18h
            b"\xFF\xD3"                        # 34 call ebx
            b"\xA3\xAB\xAB\xAB\xAB"            # 36 mov dwFadeStart, eax
            b"\xC6\x47\xAB\x01"                # 41 mov byte ptr [edi+14h], 1
            b"\xFF\xD3"                        # 45 call ebx
            b"\x8B\xF0"                        # 47 mov esi, eax
            b"\x2B\x35\xAB\xAB\xAB\xAB"        # 49 sub esi, dwFadeStart
            b"\x81\xFE\xFF\x00\x00\x00"        # 55 cmp esi, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 61 jnb loc_5B9E00
        )
        g_rendererOffset = 0
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 37
        dwFadeStartOffset2 = 51
        CRenderer_BacFrameOffset = 0
        timeGetTimeOffset = 27
        FadeBgmOffset = 9
        fadeoutFlagOffset = 43
        offset = self.exe.codeWildcard(code, b"\xAB")

        # 0  lea eax, [ebp+ThreadId]
        # 3  push esi
        # 4  push eax
        # 5  push 0
        # 7  push 0
        # 9  push offset FadeBgm
        # 14 push 0
        # 16 push 0
        # 18 call __beginthreadex
        # 23 mov esi, ds:timeGetTime
        # 29 add esp, 18h
        # 32 call esi
        # 34 mov ecx, g_renderer
        # 40 mov dwFadeStart, eax
        # 45 call CRenderer_BackupFrame
        # 50 call esi
        # 52 sub eax, dwFadeStart
        # 58 cmp eax, 0FFh
        # 63 jnb loc_51DE7B
        code = (
            b"\x8D\x45\xAB"                    # 0 lea eax, [ebp+ThreadId]
            b"\x56"                            # 3 push esi
            b"\x50"                            # 4 push eax
            b"\x6A\x00"                        # 5 push 0
            b"\x6A\x00"                        # 7 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 9 push offset FadeBgm
            b"\x6A\x00"                        # 14 push 0
            b"\x6A\x00"                        # 16 push 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 18 call __beginthreadex
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 23 mov esi, ds:timeGetTime
            b"\x83\xC4\x18"                    # 29 add esp, 18h
            b"\xFF\xD6"                        # 32 call esi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 34 mov ecx, g_renderer
            b"\xA3\xAB\xAB\xAB\xAB"            # 40 mov dwFadeStart, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call CRenderer_BackupFrame
            b"\xFF\xD6"                        # 50 call esi
            b"\x2B\x05\xAB\xAB\xAB\xAB"        # 52 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 58 cmp eax, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 63 jnb loc_51DE7B
        )
        g_rendererOffset = 36
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 41
        dwFadeStartOffset2 = 54
        CRenderer_BacFrameOffset = 46
        timeGetTimeOffset = 25
        FadeBgmOffset = 10
        fadeoutFlagOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  lea eax, [esp+8+ThreadId]
        # 4  push eax
        # 5  push 0
        # 7  push 0
        # 9  push offset FadeBgm
        # 14 push 0
        # 16 push 0
        # 18 call __beginthreadex
        # 23 mov esi, ds:timeGetTime
        # 29 add esp, 18h
        # 32 call esi
        # 34 mov ecx, g_renderer
        # 40 mov dwFadeStart, eax
        # 45 call CRenderer_BackupFrame
        # 50 call esi
        # 52 sub eax, dwFadeStart
        # 58 cmp eax, 0FFh
        # 63 jnb loc_53E6B0
        code = (
            b"\x8D\x44\x24\xAB"                # 0 lea eax, [esp+8+ThreadId]
            b"\x50"                            # 4 push eax
            b"\x6A\x00"                        # 5 push 0
            b"\x6A\x00"                        # 7 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 9 push offset FadeBgm
            b"\x6A\x00"                        # 14 push 0
            b"\x6A\x00"                        # 16 push 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 18 call __beginthreadex
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 23 mov esi, ds:timeGetTime
            b"\x83\xC4\x18"                    # 29 add esp, 18h
            b"\xFF\xD6"                        # 32 call esi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 34 mov ecx, g_renderer
            b"\xA3\xAB\xAB\xAB\xAB"            # 40 mov dwFadeStart, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call CRenderer_BackupFrame
            b"\xFF\xD6"                        # 50 call esi
            b"\x2B\x05\xAB\xAB\xAB\xAB"        # 52 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 58 cmp eax, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 63 jnb loc_53E6B0
        )
        g_rendererOffset = 36
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 41
        dwFadeStartOffset2 = 54
        CRenderer_BacFrameOffset = 46
        timeGetTimeOffset = 25
        FadeBgmOffset = 10
        fadeoutFlagOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  lea eax, [ebp+ThreadId]
        # 3  push edi
        # 4  push eax
        # 5  push 0
        # 7  push 0
        # 9  push offset FadeBgm
        # 14 push 0
        # 16 push 0
        # 18 call __beginthreadex
        # 23 mov edi, ds:timeGetTime
        # 29 add esp, 18h
        # 32 call edi
        # 34 mov ecx, g_renderer
        # 40 mov dwFadeStart, eax
        # 45 call CRenderer_BackupFrame
        # 50 call edi
        # 52 mov ecx, dwFadeStart
        # 58 mov esi, eax
        # 60 sub esi, ecx
        # 62 cmp esi, 0FFh
        # 68 jnb loc_4D4DF5
        code = (
            b"\x8D\x45\xAB"                    # 0 lea eax, [ebp+ThreadId]
            b"\x57"                            # 3 push edi
            b"\x50"                            # 4 push eax
            b"\x6A\x00"                        # 5 push 0
            b"\x6A\x00"                        # 7 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 9 push offset FadeBgm
            b"\x6A\x00"                        # 14 push 0
            b"\x6A\x00"                        # 16 push 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 18 call __beginthreadex
            b"\x8B\x3D\xAB\xAB\xAB\xAB"        # 23 mov edi, ds:timeGetTime
            b"\x83\xC4\x18"                    # 29 add esp, 18h
            b"\xFF\xD7"                        # 32 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 34 mov ecx, g_renderer
            b"\xA3\xAB\xAB\xAB\xAB"            # 40 mov dwFadeStart, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call CRenderer_BackupFrame
            b"\xFF\xD7"                        # 50 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 52 mov ecx, dwFadeStart
            b"\x8B\xF0"                        # 58 mov esi, eax
            b"\x2B\xF1"                        # 60 sub esi, ecx
            b"\x81\xFE\xFF\x00\x00\x00"        # 62 cmp esi, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 68 jnb loc_4D4DF5
        )
        g_rendererOffset = 36
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 41
        dwFadeStartOffset2 = 54
        CRenderer_BacFrameOffset = 46
        timeGetTimeOffset = 25
        FadeBgmOffset = 10
        fadeoutFlagOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  lea eax, [ebp+var_4]
        # 3  push eax
        # 4  push 0
        # 6  push 0
        # 8  push offset FadeBgm
        # 13 push 0
        # 15 push 0
        # 17 mov edi, ecx
        # 19 call _beginthreadex
        # 25 mov ebx, timeGetTime
        # 31 add esp, 18h
        # 34 call ebx
        # 36 mov dwFadeStart, eax
        # 41 mov byte ptr [edi+14h], 1
        # 45 call ebx
        # 47 mov esi, eax
        # 49 sub esi, dwFadeStart
        # 55 cmp esi, 0FFh
        # 61 jnb loc_5B9E00
        code = (
            b"\x8D\x45\xAB"                    # 0 lea eax, [ebp+var_4]
            b"\x50"                            # 3 push eax
            b"\x6A\x00"                        # 4 push 0
            b"\x6A\x00"                        # 6 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 8 push offset FadeBgm
            b"\x6A\x00"                        # 13 push 0
            b"\x6A\x00"                        # 15 push 0
            b"\x8B\xF9"                        # 17 mov edi, ecx
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 19 call _beginthreadex
            b"\x8B\x1D\xAB\xAB\xAB\xAB"        # 25 mov ebx, timeGetTime
            b"\x83\xC4\x18"                    # 31 add esp, 18h
            b"\xFF\xD3"                        # 34 call ebx
            b"\xA3\xAB\xAB\xAB\xAB"            # 36 mov dwFadeStart, eax
            b"\xC6\x47\xAB\x01"                # 41 mov byte ptr [edi+14h], 1
            b"\xFF\xD3"                        # 45 call ebx
            b"\x8B\xF0"                        # 47 mov esi, eax
            b"\x2B\x35\xAB\xAB\xAB\xAB"        # 49 sub esi, dwFadeStart
            b"\x81\xFE\xFF\x00\x00\x00"        # 55 cmp esi, 0FFh
            b"\x0F\x83\xAB\xAB\x00\x00"        # 61 jnb loc_5B9E00
        )
        g_rendererOffset = 0
        CSoundMgr_fadeOutOffset = 0
        dwFadeStartOffset1 = 37
        dwFadeStartOffset2 = 51
        CRenderer_BacFrameOffset = 0
        timeGetTimeOffset = 27
        FadeBgmOffset = 9
        fadeoutFlagOffset = 43
        offset = self.exe.codeWildcard(code, b"\xAB")


    if offset is False:
        self.log("failed in search g_renderer.")
        if errorExit is True:
            exit(1)
        return

    offset1 = offset
    self.timeGetTime = self.exe.readUInt(offset + timeGetTimeOffset)
    self.addVaVar("timeGetTime", self.timeGetTime)
    if g_rendererOffset != 0:
        self.gRenderer = self.exe.readUInt(offset + g_rendererOffset)
        self.addVaVar("g_renderer", self.gRenderer)
    self.dwFadeStart = self.exe.readUInt(offset + dwFadeStartOffset1)
    self.addVaVar("dwFadeStart", self.dwFadeStart)
    if self.dwFadeStart != self.exe.readUInt(offset + dwFadeStartOffset2):
        self.log("Error: found different dwFadeStart")
        exit(1)
    if CSoundMgr_fadeOutOffset != 0:
        self.CSoundMgr_fadeOut = self.getAddr(offset,
                                              CSoundMgr_fadeOutOffset,
                                              CSoundMgr_fadeOutOffset + 4)
        self.addRawFunc("CSoundMgr::fadeOut", self.CSoundMgr_fadeOut)
    if CRenderer_BacFrameOffset != 0:
        self.CRenderer_BackupFrame = self.getAddr(offset,
                                                  CRenderer_BacFrameOffset,
                                                  CRenderer_BacFrameOffset + 4)
        self.addRawFunc("CRenderer::BackupFrame", self.CRenderer_BackupFrame)
    if FadeBgmOffset != 0:
        self.FadeBgm = self.exe.readUInt(offset + FadeBgmOffset)
        self.addVaFunc("FadeBgm", self.FadeBgm)
    if fadeoutFlagOffset != 0:
        fadeOutFlag = self.exe.readUByte(offset + fadeoutFlagOffset)
        self.addStruct("CMode")
        self.addStructMember("fadeOutFlag", fadeOutFlag, 1, True)

    # search for DrawBoxScreen and fields below

    if g_rendererOffset != 0:
        gRendererHex = self.exe.toHex(self.gRenderer, 4)
    else:
        gRendererHex = b"\xAB\xAB\xAB\xAB"

    # 0  shl eax, 18h
    # 3  push eax
    # 4  mov eax, g_renderer
    # 9  push [eax+CRenderer.m_height]
    # 12 push [eax+CRenderer.m_width]
    # 15 push 0
    # 17 push 0
    # 19 call DrawBoxScreen
    # 24 add esp, 14h
    code = (
        b"\xC1\xE0\x18"                    # 0 shl eax, 18h
        b"\x50"                            # 3 push eax
        b"\xA1" + gRendererHex +           # 4 mov eax, g_renderer
        b"\xFF\x70\xAB"                    # 9 push [eax+CRenderer.m_height]
        b"\xFF\x70\xAB"                    # 12 push [eax+CRenderer.m_width]
        b"\x6A\x00"                        # 15 push 0
        b"\x6A\x00"                        # 17 push 0
        b"\xE8"                            # 19 call DrawBoxScreen
    )
    heightOffset = 11
    widthOffset = 14
    drawBoxScreenOffset = 20
    g_rendererOffset = 0

    offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x60)
    if offset is False:
        # 0  shl eax, 18h
        # 3  push eax
        # 4  mov eax, g_renderer
        # 9  mov ecx, [eax+28h]
        # 12 mov edx, [eax+24h]
        # 15 push ecx
        # 16 push edx
        # 17 push 0
        # 19 push 0
        # 21 call DrawBoxScreen
        code = (
            b"\xC1\xE0\x18"                    # 0 shl eax, 18h
            b"\x50"                            # 3 push eax
            b"\xA1" + gRendererHex +           # 4 mov eax, g_renderer
            b"\x8B\x48\xAB"                    # 9 mov ecx, [eax+28h]
            b"\x8B\x50\xAB"                    # 12 mov edx, [eax+24h]
            b"\x51"                            # 15 push ecx
            b"\x52"                            # 16 push edx
            b"\x6A\x00"                        # 17 push 0
            b"\x6A\x00"                        # 19 push 0
            b"\xE8"                            # 21 call DrawBoxScreen
        )
        heightOffset = 11
        widthOffset = 14
        drawBoxScreenOffset = 22
        g_rendererOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x60)

    if offset is False:
        # 0  mov eax, [edi+CMode.vptr]
        # 2  mov ecx, edi
        # 4  call dword ptr [eax+10h]
        # 7  mov eax, g_renderer
        # 12 shl esi, 18h
        # 15 push esi
        # 16 push [eax+CRenderer.m_height]
        # 19 push [eax+CRenderer.m_width]
        # 22 push 0
        # 24 push 0
        # 26 call DrawBoxScreen
        code = (
            b"\x8B\xAB"                        # 0 mov eax, [edi+CMode.vptr]
            b"\x8B\xCF"                        # 2 mov ecx, edi
            b"\xFF\x50\xAB"                    # 4 call dword ptr [eax+10h]
            b"\xA1\xAB\xAB\xAB\xAB"            # 7 mov eax, g_renderer
            b"\xC1\xE6\x18"                    # 12 shl esi, 18h
            b"\x56"                            # 15 push esi
            b"\xFF\x70\xAB"                    # 16 push [eax+CRenderer.m_heigh
            b"\xFF\x70\xAB"                    # 19 push [eax+CRenderer.m_width
            b"\x6A\x00"                        # 22 push 0
            b"\x6A\x00"                        # 24 push 0
            b"\xE8"                            # 26 call DrawBoxScreen
        )
        heightOffset = 18
        widthOffset = 21
        drawBoxScreenOffset = 27
        g_rendererOffset = 8
        offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x60)

    if offset is False:
        # 0  mov edx, [edi+CMode.vptr]
        # 2  mov eax, [edx+10h]
        # 5  mov ecx, edi
        # 7  call eax
        # 9  mov eax, g_renderer
        # 14 mov ecx, [eax+CRenderer.m_height]
        # 17 mov edx, [eax+CRenderer.m_width]
        # 20 shl esi, 18h
        # 23 push esi
        # 24 push ecx
        # 25 push edx
        # 26 push 0
        # 28 push 0
        # 30 call DrawBoxScreen
        code = (
            b"\x8B\x17"                        # 0 mov edx, [edi+CMode.vptr]
            b"\x8B\x42\xAB"                    # 2 mov eax, [edx+10h]
            b"\x8B\xCF"                        # 5 mov ecx, edi
            b"\xFF\xD0"                        # 7 call eax
            b"\xA1" + gRendererHex +           # 9 mov eax, g_renderer
            b"\x8B\x48\xAB"                    # 14 mov ecx, [eax+CRenderer.m_h
            b"\x8B\x50\xAB"                    # 17 mov edx, [eax+CRenderer.m_w
            b"\xC1\xE6\x18"                    # 20 shl esi, 18h
            b"\x56"                            # 23 push esi
            b"\x51"                            # 24 push ecx
            b"\x52"                            # 25 push edx
            b"\x6A\x00"                        # 26 push 0
            b"\x6A\x00"                        # 28 push 0
            b"\xE8"                            # 30 call DrawBoxScreen
        )
        heightOffset = 16
        widthOffset = 19
        drawBoxScreenOffset = 31
        g_rendererOffset = 10
        offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x60)

    if offset is False:
        # 0  mov ecx, g_renderer
        # 6  call sub_40D400
        # 11 test eax, eax
        # 13 jz short loc_4D4DD1
        # 15 mov eax, g_renderer
        # 20 push 0
        # 22 shl esi, 18h
        # 25 mov ecx, [eax+28h]
        # 28 mov edx, [eax+24h]
        # 31 push 0
        # 33 push esi
        # 34 push ecx
        # 35 push edx
        # 36 push 0
        # 38 push 0
        # 40 call DrawBoxScreen
        code = (
            b"\x8B\x0D" + gRendererHex +       # 0 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 6 call sub_40D400
            b"\x85\xC0"                        # 11 test eax, eax
            b"\x74\xAB"                        # 13 jz short loc_4D4DD1
            b"\xA1" + gRendererHex +           # 15 mov eax, g_renderer
            b"\x6A\x00"                        # 20 push 0
            b"\xC1\xE6\x18"                    # 22 shl esi, 18h
            b"\x8B\x48\xAB"                    # 25 mov ecx, [eax+28h]
            b"\x8B\x50\xAB"                    # 28 mov edx, [eax+24h]
            b"\x6A\x00"                        # 31 push 0
            b"\x56"                            # 33 push esi
            b"\x51"                            # 34 push ecx
            b"\x52"                            # 35 push edx
            b"\x6A\x00"                        # 36 push 0
            b"\x6A\x00"                        # 38 push 0
            b"\xE8"                            # 40 call DrawBoxScreen
        )
        heightOffset = 27
        widthOffset = 30
        drawBoxScreenOffset = 41
        g_rendererOffset = 16
        offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x60)

    if offset is False:
        self.log("failed in search DrawBoxScreen.")
        if errorExit is True:
            exit(1)
        return

    offset2 = offset
    width = self.exe.readUByte(offset + widthOffset)
    height = self.exe.readUByte(offset + heightOffset)
    self.addStruct("CRenderer")
    self.addStructMember("m_width", width, 4, True)
    self.addStructMember("m_height", height, 4, True)
    self.DrawBoxScreen = self.getAddr(offset,
                                      drawBoxScreenOffset,
                                      drawBoxScreenOffset + 4)
    self.addRawFunc("DrawBoxScreen", self.DrawBoxScreen)
    if g_rendererOffset != 0:
        self.gRenderer = self.exe.readUInt(offset + g_rendererOffset)
        self.addVaVar("g_renderer", self.gRenderer)
        gRendererHex = self.exe.toHex(self.gRenderer, 4)


    # search rag logo address
    offset, section = self.exe.string(b"rag_logo.bmp")
    if offset is False:
        self.log("failed in search rag_logo.bmp")
        exit(1)
    ragLogoHex = self.exe.toHex(section.rawToVa(offset), 4)

    gWindowMgrHex = self.exe.toHex(self.gWindowMgr, 4)
    dwFadeStartHex = self.exe.toHex(self.dwFadeStart, 4)

    # search in CMode_RunFadeOut
    # 0  push offset aRag_logo_bmp
    # 5  call g_resMgr
    # 10 mov ecx, eax
    # 12 call CResMgr_Get
    # 17 mov ecx, g_renderer
    # 23 push eax
    # 24 mov eax, [ecx+CRenderer.m_height]
    # 27 sub eax, 7Ah
    # 30 push eax
    # 31 mov eax, [ecx+CRenderer.m_width]
    # 34 sub eax, 0C0h
    # 39 push eax
    # 40 mov ecx, offset g_windowMgr
    # 45 call UIWindowMgr_DrawBitmapToFrame
    # 50 mov ecx, g_renderer
    # 56 call CRenderer_DrawScene
    # 61 test al, al
    # 63 jz short loc_72003D
    # 65 mov ecx, g_renderer
    # 71 push 1
    # 73 call CRenderer_Flip
    # 78 mov ecx, g_renderer
    # 84 call CRenderer_RestoreFrame
    # 89 call esi ; timeGetTime
    # 91 sub eax, dwFadeStart
    # 97 cmp eax, 0FFh
    # 102 jb loc_71FFD0
    code = (
        b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
        b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
        b"\x8B\xC8"                        # 10 mov ecx, eax
        b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
        b"\x8B\x0D" + gRendererHex +       # 17 mov ecx, g_renderer
        b"\x50"                            # 23 push eax
        b"\x8B\x41" + self.exe.toHexB(height) +  # 24 mov eax, [ecx+CRen.m_heig
        b"\x83\xE8\x7A"                    # 27 sub eax, 7Ah
        b"\x50"                            # 30 push eax
        b"\x8B\x41" + self.exe.toHexB(width) +  # 31 mov eax, [ecx+CRende.m_wid
        b"\x2D\xC0\x00\x00\x00"            # 34 sub eax, 0C0h
        b"\x50"                            # 39 push eax
        b"\xB9" + gWindowMgrHex +          # 40 mov ecx, offset g_windowMgr
        b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmapToFr
        b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
        b"\xE8\xAB\xAB\xAB\xAB"            # 56 call CRenderer_DrawScene
        b"\x84\xC0"                        # 61 test al, al
        b"\x74\x0D"                        # 63 jz short loc_72003D
        b"\x8B\x0D" + gRendererHex +       # 65 mov ecx, g_renderer
        b"\x6A\x01"                        # 71 push 1
        b"\xE8\xAB\xAB\xAB\xAB"            # 73 call CRenderer_Flip
        b"\x8B\x0D" + gRendererHex +       # 78 mov ecx, g_renderer
        b"\xE8\xAB\xAB\xAB\xAB"            # 84 call CRenderer_RestoreFrame
        b"\xFF\xD6"                        # 89 call esi ; timeGetTime
        b"\x2B\x05" + dwFadeStartHex +     # 91 sub eax, dwFadeStart
        b"\x3D\xFF\x00\x00\x00"            # 97 cmp eax, 0FFh
        b"\x0F\x82\xAB\xAB\xFF\xFF"        # 102 jb loc_71FFD0
    )
    g_resMgrOffset = 6
    CResMgr_GetOffset = 13
    UIWMgr_DrawBTFOffset = 46
    CRenderer_DrawSceneOffset = 57
    CRenderer_FlipOffset = 74
    CRenderer_RestoreFrmOffset = 85

    offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)
    if offset is False:
        # 0  push offset aRag_logo_bmp
        # 5  call g_resMgr
        # 10 mov ecx, eax
        # 12 call CResMgr_Get
        # 17 push eax
        # 18 mov eax, g_renderer
        # 23 mov ecx, [eax+CRenderer.m_height]
        # 26 mov edx, [eax+CRenderer.m_width]
        # 29 sub ecx, 7Ah
        # 32 push ecx
        # 33 sub edx, 0C0h
        # 39 push edx
        # 40 mov ecx, offset g_windowMgr
        # 45 call UIWindowMgr_DrawBitmapToFrame
        # 50 mov ecx, g_renderer
        # 56 call CRenderer_DrawScene
        # 61 test al, al
        # 63 jz short loc_61225F
        # 65 mov ecx, g_renderer
        # 71 push 1
        # 73 call CRenderer_Flip
        # 78 mov ecx, g_renderer
        # 84 call CRenderer_RestoreFrame
        # 89 call esi
        # 91 sub eax, dwFadeStart
        # 97 cmp eax, 0FFh
        # 102 jb loc_6121F0
        code = (
            b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
            b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
            b"\x8B\xC8"                        # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
            b"\x50"                            # 17 push eax
            b"\xA1" + gRendererHex +           # 18 mov eax, g_renderer
            b"\x8B\x48" + self.exe.toHexB(height) +  # 23 mov ecx, [eax+CRender
            b"\x8B\x50" + self.exe.toHexB(width) +  # 26 mov edx, [eax+CRendere
            b"\x83\xE9\x7A"                    # 29 sub ecx, 7Ah
            b"\x51"                            # 32 push ecx
            b"\x81\xEA\xC0\x00\x00\x00"        # 33 sub edx, 0C0h
            b"\x52"                            # 39 push edx
            b"\xB9" + gWindowMgrHex +          # 40 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmap
            b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 56 call CRenderer_DrawScene
            b"\x84\xC0"                        # 61 test al, al
            b"\x74\x0D"                        # 63 jz short loc_61225F
            b"\x8B\x0D" + gRendererHex +       # 65 mov ecx, g_renderer
            b"\x6A\x01"                        # 71 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 73 call CRenderer_Flip
            b"\x8B\x0D" + gRendererHex +       # 78 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 84 call CRenderer_RestoreFrame
            b"\xFF\xD6"                        # 89 call esi
            b"\x2B\x05" + dwFadeStartHex +     # 91 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 97 cmp eax, 0FFh
            b"\x0F\x82\xAB\xAB\xFF\xFF"        # 102 jb loc_6121F0
        )
        g_resMgrOffset = 6
        CResMgr_GetOffset = 13
        UIWMgr_DrawBTFOffset = 46
        CRenderer_DrawSceneOffset = 57
        CRenderer_FlipOffset = 74
        CRenderer_RestoreFrmOffset = 85
        offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)

    if offset is False:
        # 0  push offset aRag_logo_bmp
        # 5  call g_resMgr
        # 10 mov ecx, eax
        # 12 call CResMgr_Get
        # 17 mov ecx, g_renderer
        # 23 push eax
        # 24 mov eax, [ecx+28h]
        # 27 sub eax, 7Ah
        # 30 push eax
        # 31 mov eax, [ecx+24h]
        # 34 sub eax, 0C0h
        # 39 push eax
        # 40 mov ecx, offset g_windowMgr
        # 45 call UIWindowMgr_DrawBitmapToFrame
        # 50 mov ecx, g_renderer
        # 56 call CRenderer_DrawScene
        # 61 test al, al
        # 63 jz short loc_6294A6
        # 65 mov ecx, g_renderer
        # 71 push 1
        # 73 call CRenderer_Flip
        # 78 call ebx
        # 80 mov esi, eax
        # 82 sub esi, dwFadeStart
        # 88 cmp esi, 0FFh
        # 94 jb loc_629430
        code = (
            b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
            b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
            b"\x8B\xC8"                        # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
            b"\x8B\x0D" + gRendererHex +       # 17 mov ecx, g_renderer
            b"\x50"                            # 23 push eax
            b"\x8B\x41" + self.exe.toHexB(height) +  # 24 mov eax, [ecx+28h]
            b"\x83\xE8\x7A"                    # 27 sub eax, 7Ah
            b"\x50"                            # 30 push eax
            b"\x8B\x41" + self.exe.toHexB(width) +   # 31 mov eax, [ecx+24h]
            b"\x2D\xC0\x00\x00\x00"            # 34 sub eax, 0C0h
            b"\x50"                            # 39 push eax
            b"\xB9" + gWindowMgrHex +          # 40 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmap
            b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 56 call CRenderer_DrawScene
            b"\x84\xC0"                        # 61 test al, al
            b"\x74\x0D"                        # 63 jz short loc_6294A6
            b"\x8B\x0D" + gRendererHex +       # 65 mov ecx, g_renderer
            b"\x6A\x01"                        # 71 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 73 call CRenderer_Flip
            b"\xFF\xD3"                        # 78 call ebx
            b"\x8B\xF0"                        # 80 mov esi, eax
            b"\x2B\x35" + dwFadeStartHex +     # 82 sub esi, dwFadeStart
            b"\x81\xFE\xFF\x00\x00\x00"        # 88 cmp esi, 0FFh
            b"\x0F\x82\xAB\xAB\xFF\xFF"        # 94 jb loc_629430
        )
        g_resMgrOffset = 6
        CResMgr_GetOffset = 13
        UIWMgr_DrawBTFOffset = 46
        CRenderer_DrawSceneOffset = 57
        CRenderer_FlipOffset = 74
        CRenderer_RestoreFrmOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)

    if offset is False:
        # 0  push offset aRag_logo_bmp
        # 5  call g_resMgr
        # 10 mov ecx, eax
        # 12 call CResMgr_Get
        # 17 push eax
        # 18 mov eax, g_renderer
        # 23 mov ecx, [eax+CRenderer.m_height]
        # 26 mov edx, [eax+CRenderer.m_width]
        # 29 sub ecx, 7Ah
        # 32 push ecx
        # 33 sub edx, 0C0h
        # 39 push edx
        # 40 mov ecx, offset g_windowMgr
        # 45 call UIWindowMgr_DrawBitmapToFrame
        # 50 mov ecx, g_renderer
        # 56 call CRenderer_DrawScene
        # 61 test al, al
        # 63 jz short loc_60D54A
        # 65 mov ecx, g_renderer
        # 71 push 1
        # 73 call CRenderer_Flip
        # 78 call ebx
        # 80 mov esi, eax
        # 82 sub esi, dwFadeStart
        # 88 cmp esi, 0FFh
        # 94 jb loc_60D4D0
        code = (
            b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
            b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
            b"\x8B\xC8"                        # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
            b"\x50"                            # 17 push eax
            b"\xA1" + gRendererHex +           # 18 mov eax, g_renderer
            b"\x8B\x48" + self.exe.toHexB(height) +  # 23 mov ecx, [eax+CRender
            b"\x8B\x50" + self.exe.toHexB(width) +  # 26 mov edx, [eax+CRendere
            b"\x83\xE9\x7A"                    # 29 sub ecx, 7Ah
            b"\x51"                            # 32 push ecx
            b"\x81\xEA\xC0\x00\x00\x00"        # 33 sub edx, 0C0h
            b"\x52"                            # 39 push edx
            b"\xB9" + gWindowMgrHex +          # 40 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmap
            b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 56 call CRenderer_DrawScene
            b"\x84\xC0"                        # 61 test al, al
            b"\x74\x0D"                        # 63 jz short loc_60D54A
            b"\x8B\x0D" + gRendererHex +       # 65 mov ecx, g_renderer
            b"\x6A\x01"                        # 71 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 73 call CRenderer_Flip
            b"\xFF\xD3"                        # 78 call ebx
            b"\x8B\xF0"                        # 80 mov esi, eax
            b"\x2B\x35" + dwFadeStartHex +     # 82 sub esi, dwFadeStart
            b"\x81\xFE\xFF\x00\x00\x00"        # 88 cmp esi, 0FFh
            b"\x0F\x82\xAB\xAB\xFF\xFF"        # 94 jb loc_60D4D0
        )
        g_resMgrOffset = 6
        CResMgr_GetOffset = 13
        UIWMgr_DrawBTFOffset = 46
        CRenderer_DrawSceneOffset = 57
        CRenderer_FlipOffset = 74
        CRenderer_RestoreFrmOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)

    if offset is False:
        # 0  push offset aRag_logo_bmp
        # 5  call g_resMgr
        # 10 mov ecx, eax
        # 12 call CResMgr_Get
        # 17 push eax
        # 18 mov eax, g_renderer
        # 23 mov ecx, [eax+CRenderer.m_height]
        # 26 mov edx, [eax+CRenderer.m_width]
        # 29 sub ecx, 7Ah
        # 32 push ecx
        # 33 sub edx, 0C0h
        # 39 push edx
        # 40 mov ecx, offset g_windowMgr
        # 45 call UIWindowMgr_DrawBitmapToFrame
        # 50 mov ecx, g_renderer
        # 56 call CRenderer_DrawScene
        # 61 test al, al
        # 63 jz short loc_60D74F
        # 65 mov ecx, g_renderer
        # 71 push 1
        # 73 call CRenderer_Flip
        # 78 mov ecx, g_renderer
        # 84 call CRenderer_RestoreFrame
        # 89 call edi
        # 91 sub eax, dwFadeStart
        # 97 cmp eax, 0FFh
        # 102 jb loc_60D6E0
        code = (
            b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
            b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
            b"\x8B\xC8"                        # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
            b"\x50"                            # 17 push eax
            b"\xA1" + gRendererHex +           # 18 mov eax, g_renderer
            b"\x8B\x48" + self.exe.toHexB(height) +  # 23 mov ecx, [eax+CRender
            b"\x8B\x50" + self.exe.toHexB(width) +  # 26 mov edx, [eax+CRendere
            b"\x83\xE9\x7A"                    # 29 sub ecx, 7Ah
            b"\x51"                            # 32 push ecx
            b"\x81\xEA\xC0\x00\x00\x00"        # 33 sub edx, 0C0h
            b"\x52"                            # 39 push edx
            b"\xB9" + gWindowMgrHex +          # 40 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmap
            b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 56 call CRenderer_DrawScene
            b"\x84\xC0"                        # 61 test al, al
            b"\x74\x0D"                        # 63 jz short loc_60D74F
            b"\x8B\x0D" + gRendererHex +       # 65 mov ecx, g_renderer
            b"\x6A\x01"                        # 71 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 73 call CRenderer_Flip
            b"\x8B\x0D" + gRendererHex +       # 78 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 84 call CRenderer_RestoreFrame
            b"\xFF\xD7"                        # 89 call edi
            b"\x2B\x05" + dwFadeStartHex +     # 91 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 97 cmp eax, 0FFh
            b"\x0F\x82\xAB\xAB\xFF\xFF"        # 102 jb loc_60D6E0
        )
        g_resMgrOffset = 6
        CResMgr_GetOffset = 13
        UIWMgr_DrawBTFOffset = 46
        CRenderer_DrawSceneOffset = 57
        CRenderer_FlipOffset = 74
        CRenderer_RestoreFrmOffset = 85
        offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)

    if offset is False:
        # 0  push offset aRag_logo_bmp
        # 5  call g_resMgr
        # 10 mov ecx, eax
        # 12 call CResMgr_Get
        # 17 push eax
        # 18 mov eax, g_renderer
        # 23 mov ecx, [eax+28h]
        # 26 mov edx, [eax+24h]
        # 29 sub ecx, 7Ah
        # 32 push ecx
        # 33 sub edx, 0C0h
        # 39 push edx
        # 40 mov ecx, offset g_windowMgr
        # 45 call UIWindowMgr_DrawBitmapToFrame
        # 50 mov ecx, g_renderer
        # 56 call CRenderer_DrawScene
        # 61 test al, al
        # 63 jz short loc_594E28
        # 65 mov ecx, g_renderer
        # 71 push 1
        # 73 call CRenderer_Flip
        # 78 call ebp
        # 80 mov esi, eax
        # 82 sub esi, dwFadeStart
        # 88 cmp esi, 0FFh
        # 94 jb loc_594DB0
        code = (
            b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
            b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
            b"\x8B\xC8"                        # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
            b"\x50"                            # 17 push eax
            b"\xA1" + gRendererHex +           # 18 mov eax, g_renderer
            b"\x8B\x48" + self.exe.toHexB(height) +  # 23 mov ecx, [eax+28h]
            b"\x8B\x50" + self.exe.toHexB(width) +  # 26 mov edx, [eax+24h]
            b"\x83\xE9\x7A"                    # 29 sub ecx, 7Ah
            b"\x51"                            # 32 push ecx
            b"\x81\xEA\xC0\x00\x00\x00"        # 33 sub edx, 0C0h
            b"\x52"                            # 39 push edx
            b"\xB9" + gWindowMgrHex +          # 40 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmap
            b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 56 call CRenderer_DrawScene
            b"\x84\xC0"                        # 61 test al, al
            b"\x74\x0D"                        # 63 jz short loc_594E28
            b"\x8B\x0D" + gRendererHex +       # 65 mov ecx, g_renderer
            b"\x6A\x01"                        # 71 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 73 call CRenderer_Flip
            b"\xFF\xD5"                        # 78 call ebp
            b"\x8B\xF0"                        # 80 mov esi, eax
            b"\x2B\x35" + dwFadeStartHex +     # 82 sub esi, dwFadeStart
            b"\x81\xFE\xFF\x00\x00\x00"        # 88 cmp esi, 0FFh
            b"\x0F\x82\xAB\xAB\xFF\xFF"        # 94 jb loc_594DB0
        )
        g_resMgrOffset = 6
        CResMgr_GetOffset = 13
        UIWMgr_DrawBTFOffset = 46
        CRenderer_DrawSceneOffset = 57
        CRenderer_FlipOffset = 74
        CRenderer_RestoreFrmOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)

    if offset is False:
        # 0  push offset aRag_logo_bmp
        # 5  call g_resMgr
        # 10 mov ecx, eax
        # 12 call CResMgr_Get
        # 17 push eax
        # 18 mov eax, g_renderer
        # 23 mov ecx, [eax+28h]
        # 26 mov edx, [eax+24h]
        # 29 sub ecx, 7Ah
        # 32 sub edx, 0C0h
        # 38 push ecx
        # 39 push edx
        # 40 mov ecx, offset g_windowMgr
        # 45 call UIWindowMgr_DrawBitmapToFrame
        # 50 mov ecx, g_renderer
        # 56 call CRenderer_DrawScene
        # 61 test al, al
        # 63 jz short loc_51DE5C
        # 65 mov ecx, g_renderer
        # 71 push 1
        # 73 call CRenderer_Flip
        # 78 mov ecx, g_renderer
        # 84 call CRenderer_RestoreFrame
        # 89 call esi
        # 91 sub eax, dwFadeStart
        # 97 cmp eax, 0FFh
        # 102 jb loc_51DDED
        code = (
            b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
            b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
            b"\x8B\xC8"                        # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
            b"\x50"                            # 17 push eax
            b"\xA1" + gRendererHex +           # 18 mov eax, g_renderer
            b"\x8B\x48" + self.exe.toHexB(height) +  # 23 mov ecx, [eax+28h]
            b"\x8B\x50" + self.exe.toHexB(width) +  # 26 mov edx, [eax+24h]
            b"\x83\xE9\x7A"                    # 29 sub ecx, 7Ah
            b"\x81\xEA\xC0\x00\x00\x00"        # 32 sub edx, 0C0h
            b"\x51"                            # 38 push ecx
            b"\x52"                            # 39 push edx
            b"\xB9" + gWindowMgrHex +          # 40 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmap
            b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 56 call CRenderer_DrawScene
            b"\x84\xC0"                        # 61 test al, al
            b"\x74\x0D"                        # 63 jz short loc_51DE5C
            b"\x8B\x0D" + gRendererHex +       # 65 mov ecx, g_renderer
            b"\x6A\x01"                        # 71 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 73 call CRenderer_Flip
            b"\x8B\x0D" + gRendererHex +       # 78 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 84 call CRenderer_RestoreFrame
            b"\xFF\xD6"                        # 89 call esi
            b"\x2B\x05" + dwFadeStartHex +     # 91 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 97 cmp eax, 0FFh
            b"\x0F\x82\xAB\xAB\xFF\xFF"        # 102 jb loc_51DDED
        )
        g_resMgrOffset = 6
        CResMgr_GetOffset = 13
        UIWMgr_DrawBTFOffset = 46
        CRenderer_DrawSceneOffset = 57
        CRenderer_FlipOffset = 74
        CRenderer_RestoreFrmOffset = 85
        offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)

    if offset is False:
        # 0  push offset aRag_logo_bmp
        # 5  call g_resMgr
        # 10 mov ecx, eax
        # 12 call CResMgr_Get
        # 17 push eax
        # 18 mov eax, g_renderer
        # 23 mov ecx, [eax+28h]
        # 26 mov edx, [eax+24h]
        # 29 sub ecx, 7Ah
        # 32 sub edx, 0C0h
        # 38 push ecx
        # 39 push edx
        # 40 mov ecx, offset g_windowMgr
        # 45 call UIWindowMgr_DrawBitmapToFrame
        # 50 mov ecx, g_renderer
        # 56 call sub_40D440
        # 61 mov ecx, g_renderer
        # 67 push 1
        # 69 call CRenderer_Flip
        # 74 mov ecx, g_renderer
        # 80 call CRenderer_RestoreFrame
        # 85 call edi
        # 87 mov ecx, dwFadeStart
        # 93 mov esi, eax
        # 95 sub esi, ecx
        # 97 cmp esi, 0FFh
        # 103 jb loc_4D4D53
        code = (
            b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
            b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
            b"\x8B\xC8"                        # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
            b"\x50"                            # 17 push eax
            b"\xA1" + gRendererHex +           # 18 mov eax, g_renderer
            b"\x8B\x48" + self.exe.toHexB(height) +  # 23 mov ecx, [eax+28h]
            b"\x8B\x50" + self.exe.toHexB(width) +  # 26 mov edx, [eax+24h]
            b"\x83\xE9\x7A"                    # 29 sub ecx, 7Ah
            b"\x81\xEA\xC0\x00\x00\x00"        # 32 sub edx, 0C0h
            b"\x51"                            # 38 push ecx
            b"\x52"                            # 39 push edx
            b"\xB9" + gWindowMgrHex +          # 40 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmap
            b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 56 call sub_40D440
            b"\x8B\x0D" + gRendererHex +       # 61 mov ecx, g_renderer
            b"\x6A\x01"                        # 67 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 69 call CRenderer_Flip
            b"\x8B\x0D" + gRendererHex +       # 74 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 80 call CRenderer_RestoreFrame
            b"\xFF\xD7"                        # 85 call edi
            b"\x8B\x0D" + dwFadeStartHex +     # 87 mov ecx, dwFadeStart
            b"\x8B\xF0"                        # 93 mov esi, eax
            b"\x2B\xF1"                        # 95 sub esi, ecx
            b"\x81\xFE\xFF\x00\x00\x00"        # 97 cmp esi, 0FFh
            b"\x0F\x82\xAB\xAB\xFF\xFF"        # 103 jb loc_4D4D53
        )
        g_resMgrOffset = 6
        CResMgr_GetOffset = 13
        UIWMgr_DrawBTFOffset = 46
        CRenderer_DrawSceneOffset = 0
        CRenderer_FlipOffset = 70
        CRenderer_RestoreFrmOffset = 81
        offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)

    if offset is False:
        # 0  push offset aRag_logo_bmp
        # 5  call g_resMgr
        # 10 mov ecx, eax
        # 12 call CResMgr_Get
        # 17 mov ecx, g_renderer
        # 23 push eax
        # 24 mov eax, [ecx+CRenderer.m_height]
        # 27 sub eax, 7Ah
        # 30 push eax
        # 31 mov eax, [ecx+CRenderer.m_width]
        # 34 mov ecx, offset g_windowMgr
        # 39 sub eax, 0C0h
        # 44 push eax
        # 45 call UIWindowMgr_DrawBitmapToFrame
        # 50 mov ecx, g_renderer
        # 56 call CRenderer_DrawScene
        # 61 test al, al
        # 63 jz short loc_718BBD
        # 65 mov ecx, g_renderer
        # 71 push 1
        # 73 call CRenderer_Flip
        # 78 mov ecx, g_renderer
        # 84 call CRenderer_RestoreFrame
        # 89 call esi
        # 91 sub eax, dwFadeStart
        # 97 cmp eax, 0FFh
        # 102 jb loc_718B50
        code = (
            b"\x68" + ragLogoHex +             # 0 push offset aRag_logo_bmp
            b"\xE8\xAB\xAB\xAB\xAB"            # 5 call g_resMgr
            b"\x8B\xC8"                        # 10 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 12 call CResMgr_Get
            b"\x8B\x0D" + gRendererHex +       # 17 mov ecx, g_renderer
            b"\x50"                            # 23 push eax
            b"\x8B\x41" + self.exe.toHexB(height) +  # 24 mov eax, [ecx+CRender
            b"\x83\xE8\x7A"                    # 27 sub eax, 7Ah
            b"\x50"                            # 30 push eax
            b"\x8B\x41" + self.exe.toHexB(width) +  # 31 mov eax, [ecx+CRendere
            b"\xB9" + gWindowMgrHex +          # 34 mov ecx, offset g_windowMgr
            b"\x2D\xC0\x00\x00\x00"            # 39 sub eax, 0C0h
            b"\x50"                            # 44 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 45 call UIWindowMgr_DrawBitmap
            b"\x8B\x0D" + gRendererHex +       # 50 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 56 call CRenderer_DrawScene
            b"\x84\xC0"                        # 61 test al, al
            b"\x74\x0D"                        # 63 jz short loc_718BBD
            b"\x8B\x0D" + gRendererHex +       # 65 mov ecx, g_renderer
            b"\x6A\x01"                        # 71 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 73 call CRenderer_Flip
            b"\x8B\x0D" + gRendererHex +       # 78 mov ecx, g_renderer
            b"\xE8\xAB\xAB\xAB\xAB"            # 84 call CRenderer_RestoreFrame
            b"\xFF\xD6"                        # 89 call esi
            b"\x2B\x05\xAB\xAB\xAB\xAB"        # 91 sub eax, dwFadeStart
            b"\x3D\xFF\x00\x00\x00"            # 97 cmp eax, 0FFh
            b"\x0F\x82"                        # 102 jb loc_718B50
        )
        g_resMgrOffset = 6
        CResMgr_GetOffset = 13
        UIWMgr_DrawBTFOffset = 46
        CRenderer_DrawSceneOffset = 57
        CRenderer_FlipOffset = 74
        CRenderer_RestoreFrmOffset = 85
        offset = self.exe.codeWildcard(code, b"\xAB", offset2, offset2 + 0x90)

    if offset is False:
        self.log("failed in search CResMgr_Get.")
        if errorExit is True:
            exit(1)
        return

    gResMgr = self.getAddr(offset,
                           g_resMgrOffset,
                           g_resMgrOffset + 4)
    if self.gResMgr != gResMgr:
        self.log("Error: found wrong g_resMgr.")
        exit(1)
    self.CResMgr_Get = self.getAddr(offset,
                                    CResMgr_GetOffset,
                                    CResMgr_GetOffset + 4)
    self.addRawFunc("CResMgr::Get", self.CResMgr_Get)
    self.UIWindowMgr_DrawBitmapToFrame = self.getAddr(offset,
                                                      UIWMgr_DrawBTFOffset,
                                                      UIWMgr_DrawBTFOffset + 4)
    self.addRawFunc("UIWindowMgr::DrawBitmapToFrame",
                    self.UIWindowMgr_DrawBitmapToFrame)
    if CRenderer_DrawSceneOffset != 0:
        self.CRenderer_DrawScene = self.getAddr(offset,
                                                CRenderer_DrawSceneOffset,
                                                CRenderer_DrawSceneOffset + 4)
        self.addRawFunc("CRenderer::DrawScene", self.CRenderer_DrawScene)
    self.CRenderer_Flip = self.getAddr(offset,
                                       CRenderer_FlipOffset,
                                       CRenderer_FlipOffset + 4)
    self.addRawFunc("CRenderer::Flip", self.CRenderer_Flip)
    if CRenderer_RestoreFrmOffset != 0:
        self.CRenderer_RestoreFrame = self.getAddr(offset,
                                                   CRenderer_RestoreFrmOffset,
                                                   CRenderer_RestoreFrmOffset +
                                                   4)
        self.addRawFunc("CRenderer::RestoreFrame", self.CRenderer_RestoreFrame)
