#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# search inside CFile::open
# 2017-


def searchCFileOpen1(self):
    # 2016-01-06
    # 0  cmp [ebp+directFlag], 0
    # 4  mov [esi+CFile.m_cursor], 0
    # 11 lea ebx, [esi+CFile.m_size]
    # 14 jnz short loc_6C5DC9
    # 16 cmp g_readFolderFirst, 0
    # 23 push ebx
    # 24 mov ecx, offset g_fileMgr
    # 29 push edi
    # 30 jz short loc_6C5DC0
    # 32 call CFileMgr_readFileDirect
    # 37 test eax, eax
    # 39 jnz short loc_6C5DD5
    # 41 push ebx
    # 42 push edi
    # 43 mov ecx, offset g_fileMgr
    # 48 call CFileMgr_readFileVirtualFs
    # 53 pop edi
    # 54 mov [esi+CFile.m_buf], eax
    # 57 test eax, eax
    # 59 pop esi
    # 60 setnz al
    code = (
        b"\x80\x7D\xAB\x00"                # 0 cmp [ebp+directFlag], 0
        b"\xC7\x46\xAB\x00\x00\x00\x00"    # 4 mov [esi+CFile.m_cursor], 0
        b"\x8D\x5E\xAB"                    # 11 lea ebx, [esi+CFile.m_size]
        b"\x75\xAB"                        # 14 jnz short loc_6C5DC9
        b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 16 cmp g_readFolderFirst, 0
        b"\x53"                            # 23 push ebx
        b"\xB9\xAB\xAB\xAB\xAB"            # 24 mov ecx, offset g_fileMgr
        b"\x57"                            # 29 push edi
        b"\x74\xAB"                        # 30 jz short loc_6C5DC0
        b"\xE8\xAB\xAB\xAB\xAB"            # 32 call CFileMgr_readFileDirect
        b"\x85\xC0"                        # 37 test eax, eax
        b"\x75\xAB"                        # 39 jnz short loc_6C5DD5
        b"\x53"                            # 41 push ebx
        b"\x57"                            # 42 push edi
        b"\xB9\xAB\xAB\xAB\xAB"            # 43 mov ecx, offset g_fileMgr
        b"\xE8\xAB\xAB\xAB\xAB"            # 48 call CFileMgr_readFileVirtualFs
        b"\x5F"                            # 53 pop edi
        b"\x89\x46\xAB"                    # 54 mov [esi+CFile.m_buf], eax
        b"\x85\xC0"                        # 57 test eax, eax
        b"\x5E"                            # 59 pop esi
        b"\x0F\x95\xC0"                    # 60 setnz al
    )
    cursorOffset = (6, 1)
    sizeOffset = (13, 1)
    bufOffset = (56, 1)
    readFolderOffset = 18
    gFileMgrOffset1 = 25
    gFileMgrOffset2 = 44
    readDirectOffset = 33
    readFileVfs = 49
    readFileOffset = 0
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.CFile_open,
                                   self.CFile_open + 0x100)

    if offset is False:
        # 2015-01-07
        # 0  mov ecx, dword ptr [ebp+directFlag]
        # 3  push ecx
        # 4  lea edx, [esi+CFile.m_size]
        # 7  push edx
        # 8  push edi
        # 9  mov ecx, offset g_fileMgr
        # 14 mov [esi+CFile.m_cursor], 0
        # 21 call CFileMgr_readFileWithFlag
        # 26 mov [esi+CFile.m_buf], eax
        # 29 test eax, eax
        # 31 pop edi
        # 32 setnz al
        code = (
            b"\x8B\x4D\xAB"                    # 0 mov ecx, dword ptr [ebp+dire
            b"\x51"                            # 3 push ecx
            b"\x8D\x56\xAB"                    # 4 lea edx, [esi+CFile.m_size]
            b"\x52"                            # 7 push edx
            b"\x57"                            # 8 push edi
            b"\xB9\xAB\xAB\xAB\xAB"            # 9 mov ecx, offset g_fileMgr
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 14 mov [esi+CFile.m_cursor], 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 21 call CFileMgr_readFileWithF
            b"\x89\x46\xAB"                    # 26 mov [esi+CFile.m_buf], eax
            b"\x85\xC0"                        # 29 test eax, eax
            b"\x5F"                            # 31 pop edi
            b"\x0F\x95\xC0"                    # 32 setnz al
        )
        cursorOffset = (16, 1)
        sizeOffset = (6, 1)
        bufOffset = (28, 1)
        readFolderOffset = 0
        gFileMgrOffset1 = 10
        gFileMgrOffset2 = 0
        readDirectOffset = 0
        readFileVfs = 0
        readFileOffset = 22
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x100)

    if offset is False:
        # 0  mov ecx, dword ptr [esp+8+directFlag]
        # 4  push ecx
        # 5  lea edx, [esi+CFile.m_size]
        # 8  push edx
        # 9  push edi
        # 10 mov ecx, offset g_fileMgr
        # 15 mov [esi+CFile.m_cursor], 0
        # 22 call CFileMgr_readFileWithFlag
        # 27 mov [esi+CFile.m_buf], eax
        # 30 test eax, eax
        # 32 pop edi
        # 33 setnz al
        code = (
            b"\x8B\x4C\x24\xAB"                # 0 mov ecx, dword ptr [esp+8+di
            b"\x51"                            # 4 push ecx
            b"\x8D\x56\xAB"                    # 5 lea edx, [esi+CFile.m_size]
            b"\x52"                            # 8 push edx
            b"\x57"                            # 9 push edi
            b"\xB9\xAB\xAB\xAB\xAB"            # 10 mov ecx, offset g_fileMgr
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 15 mov [esi+CFile.m_cursor], 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 22 call CFileMgr_readFileWithF
            b"\x89\x46\xAB"                    # 27 mov [esi+CFile.m_buf], eax
            b"\x85\xC0"                        # 30 test eax, eax
            b"\x5F"                            # 32 pop edi
            b"\x0F\x95\xC0"                    # 33 setnz al
        )
        cursorOffset = (17, 1)
        sizeOffset = (7, 1)
        bufOffset = (29, 1)
        readFolderOffset = 0
        gFileMgrOffset1 = 11
        gFileMgrOffset2 = 0
        readDirectOffset = 0
        readFileVfs = 0
        readFileOffset = 23
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x100)

    if offset is False:
        # 2011-01-04
        # 0  lea edi, [ebx+CFile.m_size]
        # 3  mov [ebx+CFile.m_cursor], 0
        # 10 cmp g_readFolderFirst, 0
        # 17 push edi
        # 18 mov ecx, offset g_fileMgr
        # 23 push esi
        # 24 jz short loc_55DF3B
        # 26 call CFileMgr_readFileDirect
        # 31 test eax, eax
        # 33 jnz short loc_55DF50
        # 35 push edi
        # 36 push esi
        # 37 mov ecx, offset g_fileMgr
        # 42 call CFileMgr_readFileVirtualFs
        # 47 pop edi
        # 48 mov [ebx+CFile.m_buf], eax
        # 51 test eax, eax
        # 53 pop esi
        # 54 setnz al
        code = (
            b"\x8D\x7B\xAB"                    # 0 lea edi, [ebx+CFile.m_size]
            b"\xC7\x43\xAB\x00\x00\x00\x00"    # 3 mov [ebx+CFile.m_cursor], 0
            b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 10 cmp g_readFolderFirst, 0
            b"\x57"                            # 17 push edi
            b"\xB9\xAB\xAB\xAB\xAB"            # 18 mov ecx, offset g_fileMgr
            b"\x56"                            # 23 push esi
            b"\x74\xAB"                        # 24 jz short loc_55DF3B
            b"\xE8\xAB\xAB\xAB\xAB"            # 26 call CFileMgr_readFileDirec
            b"\x85\xC0"                        # 31 test eax, eax
            b"\x75\xAB"                        # 33 jnz short loc_55DF50
            b"\x57"                            # 35 push edi
            b"\x56"                            # 36 push esi
            b"\xB9\xAB\xAB\xAB\xAB"            # 37 mov ecx, offset g_fileMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 42 call CFileMgr_readFileVirtu
            b"\x5F"                            # 47 pop edi
            b"\x89\x43\xAB"                    # 48 mov [ebx+CFile.m_buf], eax
            b"\x85\xC0"                        # 51 test eax, eax
            b"\x5E"                            # 53 pop esi
            b"\x0F\x95\xC0"                    # 54 setnz al
        )
        cursorOffset = (5, 1)
        sizeOffset = (2, 1)
        bufOffset = (50, 1)
        readFolderOffset = 12
        gFileMgrOffset1 = 19
        gFileMgrOffset2 = 38
        readDirectOffset = 27
        readFileVfs = 43
        readFileOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x100)

    if offset is False:
        # 2010-01-05
        # 0  push esi
        # 1  push ebx
        # 2  mov ecx, offset g_fileMgr
        # 7  call CFileMgr_readFileVirtualFs
        # 12 mov edi, [ebp+var_4]
        # 15 test eax, eax
        # 17 mov [edi+CFile.m_buf], eax
        # 20 pop edi
        # 21 pop esi
        # 22 pop ebx
        # 23 setnz al
        code = (
            b"\x56"                            # 0 push esi
            b"\x53"                            # 1 push ebx
            b"\xB9\xAB\xAB\xAB\xAB"            # 2 mov ecx, offset g_fileMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 7 call CFileMgr_readFileVirtua
            b"\x8B\x7D\xAB"                    # 12 mov edi, [ebp+var_4]
            b"\x85\xC0"                        # 15 test eax, eax
            b"\x89\x47\xAB"                    # 17 mov [edi+CFile.m_buf], eax
            b"\x5F"                            # 20 pop edi
            b"\x5E"                            # 21 pop esi
            b"\x5B"                            # 22 pop ebx
            b"\x0F\x95\xC0"                    # 23 setnz al
        )
        cursorOffset = 0
        sizeOffset = 0
        bufOffset = (19, 1)
        readFolderOffset = 0
        gFileMgrOffset1 = 3
        gFileMgrOffset2 = 0
        readDirectOffset = 0
        readFileVfs = 8
        readFileOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x100)

    if offset is False:
        # 2003-10-28
        # 0  lea esi, [ebx+CFile.m_size]
        # 3  mov ecx, offset g_fileMgr
        # 8  mov [ebx+CFile.m_cursor], 0
        # 15 push esi
        # 16 push edi
        # 17 call CFileMgr_readFileVirtualFs
        # 22 test eax, eax
        # 24 jnz loc_4E8577
        code = (
            b"\x8D\x73\xAB"                    # 0 lea esi, [ebx+CFile.m_size]
            b"\xB9\xAB\xAB\xAB\xAB"            # 3 mov ecx, offset g_fileMgr
            b"\xC7\x43\xAB\x00\x00\x00\x00"    # 8 mov [ebx+CFile.m_cursor], 0
            b"\x56"                            # 15 push esi
            b"\x57"                            # 16 push edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 17 call CFileMgr_readFileVirtu
            b"\x85\xC0"                        # 22 test eax, eax
            b"\x0F\x85"                        # 24 jnz loc_4E8577
        )
        cursorOffset = (10, 1)
        sizeOffset = (2, 1)
        bufOffset = 0
        readFolderOffset = 0
        gFileMgrOffset1 = 4
        gFileMgrOffset2 = 0
        readDirectOffset = 0
        readFileVfs = 18
        readFileOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x100)


    if offset is False:
        self.log("Error: failed in seach CFile::open 1")
        if self.cFileMode == 0:
            exit(1)
        return

    if bufOffset != 0:
        buf = self.getVarAddr(offset, bufOffset)
    if cursorOffset != 0:
        cursor = self.getVarAddr(offset, cursorOffset)
    else:
        cursor = buf + 4
    if sizeOffset != 0:
        size = self.getVarAddr(offset, sizeOffset)
    else:
        size = buf + 8
    if bufOffset == 0:
        buf = cursor - 4
    fileName = size + 4
    if cursor == 0 or buf == 0 or size == 0:
        self.log("Error: wrong CFile struct member offset")
        exit(1)

    self.gFileMgr = self.exe.readUInt(offset + gFileMgrOffset1)
    if gFileMgrOffset2 != 0:
        gFileMgr2 = self.exe.readUInt(offset + gFileMgrOffset2)
        if self.gFileMgr != gFileMgr2:
            self.log("Error: found different g_fileMgr")
            exit(1)
    self.addVaVar("g_fileMgr", self.gFileMgr)
    if readFolderOffset != 0:
        self.gReadFolderFirst = self.exe.readUInt(offset + readFolderOffset)
        self.addVaVar("g_readFolderFirst", self.gReadFolderFirst)

    if readDirectOffset != 0:
        self.CFileMgr_readFileDirect = self.getAddr(offset,
                                                    readDirectOffset,
                                                    readDirectOffset + 4)
        self.addRawFunc("CFileMgr::readFileDirect",
                        self.CFileMgr_readFileDirect)
    if readFileVfs != 0:
        self.CFileMgr_readFileVirtualFs = self.getAddr(offset,
                                                       readFileVfs,
                                                       readFileVfs + 4)
        self.addRawFunc("CFileMgr::readFileVirtualFs",
                        self.CFileMgr_readFileVirtualFs)

    self.addStruct("CFile")
    self.addStructMember("m_hFile", 0, 4, True)
    self.setStructMemberType(0, "HANDLE")
    self.addStructMember("m_buf", buf, 4, True)
    self.setStructMemberType(buf, "char*")
    self.addStructMember("m_cursor", cursor, 4, True)
    self.addStructMember("m_size", size, 4, True)
    self.addStructMember("m_name", fileName, 4, True)
    self.setStructMemberType(fileName, "char[128]")
    self.cFileMode = 2

    if readFileOffset != 0:
        self.CFileMgr_readFileWithFlag = self.getAddr(offset,
                                                      readFileOffset,
                                                      readFileOffset + 4)
        self.CFileMgr_readFileWithFlagCall = offset + readFileOffset
        self.addRawFuncType("CFileMgr::readFileWithFlag",
                            self.CFileMgr_readFileWithFlag,
                            "void *__thiscall CFileMgr_readFileWithFlag("
                            "CFileMgr *this, LPCSTR lpFileName, "
                            "int *fileSize, char directFlag)")
