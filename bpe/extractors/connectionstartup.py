#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchConnectionStartup(self, errorExit):
    # seach Connection_Startup function call from WinMain
    # 2017 +
    # jnz label1
    # call CConnection_Startup
    # test al, al
    # jnz label1
    # mov esi, 1
    # jmp label2
    code = (
        b"\x75\xAB" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x84\xC0" +
        b"\x75\xAB" +
        b"\xBE\x01\x00\x00\x00" +
        b"\xE9")
    offset = self.exe.codeWildcard(code, b"\xAB")
    addrOffset = 3
    if offset is False:
        # 2016-03-16
        # jnz label1
        # call CConnection_Startup
        # test al, al
        # jz label2
        # push 3Ch
        # call addr1
        code = (
            b"\x75\xAB" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x84\xC0" +
            b"\x74\xAB" +
            b"\x6A\x3C" +
            b"\xE8")
        offset = self.exe.codeWildcard(code, b"\xAB")
        addrOffset = 3
    if offset is False:
        # 2015-03-18
        # jnz label1
        # call CConnection_Startup
        # test al, al
        # jz label2
        # push 3Ch
        # call addr1
        code = (
            b"\x75\xAB" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x84\xC0" +
            b"\x0F\xAB\xAB\xAB\xAB\xFF" +
            b"\x6A\x3C" +
            b"\xE8")
        offset = self.exe.codeWildcard(code, b"\xAB")
        addrOffset = 3
    if offset is False:
        # 2011-01-04
        # mov addr1, eax
        # call CConnection_Startup
        # test al, al
        # jz label2
        # push 3Ch
        # call addr1
        code = (
            b"\xA3\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x84\xC0" +
            b"\x74\xAB" +
            b"\x6A\x3C" +
            b"\xE8")
        offset = self.exe.codeWildcard(code, b"\xAB")
        addrOffset = 6
    if offset is False:
        # 2010-01-05
        # mov addr1, eax
        # call CConnection_Startup
        # test al, al
        # jnz label2
        # pop edi
        # pop esi
        code = (
            b"\xA3\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x84\xC0" +
            b"\x75\xAB" +
            b"\x5F" +
            b"\x5E")
        offset = self.exe.codeWildcard(code, b"\xAB")
        addrOffset = 6
    if offset is False:
        # 2010-08-17
        # mov addr1, eax
        # call CConnection_Startup
        # test al, al
        # jz label2
        # push 3Ch
        # call addr1
        code = (
            b"\xA3\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x84\xC0" +
            b"\x0F\xAB\xAB\xAB\xAB\xFF" +
            b"\x6A\x3C" +
            b"\xE8")
        offset = self.exe.codeWildcard(code, b"\xAB")
        addrOffset = 6
    if offset is False:
        self.log("failed in search Connection::Startup call")
        if errorExit is True:
            exit(1)
        return
    self.connectionStartup = self.getAddr(offset,
                                          addrOffset,
                                          addrOffset + 4)
    self.addRawFunc("CConnection::Startup", self.connectionStartup)
    self.connectionStartupVa = self.exe.rawToVa(self.connectionStartup)
    recvOffset, recvSection = self.exe.string(b"recv")
    if recvOffset is False:
        self.log("'recv' string not found")
        if errorExit is True:
            exit(1)
        return
    recvOffset = recvSection.rawToVa(recvOffset)

    sendOffset, sendSection = self.exe.string(b"send")
    if sendOffset is False:
        self.log("'send' string not found")
        if errorExit is True:
            exit(1)
        return
    sendOffset = sendSection.rawToVa(sendOffset)

    # search setting recvAdd and sendAddr block.

    # 2016 +
    # 0  push offset "send"
    # 5  push eax
    # 6  mov hModule, eax
    # 11 call GetProccAddress
    # 13 push offset "recv"
    # 18 push hModule
    # 24 mov sendAddr, eax     <--
    # 29 call GetProccAddress
    # 31 cmp sendAddr, 0
    # 38 mov esi, MessageBoxA
    # 44 mov recvAddr, eax     <--
    # 49 jnz label1
    code = (
        b"\x68" + self.exe.toHex(sendOffset, 4) +
        b"\xAB" +
        b"\xA3\xAB\xAB\xAB\xAB" +
        b"\xFF\xAB" +
        b"\x68" + self.exe.toHex(recvOffset, 4) +
        b"\xFF\x35\xAB\xAB\xAB\xAB" +
        b"\xA3\xAB\xAB\xAB\xAB" +
        b"\xFF\xAB" +
        b"\x83\xAB\xAB\xAB\xAB\xAB\x00" +
        b"\x8B\x35\xAB\xAB\xAB\xAB" +
        b"\xA3\xAB\xAB\xAB\xAB" +
        b"\x75")
    sendAddrOffset = 25
    recvAddrOffset = 45
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.connectionStartup,
                                   self.connectionStartup + 0x100)
    if offset is False:
        # 2015-01-07aRagexeRE
        # 0  push offset "send"
        # 5  push eax
        # 6  mov hModule, eax
        # 11 call GetProccAddress
        # 13 mov sendAddr, eax     <--
        # 18 mov eax, hModule
        # 23 push offset "recv"
        # 28 push eax
        # 29 call GetProccAddress
        # 31 cmp sendAddr, 0
        # 38 mov esi, MessageBoxA
        # 44 mov recvAddr, eax     <--
        # 49 jnz label1
        code = (
            b"\x68" + self.exe.toHex(sendOffset, 4) +
            b"\xAB" +
            b"\xA3\xAB\xAB\xAB\xAB" +
            b"\xFF\xAB" +
            b"\xA3\xAB\xAB\xAB\xAB" +
            b"\xA1\xAB\xAB\xAB\xAB" +
            b"\x68" + self.exe.toHex(recvOffset, 4) +
            b"\xAB" +
            b"\xFF\xAB" +
            b"\x83\xAB\xAB\xAB\xAB\xAB\x00" +
            b"\x8B\x35\xAB\xAB\xAB\xAB" +
            b"\xA3\xAB\xAB\xAB\xAB" +
            b"\x75")
        sendAddrOffset = 14
        recvAddrOffset = 45
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.connectionStartup,
                                       self.connectionStartup + 0x100)
    if offset is False:
        # 2010-01-05
        # 0  push offset "send"
        # 5  push eax
        # 6  mov hModule, eax
        # 11 call GetProccAddress
        # 13 mov sendAddr, eax     <--
        # 18 mov eax, hModule
        # 23 push offset "recv"
        # 28 push eax
        # 29 call GetProccAddress
        # 31 mov ebx, MessageBoxA
        # 37 mov recvAddr, eax     <--
        # 42 mov eax, sendAddr
        # 47 test eax, eax
        # 49 jnz label1
        code = (
            b"\x68" + self.exe.toHex(sendOffset, 4) +
            b"\xAB" +
            b"\xA3\xAB\xAB\xAB\xAB" +
            b"\xFF\xAB" +
            b"\xA3\xAB\xAB\xAB\xAB" +
            b"\xA1\xAB\xAB\xAB\xAB" +
            b"\x68" + self.exe.toHex(recvOffset, 4) +
            b"\xAB" +
            b"\xFF\xAB" +
            b"\x8B\x1D\xAB\xAB\xAB\xAB" +
            b"\xA3\xAB\xAB\xAB\xAB" +
            b"\xA1\xAB\xAB\xAB\xAB" +
            b"\x85\xC0" +
            b"\x75")
        sendAddrOffset = 14
        recvAddrOffset = 38
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.connectionStartup,
                                       self.connectionStartup + 0x100)
    if offset is False:
        self.log("failed in search recvAddr and sendAddr block")
        if errorExit is True:
            exit(1)
        return
    self.showRawAddr("recv/send init block", offset)
    self.sendAddr = self.exe.readUInt(offset + sendAddrOffset)
    self.recvAddr = self.exe.readUInt(offset + recvAddrOffset)
    self.addVaVar("sendAddr", self.sendAddr)
    self.addVaVar("recvAddr", self.recvAddr)
