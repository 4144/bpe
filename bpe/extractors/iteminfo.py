#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchItemInfo(self):
    if self.ITEM_INFO_init == 0:
        self.log("Error: skip ITEM_INFO search because "
                 "ITEM_INFO_init not found")
        if self.packetVersion < "20160000" or \
           self.clientType in ("ruro", "bro", "iro", "euro", "tro"):
            return
        exit(1)

    # search in ITEM_INFO_init
    # 0  ret retn
    # 1  align 10h
    code = (
        b"\xC3"                            # 0
        b"\xCC\xCC"                        # 1
    )
    offset = self.exe.code(code,
                           self.ITEM_INFO_init,
                           self.ITEM_INFO_init + 0x150)
    if offset is False:
        # 0  ret retn
        # 1  align 10h
        code = (
            b"\xC3\xCC"                        # 0
        )
        offset = self.exe.code(code,
                               self.ITEM_INFO_init,
                               self.ITEM_INFO_init + 0x150)
    if offset is False:
        # 0  ret retn
        code = (
            b"\xC3"                            # 0
        )
        offset = self.exe.code(code,
                               self.ITEM_INFO_init,
                               self.ITEM_INFO_init + 0x150)
    if offset is False:
        self.log("failed in search ITEM_INFO_init end")
        exit(1)
    endOffset = offset

    # 0  mov [esi+ITEM_INFO.item_options_count], 0
    # 10 mov ecx, 19h
    # 15 lea eax, [esi+ITEM_INFO.item_options]
    # 21 mov byte ptr [eax], 0
    # 24 lea eax, [eax+1]
    # 27 dec ecx
    # 28 jnz short loc_7FA047
    # 30 mov [esi+ITEM_INFO.field_DD], cl
    # 36 pop esi
    # 37 ret retn
    code = (
        b"\xC7\x86\xAB\xAB\x00\x00\x00\x00\x00\x00"  # 0
        b"\xB9\xAB\xAB\x00\x00"            # 10
        b"\x8D\x86\xAB\xAB\x00\x00"        # 15
        b"\xC6\x00\x00"                    # 21
        b"\x8D\x40\x01"                    # 24
        b"\x49"                            # 27
        b"\x75\xF7"                        # 28
        b"\x88\x8E\xAB\xAB\x00\x00"        # 30
        b"\x5E"                            # 36
        b"\xC3"                            # 37
    )
    itemOptionsCountOffset = (2, 4)
    itemOptionsSizeOffset = (11, 4)
    itemOptionsOffset = (17, 4)
    lastFieldOffset = (32, 4)
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.ITEM_INFO_init,
                                   endOffset)

    if offset is False:
        # 0  mov [esi+ITEM_INFO.item_options_count], 0
        # 10 lea eax, [esi+ITEM_INFO.item_options]
        # 16 mov ecx, 19h
        # 21 pop esi
        # 22 mov byte ptr [eax], 0
        # 25 lea eax, [eax+1]
        # 28 dec ecx
        # 29 jnz short loc_77C3C5
        # 31 ret retn
        code = (
            b"\xC7\x86\xAB\xAB\x00\x00\x00\x00\x00\x00"  # 0
            b"\x8D\x86\xAB\xAB\x00\x00"        # 10
            b"\xB9\xAB\xAB\x00\x00"            # 16
            b"\x5E"                            # 21
            b"\xC6\x00\x00"                    # 22
            b"\x8D\x40\x01"                    # 25
            b"\x49"                            # 28
            b"\x75\xF7"                        # 29
            b"\xC3"                            # 31
        )
        itemOptionsCountOffset = (2, 4)
        itemOptionsSizeOffset = (17, 4)
        itemOptionsOffset = (12, 4)
        lastFieldOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.ITEM_INFO_init,
                                       endOffset)
    if offset is False:
        # 0  mov [esi+ITEM_INFO.item_options_count], 0
        # 7  lea eax, [esi+ITEM_INFO.item_options]
        # 10 mov ecx, 19h
        # 15 pop esi
        # 16 mov byte ptr [eax], 0
        # 19 lea eax, [eax+1]
        # 22 dec ecx
        # 23 jnz short loc_6B8194
        # 25 ret retn
        code = (
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 0
            b"\x8D\x46\xAB"                    # 7
            b"\xB9\xAB\xAB\x00\x00"            # 10
            b"\x5E"                            # 15
            b"\xC6\x00\x00"                    # 16
            b"\x8D\x40\x01"                    # 19
            b"\x49"                            # 22
            b"\x75\xF7"                        # 23
            b"\xC3"                            # 25
        )
        itemOptionsCountOffset = (2, 1)
        itemOptionsSizeOffset = (11, 4)
        itemOptionsOffset = (9, 1)
        lastFieldOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.ITEM_INFO_init,
                                       endOffset)

    if offset is False:
        # 0  lea eax, [esi+ITEM_INFO.item_options.index]
        # 6  mov [esi+ITEM_INFO.view_sprite], 0
        # 13 mov ecx, 19h
        # 18 mov word ptr [esi+ITEM_INFO.favorite], 0
        # 24 mov [esi+ITEM_INFO.move_flag_], 0
        # 31 mov [esi+ITEM_INFO.field_7C], 0
        # 38 mov [esi+ITEM_INFO.item_options_count], 0
        # 48 mov byte ptr [eax], 0
        # 51 lea eax, [eax+1]
        # 54 sub ecx, 1
        # 57 jnz short loc_81B371
        # 59 mov [esi+ITEM_INFO.have_index], cl
        # 65 pop esi
        # 66 ret retn
        code = (
            b"\x8D\x86\xAB\xAB\x00\x00"        # 0 lea eax, [esi+ITEM_INFO.item
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 6 mov [esi+ITEM_INFO.view_spri
            b"\xB9\xAB\xAB\x00\x00"            # 13 mov ecx, 19h
            b"\x66\xC7\x46\xAB\x00\x00"        # 18 mov word ptr [esi+ITEM_INFO
            b"\xC6\x86\xAB\x00\x00\x00\x00"    # 24 mov [esi+ITEM_INFO.move_fla
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 31 mov [esi+ITEM_INFO.field_7C
            b"\xC7\x86\xAB\xAB\x00\x00\x00\x00\x00\x00"  # 38 mov [esi+ITEM_INF
            b"\xC6\x00\x00"                    # 48 mov byte ptr [eax], 0
            b"\x8D\x40\x01"                    # 51 lea eax, [eax+1]
            b"\x83\xE9\x01"                    # 54 sub ecx, 1
            b"\x75\xF5"                        # 57 jnz short loc_81B371
            b"\x88\x8E\xAB\xAB\x00\x00"        # 59 mov [esi+ITEM_INFO.have_ind
            b"\x5E"                            # 65 pop esi
            b"\xC3"                            # 66 ret retn
        )
        itemOptionsCountOffset = (40, 4)
        itemOptionsSizeOffset = (14, 4)
        itemOptionsOffset = (2, 4)
        lastFieldOffset = (61, 4)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.ITEM_INFO_init,
                                       endOffset)

    if offset is False:
        # iro 2019-05-13
        # 0  mov [esi+ITEM_INFO.item_options_count], 0
        # 10 lea eax, [esi+ITEM_INFO.item_options]
        # 16 mov ecx, 19h
        # 21 pop esi
        # 22 lea ebx, [ebx+0]
        # 28 mov byte ptr [eax], 0
        # 31 lea eax, [eax+1]
        # 34 dec ecx
        # 35 jnz short loc_6EA8F0
        # 37 ret retn
        code = (
            b"\xC7\x86\xAB\xAB\x00\x00\x00\x00\x00\x00"  # 0 mov [esi+ITEM_INFO
            b"\x8D\x86\xAB\xAB\x00\x00"       # 10 lea eax, [esi+ITEM_INFO.item
            b"\xB9\xAB\xAB\x00\x00"           # 16 mov ecx, 19h
            b"\x5E"                           # 21 pop esi
            b"\x8D\x9B\x00\x00\x00\x00"       # 22 lea ebx, [ebx+0]
            b"\xC6\x00\x00"                   # 28 mov byte ptr [eax], 0
            b"\x8D\x40\x01"                   # 31 lea eax, [eax+1]
            b"\x49"                           # 34 dec ecx
            b"\x75\xF7"                       # 35 jnz short loc_6EA8F0
            b"\xC3"                           # 37 ret retn
        )
        itemOptionsCountOffset = (2, 4)
        itemOptionsSizeOffset = (17, 4)
        itemOptionsOffset = (12, 4)
        lastFieldOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.ITEM_INFO_init,
                                       endOffset)

    if offset is False:
        self.log("Error: failed in search ITEM_INFO_init last offset")
        exit(1)

    itemOptionsCount = self.getVarAddr(offset, itemOptionsCountOffset)
    itemOptionsSize = self.getVarAddr(offset, itemOptionsSizeOffset)
    itemOptions = self.getVarAddr(offset, itemOptionsOffset)
    if lastFieldOffset != 0:
        lastField = self.getVarAddr(offset, lastFieldOffset)
        if itemOptions + itemOptionsSize != lastField:
            self.log("Error: found wrong end field in ITEM_INFO")
            exit(1)
        haveIndex = lastField
    else:
        lastField = itemOptions + itemOptionsSize
        haveIndex = -1

    if itemOptionsCount + 4 != itemOptions:
        self.log("Error: wrong item options fields found in ITEM_INFO")
        exit(1)
    if itemOptionsSize != 25:
        self.log("Error: found unknown item options fields size in ITEM_INFO")
        exit(1)

    itemOptionsMaxCount = int(itemOptionsSize / 5)

    # 0  push eax
    # 1  push offset g_regPath
    # 6  lea ecx, [esi+ITEM_INFO.m_itemname_IdStr.m_cstr]
    # 9  mov word ptr [esi+ITEM_INFO.identified], 0
    # 15 mov [esi+ITEM_INFO.refine_level], 0
    # 22 mov [esi+ITEM_INFO.bind_on_equip], ax
    # 26 call std_string_assign
    # 31 push 0
    # 33 push offset g_regPath
    # 38 lea ecx, [esi+ITEM_INFO.str_field_48.m_cstr]
    # 41 call std_string_assign
    code = (
        b"\x50"                            # 0
        b"\x68\xAB\xAB\xAB\xAB"            # 1
        b"\x8D\x4E\xAB"                    # 6
        b"\x66\xC7\x46\xAB\x00\x00"        # 9   unused
        b"\xC7\x46\xAB\x00\x00\x00\x00"    # 15  unused
        b"\x66\x89\x46\xAB"                # 22  unused
        b"\xE8\xAB\xAB\xAB\xAB"            # 26
        b"\x6A\x00"                        # 31
        b"\x68\xAB\xAB\xAB\xAB"            # 33
        b"\x8D\x4E\xAB"                    # 38
        b"\xE8\xAB\xAB\xAB\xAB"            # 41
    )
    itemname_IdStrOffset = (8, 1)
    id_strOffset = (40, 1)
    fromChars1Offset = 27
    fromChars2Offset = 42
    cardsOffset = 0
    identifiedOffset = (12, 1)
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.ITEM_INFO_init,
                                   endOffset)

    if offset is False:
        # 0  push eax
        # 1  push offset emptyStr
        # 6  mov [esi+ITEM_INFO.item_type], 0
        # 12 lea ecx, [esi+ITEM_INFO.id_str.m_cstr]
        # 15 mov [esi+ITEM_INFO.location], 0
        # 22 mov [esi+ITEM_INFO.item_index], 0
        # 29 mov [esi+ITEM_INFO.wear_location], 0
        # 36 mov [esi+ITEM_INFO.amount], 0
        # 43 mov [esi+ITEM_INFO.price], 0
        # 50 mov [esi+ITEM_INFO.real_price], 0
        # 57 movups xmmword ptr [esi+ITEM_INFO.card0], xmm0
        # 61 mov word ptr [esi+ITEM_INFO.identified], 0
        # 67 mov [esi+ITEM_INFO.refine_level], 0
        # 74 mov [esi+ITEM_INFO.bind_on_equip], ax
        # 78 call std_string_assign
        # 83 push 0
        # 85 push offset emptyStr
        # 90 lea ecx, [esi+ITEM_INFO.item_name.m_cstr]
        # 93 call std_string_assign
        code = (
            b"\x50"                            # 0 push eax
            b"\x68\xAB\xAB\xAB\xAB"            # 1 push offset emptyStr
            b"\xC7\x06\x00\x00\x00\x00"        # 6 mov [esi+ITEM_INFO.item_type
            b"\x8D\x4E\xAB"                    # 12 lea ecx, [esi+ITEM_INFO.id_
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 15 mov [esi+ITEM_INFO.location
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 22 mov [esi+ITEM_INFO.item_ind
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 29 mov [esi+ITEM_INFO.wear_loc
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 36 mov [esi+ITEM_INFO.amount],
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 43 mov [esi+ITEM_INFO.price],
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 50 mov [esi+ITEM_INFO.real_pri
            b"\x0F\x11\x46\xAB"                # 57 movups xmmword ptr [esi+ITE
            b"\x66\xC7\x46\xAB\x00\x00"        # 61 mov word ptr [esi+ITEM_INFO
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 67 mov [esi+ITEM_INFO.refine_l
            b"\x66\x89\x46\xAB"                # 74 mov [esi+ITEM_INFO.bind_on_
            b"\xE8\xAB\xAB\xAB\xAB"            # 78 call std_string_assign
            b"\x6A\x00"                        # 83 push 0
            b"\x68\xAB\xAB\xAB\xAB"            # 85 push offset emptyStr
            b"\x8D\x4E\xAB"                    # 90 lea ecx, [esi+ITEM_INFO.ite
            b"\xE8"                            # 93 call std_string_assign
        )
        itemname_IdStrOffset = (14, 1)
        id_strOffset = (92, 1)
        fromChars1Offset = 79
        fromChars2Offset = 94
        cardsOffset = (60, 1)
        item_typeValue = 0
        locationOffset = (17, 1)
        item_indexOffset = (24, 1)
        wear_locationOffset = (31, 1)
        amountOffset = (38, 1)
        priceOffset = (45, 1)
        real_priceOffset = (52, 1)
        card1Offset = cardsOffset
        card2Offset = 0
        identifiedOffset = (64, 1)
        refineLevelOffset = (69, 1)
        bind_on_equipOffset = (77, 1)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.ITEM_INFO_init,
                                       endOffset)

    if offset is False:
        self.log("Error: failed in search std_string_assign "
                 "in ITEM_INFO_init")
        if self.packetVersion > "20170000":
            exit(1)
        foundChars = False
    else:
        foundChars = True

    if foundChars is True:
        itemname_IdStr = self.getVarAddr(offset, itemname_IdStrOffset)
        id_str = self.getVarAddr(offset, id_strOffset)
        fromChars1 = self.getAddr(offset,
                                  fromChars1Offset,
                                  fromChars1Offset + 4)
        fromChars2 = self.getAddr(offset,
                                  fromChars2Offset,
                                  fromChars2Offset + 4)
        if fromChars1 != fromChars2:
            self.log("Error: found different std_string_assign")
            exit(1)
        self.std_string_assign = fromChars1
        self.addRawFuncType("std::string::assign",
                            self.std_string_assign,
                            "int __thiscall std_string_assign("
                            "struct_std_string *std_string_ptr,"
                            " char *inStr, int len)")

    if cardsOffset == 0:
        # 0  push esi
        # 1  mov esi, ecx
        # 3  xor eax, eax
        # 5  xorps xmm0, xmm0
        # 8  mov dword ptr [esi+ITEM_INFO.item_type], 0
        # 14 mov [esi+ITEM_INFO.location], 0
        # 21 mov [esi+ITEM_INFO.item_index], 0
        # 28 mov [esi+ITEM_INFO.wear_location], 0
        # 35 mov [esi+ITEM_INFO.amount], 0
        # 42 mov [esi+ITEM_INFO.price], 0
        # 49 mov [esi+ITEM_INFO.real_price], 0
        # 56 movq qword ptr [esi+ITEM_INFO.card0], xmm0
        # 61 movq qword ptr [esi+ITEM_INFO.card2], xmm0
        code = (
            b"\x56"                            # 0
            b"\x8B\xF1"                        # 1
            b"\x33\xC0"                        # 3
            b"\x0F\x57\xC0"                    # 5
            b"\xC7\x06\x00\x00\x00\x00"        # 8
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 14
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 21
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 28
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 35
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 42
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 49
            b"\x66\x0F\xD6\x46\xAB"            # 56
            b"\x66\x0F\xD6\x46"                # 61
        )
        item_typeValue = 0
        locationOffset = (16, 1)
        item_indexOffset = (23, 1)
        wear_locationOffset = (30, 1)
        amountOffset = (37, 1)
        priceOffset = (44, 1)
        real_priceOffset = (51, 1)
        card1Offset = (60, 1)
        card2Offset = (65, 1)
        identifiedOffset = 0
        refineLevelOffset = 0
        bind_on_equipOffset = 0
        offset = self.exe.matchWildcardOffset(code,
                                              b"\xAB",
                                              self.ITEM_INFO_init)

        if offset is not True:
            self.log("Error: failed in search cards in ITEM_INFO_init")
            exit(1)
        offset = self.ITEM_INFO_init

    if offset is False:
        self.log("Error: ITEM_INFO not found")
        if self.packetVersion < "20170000":
            return
        exit(1)

    if item_typeValue >= 0:
        item_type = item_typeValue
    else:
        self.log("Error: item_type not found")
        exit(1)

    location = self.getVarAddr(offset, locationOffset)
    item_index = self.getVarAddr(offset, item_indexOffset)
    wear_location = self.getVarAddr(offset, wear_locationOffset)
    amount = self.getVarAddr(offset, amountOffset)
    price = self.getVarAddr(offset, priceOffset)
    real_price = self.getVarAddr(offset, real_priceOffset)
    card1 = self.getVarAddr(offset, card1Offset)
    if card2Offset != 0:
        card2 = self.getVarAddr(offset, card2Offset)
    elif cardsOffset != 0:
        card2 = card1 + 4
    if identifiedOffset != 0:
        identified = self.getVarAddr(offset, identifiedOffset)
    if refineLevelOffset != 0:
        refineLevel = self.getVarAddr(offset, refineLevelOffset)
    if bind_on_equipOffset != 0:
        bind_on_equip = self.getVarAddr(offset, bind_on_equipOffset)
    if foundChars is True:
        if card1 >= itemname_IdStr:
            self.log("Error: found wrong location in ITEM_INFO_init")
            exit(1)
        if itemname_IdStr >= id_str:
            self.log("Error: found wrong location in ITEM_INFO_init")
            exit(1)
        if identifiedOffset != 0:
            if id_str >= identified:
                self.log("Error: found wrong location in ITEM_INFO_init")
                exit(1)
        # add more checks
        if location >= item_index:
            self.log("Error: found wrong location in ITEM_INFO_init")
            exit(1)
        if item_index >= wear_location:
            self.log("Error: found wrong item_index in ITEM_INFO_init")
            exit(1)
        if wear_location >= amount:
            self.log("Error: found wrong wear_location in ITEM_INFO_init")
            exit(1)
        if amount >= price:
            self.log("Error: found wrong amount in ITEM_INFO_init")
            exit(1)
        if price >= real_price:
            self.log("Error: found wrong price in ITEM_INFO_init")
            exit(1)
        if real_price >= card1:
            self.log("Error: found wrong real_price in ITEM_INFO_init")
            exit(1)
        if card1 >= card2:
            self.log("Error: found wrong cards in ITEM_INFO_init")
            exit(1)

    self.addStruct("struct_item_option")
    self.addStructMember("index", 0, 2)
    self.addStructMember("value", 2, 2)
    self.addStructMember("param", 4, 1)

    mod = lastField % 4
    if mod != 1:
        self.log("Possible error in ITEM_INFO size search")
        exit(1)
    self.ITEM_INFO_Size = lastField + 4 - mod
    self.showVaAddr("len(ITEM_INFO)", self.ITEM_INFO_Size)
    self.addStruct("ITEM_INFO")
    self.setStructComment("Size {0}".format(hex(self.ITEM_INFO_Size)))
    self.addStructMember("item_type", item_type, 4, True)
    self.addStructMember("location", location, 4, True)
    self.addStructMember("item_index", item_index, 4, True)
    self.addStructMember("wear_location", wear_location, 4, True)
    self.addStructMember("amount", amount, 4, True)
    self.addStructMember("price", price, 4, True)
    self.addStructMember("real_price", real_price, 4, True)
    self.addStructMember("cards", card1, 16, True)
    self.setStructMemberType(card1, "int[4]")
    if foundChars is True:
        self.addStructMember("item_id_str",
                             itemname_IdStr,
                             self.stdstringSize,
                             True)
        self.setStructMemberType(itemname_IdStr, "struct_std_string")
        self.addStructMember("item_name_str",
                             id_str,
                             self.stdstringSize,
                             True)
        self.setStructMemberType(id_str, "struct_std_string")
    if identifiedOffset != 0:
        self.addStructMember("identified", identified, 1, True)
        self.addStructMember("damaged", identified + 1, 1, True)
    if refineLevelOffset != 0:
        self.addStructMember("refine_level", refineLevel, 4, True)
    if bind_on_equipOffset != 0:
        self.addStructMember("bind_on_equip", bind_on_equip, 2, True)
    self.addStructMember("item_options_count", itemOptionsCount, 4, True)
    self.addStructMember("item_options", itemOptions, itemOptionsSize, True)
    self.setStructMemberType(itemOptions,
                             "struct_item_option[{0}]".
                             format(itemOptionsMaxCount))
    if haveIndex != -1:
        self.addStructMember("have_index", haveIndex, 1, True)
    self.addStructMember("padding_end", self.ITEM_INFO_Size - 1, 1)
