#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchAccountId(self):
    # search in CMsgEffect::OnProcess (CMsgEffect_virt4)
    # 0  cmp eax, 1
    # 3  jnz short loc_AD3CE5
    # 5  mov eax, [edi+CMsgEffect.m_masterGid]
    # 11 cmp eax, accountId
    # 17 jnz short loc_AD3CE5
    # 19 mov ecx, offset g_modeMgr
    # 24 call CModeMgr_GetGameMode
    # 29 mov edx, [eax]
    # 31 push 0
    # 33 push 0
    # 35 push 0
    code = (
        b"\x83\xF8\x01"                    # 0
        b"\x75\xAB"                        # 3
        b"\x8B\x87\xAB\xAB\xAB\xAB"        # 5
        b"\x3B\x05\xAB\xAB\xAB\xAB"        # 11
        b"\x75\xAB"                        # 17
        b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +  # 19
        b"\xE8\xAB\xAB\xAB\xAB"            # 24
        b"\x8B\x10"                        # 29
        b"\x6A\x00"                        # 31
        b"\x6A\x00"                        # 33
        b"\x6A\x00"                        # 35
    )
    jmp1Offset = (4, 1)
    jmp2Offset = (18, 1)
    masterGidOffset = 7
    accountIdOffset = 13
    getGameModeOffset = 25
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  cmp eax, 1
        # 3  jnz loc_AB1375
        # 9  mov eax, [edi+CMsgEffect.m_masterGid]
        # 15 cmp eax, g_session.aid
        # 21 jnz loc_AB1375
        # 27 mov ecx, offset g_modeMgr
        # 32 call CModeMgr_GetGameMode
        # 37 push 0
        # 39 push 0
        # 41 push 0
        code = (
            b"\x83\xF8\x01"                    # 0 cmp eax, 1
            b"\x0F\x85\xAB\xAB\xAB\xAB"        # 3 jnz loc_AB1375
            b"\x8B\x87\xAB\xAB\xAB\x00"        # 9 mov eax, [edi+CMsgEffect.m_m
            b"\x3B\x05\xAB\xAB\xAB\xAB"        # 15 cmp eax, g_session.aid
            b"\x0F\x85\xAB\xAB\xAB\xAB"        # 21 jnz loc_AB1375
            b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +  # 27 mov ecx, offset
            b"\xE8\xAB\xAB\xAB\xAB"            # 32 call CModeMgr_GetGameMode
            b"\x6A\x00"                        # 37 push 0
            b"\x6A\x00"                        # 39 push 0
            b"\x6A\x00"                        # 41 push 0
        )
        jmp1Offset = (5, 4)
        jmp2Offset = (23, 4)
        masterGidOffset = 11
        accountIdOffset = 17
        getGameModeOffset = 33
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  cmp ecx, 1
        # 3  jnz loc_6877FD
        # 9  mov ecx, [esi+140h]
        # 15 mov eax, dword ptr account_id
        # 20 cmp ecx, eax
        # 22 jnz loc_6877FD
        # 28 mov ecx, offset g_modeMgr
        # 33 call CModeMgr_GetGameMode
        # 38 mov edx, [eax]
        # 40 push 0
        # 42 push 0
        # 44 push 0
        code = (
            b"\x83\xF9\x01"                    # 0
            b"\x0F\x85\xAB\xAB\xAB\xAB"        # 3
            b"\x8B\x8E\xAB\xAB\xAB\xAB"        # 9
            b"\xA1\xAB\xAB\xAB\xAB"            # 15
            b"\x3B\xC8"                        # 20
            b"\x0F\x85\xAB\xAB\xAB\xAB"        # 22
            b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +  # 28
            b"\xE8\xAB\xAB\xAB\xAB"            # 33
            b"\x8B\x10"                        # 38
            b"\x6A\x00"                        # 40
            b"\x6A\x00"                        # 42
            b"\x6A\x00"                        # 44
        )
        jmp1Offset = (5, 4)
        jmp2Offset = (24, 4)
        masterGidOffset = 11
        accountIdOffset = 16
        getGameModeOffset = 34
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  cmp ecx, 1
        # 3  jnz short loc_91B336
        # 5  mov edx, [esi+158h]
        # 11 cmp edx, dword_CD9530
        # 17 jnz short loc_91B336
        # 19 mov ecx, offset g_modeMgr
        # 24 call CModeMgr_GetGameMode
        # 29 mov edx, [eax]
        # 31 push 0
        # 33 push 0
        # 35 push 0
        code = (
            b"\x83\xF9\x01"                    # 0
            b"\x75\xAB"                        # 3
            b"\x8B\x96\xAB\xAB\xAB\xAB"        # 5
            b"\x3B\x15\xAB\xAB\xAB\xAB"        # 11
            b"\x75\xAB"                        # 17
            b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +  # 19
            b"\xE8\xAB\xAB\xAB\xAB"            # 24
            b"\x8B\x10"                        # 29
            b"\x6A\x00"                        # 31
            b"\x6A\x00"                        # 33
            b"\x6A\x00"                        # 35
        )
        jmp1Offset = (4, 1)
        jmp2Offset = (18, 1)
        masterGidOffset = 7
        accountIdOffset = 13
        getGameModeOffset = 25
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  cmp ecx, 1
        # 3  jnz short loc_87B1FA
        # 5  mov edx, [esi+158h]
        # 11 cmp edx, account_id
        # 17 jnz short loc_87B1FA
        # 19 mov ecx, offset g_modeMgr
        # 24 call CModeMgr_GetGameMode
        # 29 mov edx, [eax]
        # 31 push ebx
        # 32 push ebx
        # 33 push ebx
        code = (
            b"\x83\xF9\x01"                    # 0
            b"\x75\xAB"                        # 3
            b"\x8B\x96\xAB\xAB\xAB\xAB"        # 5
            b"\x3B\x15\xAB\xAB\xAB\xAB"        # 11
            b"\x75\xAB"                        # 17
            b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +  # 19
            b"\xE8\xAB\xAB\xAB\xAB"            # 24
            b"\x8B\x10"                        # 29
            b"\x53"                            # 31
            b"\x53"                            # 32
            b"\x53"                            # 33
        )
        jmp1Offset = (4, 1)
        jmp2Offset = (18, 1)
        masterGidOffset = 7
        accountIdOffset = 13
        getGameModeOffset = 25
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  cmp ecx, 1
        # 3  jnz short loc_6B5846
        # 5  mov ecx, [esi+14Ch]
        # 11 cmp ecx, accountId
        # 17 jnz short loc_6B5846
        # 19 mov ecx, offset g_modeMgr
        # 24 call CModeMgr_GetGameMode
        # 29 mov edx, [eax]
        # 31 push ebx
        # 32 push ebx
        # 33 push ebx
        code = (
            b"\x83\xF9\x01"                    # 0
            b"\x75\xAB"                        # 3
            b"\x8B\x8E\xAB\xAB\xAB\xAB"        # 5
            b"\x3B\x0D\xAB\xAB\xAB\xAB"        # 11
            b"\x75\xAB"                        # 17
            b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +  # 19
            b"\xE8\xAB\xAB\xAB\xAB"            # 24
            b"\x8B\x10"                        # 29
            b"\x53"                            # 31
            b"\x53"                            # 32
            b"\x53"                            # 33
        )
        jmp1Offset = (4, 1)
        jmp2Offset = (18, 1)
        masterGidOffset = 7
        accountIdOffset = 13
        getGameModeOffset = 25
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  cmp ecx, 1
        # 3  jnz short loc_6E6BFB
        # 5  mov eax, [esi+15Ch]
        # 11 cmp eax, dword ptr accountId
        # 17 jnz short loc_6E6BFB
        # 19 mov ecx, offset g_modeMgr
        # 24 call CModeMgr_GetGameMode
        # 29 mov edx, [eax]
        # 31 push ebx
        # 32 push ebx
        # 33 push ebx
        code = (
            b"\x83\xF9\x01"                    # 0
            b"\x75\xAB"                        # 3
            b"\x8B\x86\xAB\xAB\xAB\xAB"        # 5
            b"\x3B\x05\xAB\xAB\xAB\xAB"        # 11
            b"\x75\xAB"                        # 17
            b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +  # 19
            b"\xE8\xAB\xAB\xAB\xAB"            # 24
            b"\x8B\x10"                        # 29
            b"\x53"                            # 31
            b"\x53"                            # 32
            b"\x53"                            # 33
        )
        jmp1Offset = (4, 1)
        jmp2Offset = (18, 1)
        masterGidOffset = 7
        accountIdOffset = 13
        getGameModeOffset = 25
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  cmp ecx, 1
        # 3  jnz loc_6A3FED
        # 9  mov edx, [esi+140h]
        # 15 mov eax, accountId
        # 20 cmp edx, eax
        # 22 jnz loc_6A3FED
        # 28 mov ecx, offset g_modeMgr
        # 33 call CModeMgr_GetGameMode
        # 38 mov edx, [eax]
        # 40 push 0
        # 42 push 0
        # 44 push 0
        code = (
            b"\x83\xF9\x01"                    # 0
            b"\x0F\x85\xAB\xAB\xAB\xAB"        # 3
            b"\x8B\x96\xAB\xAB\xAB\xAB"        # 9
            b"\xA1\xAB\xAB\xAB\xAB"            # 15
            b"\x3B\xD0"                        # 20
            b"\x0F\x85\xAB\xAB\xAB\xAB"        # 22
            b"\xB9" + self.exe.toHex(self.g_modeMgr, 4) +  # 28
            b"\xE8\xAB\xAB\xAB\xAB"            # 33
            b"\x8B\x10"                        # 38
            b"\x6A\x00"                        # 40
            b"\x6A\x00"                        # 42
            b"\x6A\x00"                        # 44
        )
        jmp1Offset = (5, 4)
        jmp2Offset = (24, 4)
        masterGidOffset = 11
        accountIdOffset = 16
        getGameModeOffset = 34
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search accountId.")
        exit(1)
    getGameMode = self.getAddr(offset,
                               getGameModeOffset,
                               getGameModeOffset + 4)
    if getGameMode != self.getGameModeAddr:
        self.log("failed in search accountId (CModeMgr::GetGameMode wrong)")
        exit(1)
    jmp1 = self.getVarAddr(offset, jmp1Offset) + jmp1Offset[0]
    jmp2 = self.getVarAddr(offset, jmp2Offset) + jmp2Offset[0]
    if jmp1 != jmp2:
        self.log("Wrong jmp in search accountId")
        exit(1)
    masterGid = self.exe.readUInt(offset + masterGidOffset)
    self.addStruct("CMsgEffect")
    self.addStructMember("m_masterGid", masterGid, 4, True)

    if self.session == 0:
        self.log("Error: cant decode CSession.m_accountId "
                 "because g_session not found")
        exit(1)
    self.CSession_accountId = self.exe.readUInt(offset + accountIdOffset) - \
        self.session
    self.addStruct("CSession")
    self.addStructMember("m_account_id", self.CSession_accountId, 4, True)
