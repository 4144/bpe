#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchActRes(self):
    # search in CRenderObject_virt28
    # 0  fstp [ebp+actIndex]
    # 3  movss xmm0, [ebp+actIndex]
    # 8  xorps xmm1, xmm1
    # 11 comiss xmm1, xmm0
    # 14 movss [ebp+actIndex], xmm0
    # 19 jb short loc_9A2388
    # 21 push edi
    # 22 mov ecx, ebx
    # 24 call CActRes_GetDelay
    code = (
        b"\xD9\x5D\xAB"                    # 0 fstp [ebp+actIndex]
        b"\xF3\x0F\x10\x45\xAB"            # 3 movss xmm0, [ebp+actIndex]
        b"\x0F\x57\xC9"                    # 8 xorps xmm1, xmm1
        b"\x0F\x2F\xC8"                    # 11 comiss xmm1, xmm0
        b"\xF3\x0F\x11\x45\xAB"            # 14 movss [ebp+actIndex], xmm0
        b"\x72\xAB"                        # 19 jb short loc_9A2388
        b"\x57"                            # 21 push edi
        b"\x8B\xCB"                        # 22 mov ecx, ebx
        b"\xE8"                            # 24 call CActRes_GetDelay
    )
    actIndexOffsets = (2, 7, 18)
    getDelayOffset = 25
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search CActRes::GetDelay.")
        return
    actIndex = -1
    for indexOffset in actIndexOffsets:
        tmp = self.exe.readUByte(offset + indexOffset)
        if actIndex == -1:
            actIndex = tmp
        elif actIndex != tmp:
            self.log("Error: found different actIndex offset: "
                     "{0} vs {1}".format(actIndex, tmp))
            exit(1)
    self.CActRes_GetDelay = self.getAddr(offset,
                                         getDelayOffset,
                                         getDelayOffset + 4)
    self.addRawFunc("CActRes::GetDelay", self.CActRes_GetDelay)
