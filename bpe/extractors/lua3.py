#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchLua3(self):
    offset, _ = self.exe.string(b"Lua 5.1")
    if offset is False:
        self.log("Error: Lua 5.1 not found")
        if self.packetVersion >= "20120110" and self.clientType != "iro":
            exit(1)
        return

    offset, section = self.exe.string(b"CItemInfoMgr")
    if offset is False:
        self.log("Error: CItemInfoMgr not found")
        if self.packetVersion >= "20130000" and \
           self.clientType not in ("bro", "euro", "ruro", "tro"):
            exit(1)
        return
    CItemInfoMgrHex = self.exe.toHex(section.rawToVa(offset), 4)

    offset, section = self.exe.string(b"second return value is not string...")
    if offset is False:
        self.log("Error: CAchievementMgr not found")
        exit(1)
    secondReturnHex = self.exe.toHex(section.rawToVa(offset), 4)

    # 0  push 0FFFFFFFFh
    # 2  push [ebp+obj.L]
    # 5  call lua_isstring
    # 10 add esp, 8
    # 13 test eax, eax
    # 15 jnz short loc_72F84A
    # 17 push eax
    # 18 push offset aCiteminfomgr
    # 23 push offset aSecondReturnVa
    # 28 jmp short loc_72F7F6
    # 30 push 0FFFFFFFEh
    # 32 push [ebp+obj.L]
    # 35 call lua_toboolean
    # 40 add esp, 8
    # 43 test eax, eax
    # 45 jnz loc_72F8DF
    # 51 push eax
    # 52 push 0FFFFFFFFh
    # 54 push [ebp+obj.L]
    # 57 call lua_tolstring
    # 62 add esp, 0Ch
    # 65 lea ecx, [ebp+var_58]
    # 68 push eax
    # 69 call std_string_from_chars
    code = (
        b"\x6A\xFF"                        # 0 push 0FFFFFFFFh
        b"\xFF\x75\xAB"                    # 2 push [ebp+obj.L]
        b"\xE8\xAB\xAB\xAB\xAB"            # 5 call lua_isstring
        b"\x83\xC4\x08"                    # 10 add esp, 8
        b"\x85\xC0"                        # 13 test eax, eax
        b"\x75\x0D"                        # 15 jnz short loc_72F84A
        b"\x50"                            # 17 push eax
        b"\x68" + CItemInfoMgrHex +        # 18 push offset aCiteminfomgr
        b"\x68" + secondReturnHex +        # 23 push offset aSecondReturnVa
        b"\xEB\xAB"                        # 28 jmp short loc_72F7F6
        b"\x6A\xFE"                        # 30 push 0FFFFFFFEh
        b"\xFF\x75\xAB"                    # 32 push [ebp+obj.L]
        b"\xE8\xAB\xAB\xAB\xAB"            # 35 call lua_toboolean
        b"\x83\xC4\x08"                    # 40 add esp, 8
        b"\x85\xC0"                        # 43 test eax, eax
        b"\x0F\x85\xAB\xAB\xAB\xAB"        # 45 jnz loc_72F8DF
        b"\x50"                            # 51 push eax
        b"\x6A\xFF"                        # 52 push 0FFFFFFFFh
        b"\xFF\x75\xAB"                    # 54 push [ebp+obj.L]
        b"\xE8\xAB\xAB\xAB\xAB"            # 57 call lua_tolstring
        b"\x83\xC4\x0C"                    # 62 add esp, 0Ch
        b"\x8D\x4D\xAB"                    # 65 lea ecx, [ebp+var_58]
        b"\x50"                            # 68 push eax
        b"\xE8"                            # 69 call std_string_from_chars
    )
    isstringOffset = 6
    tobooleanOffset = 36
    tolstringOffset = 58
    fromCharsOffset = (70, True)
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2015
        # 0  mov edx, [ebp+L]
        # 3  push 0FFFFFFFFh
        # 5  push edx
        # 6  call lua_isstring
        # 11 add esp, 8
        # 14 test eax, eax
        # 16 jnz short loc_6229D2
        # 18 push eax
        # 19 push offset aCiteminfomgr
        # 24 push offset aSecondReturnVa
        # 29 jmp loc_622944
        # 34 mov eax, [ebp+L]
        # 37 push 0FFFFFFFEh
        # 39 push eax
        # 40 call lua_toboolean
        # 45 add esp, 8
        # 48 test eax, eax
        # 50 jnz loc_622A6A
        # 56 mov ecx, [ebp+L]
        # 59 push eax
        # 60 push 0FFFFFFFFh
        # 62 push ecx
        # 63 call lua_tolstring
        # 68 add esp, 0Ch
        # 71 push eax
        # 72 lea ecx, [ebp+var_64]
        # 75 call std_string_from_chars
        code = (
            b"\x8B\x55\xAB"                    # 0 mov edx, [ebp+L]
            b"\x6A\xFF"                        # 3 push 0FFFFFFFFh
            b"\x52"                            # 5 push edx
            b"\xE8\xAB\xAB\xAB\xAB"            # 6 call lua_isstring
            b"\x83\xC4\x08"                    # 11 add esp, 8
            b"\x85\xC0"                        # 14 test eax, eax
            b"\x75\x10"                        # 16 jnz short loc_6229D2
            b"\x50"                            # 18 push eax
            b"\x68" + CItemInfoMgrHex +        # 19 push offset aCiteminfomgr
            b"\x68" + secondReturnHex +        # 24 push offset aSecondReturnVa
            b"\xE9\xAB\xAB\xAB\xAB"            # 29 jmp loc_622944
            b"\x8B\x45\xAB"                    # 34 mov eax, [ebp+L]
            b"\x6A\xFE"                        # 37 push 0FFFFFFFEh
            b"\x50"                            # 39 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 40 call lua_toboolean
            b"\x83\xC4\x08"                    # 45 add esp, 8
            b"\x85\xC0"                        # 48 test eax, eax
            b"\x0F\x85\xAB\xAB\xAB\xAB"        # 50 jnz loc_622A6A
            b"\x8B\x4D\xAB"                    # 56 mov ecx, [ebp+L]
            b"\x50"                            # 59 push eax
            b"\x6A\xFF"                        # 60 push 0FFFFFFFFh
            b"\x51"                            # 62 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"            # 63 call lua_tolstring
            b"\x83\xC4\x0C"                    # 68 add esp, 0Ch
            b"\x50"                            # 71 push eax
            b"\x8D\x4D\xAB"                    # 72 lea ecx, [ebp+var_64]
            b"\xE8"                            # 75 call std_string_from_chars
        )
        isstringOffset = 7
        tobooleanOffset = 41
        tolstringOffset = 64
        fromCharsOffset = (76, True)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2013
        # 0  mov edx, [esp+78h+L]
        # 4  push 0FFFFFFFFh
        # 6  push edx
        # 7  call lua_isstring
        # 12 add esp, 8
        # 15 test eax, eax
        # 17 jnz short loc_5A221B
        # 19 push eax
        # 20 push offset aCiteminfomgr
        # 25 push offset aSecondReturnVa
        # 30 jmp loc_5A2186
        # 35 mov eax, [esp+78h+L]
        # 39 push 0FFFFFFFEh
        # 41 push eax
        # 42 call lua_toboolean
        # 47 add esp, 8
        # 50 test eax, eax
        # 52 jnz loc_5A22C9
        # 58 mov ecx, [esp+78h+L]
        # 62 push eax
        # 63 push 0FFFFFFFFh
        # 65 push ecx
        # 66 call lua_tolstring
        # 71 add esp, 0Ch
        # 74 push eax
        # 75 lea ecx, [esp+7Ch+var_64]
        # 79 call ??0?$basic_string@DU?$char_traits@D@std@@V?$allocator@D@2@@st
        code = (
            b"\x8B\x54\x24\xAB"                # 0 mov edx, [esp+78h+L]
            b"\x6A\xFF"                        # 4 push 0FFFFFFFFh
            b"\x52"                            # 6 push edx
            b"\xE8\xAB\xAB\xAB\xAB"            # 7 call lua_isstring
            b"\x83\xC4\x08"                    # 12 add esp, 8
            b"\x85\xC0"                        # 15 test eax, eax
            b"\x75\x10"                        # 17 jnz short loc_5A221B
            b"\x50"                            # 19 push eax
            b"\x68" + CItemInfoMgrHex +        # 20 push offset aCiteminfomgr
            b"\x68" + secondReturnHex +        # 25 push offset aSecondReturnVa
            b"\xE9\xAB\xAB\xAB\xAB"            # 30 jmp loc_5A2186
            b"\x8B\x44\x24\xAB"                # 35 mov eax, [esp+78h+L]
            b"\x6A\xFE"                        # 39 push 0FFFFFFFEh
            b"\x50"                            # 41 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 42 call lua_toboolean
            b"\x83\xC4\x08"                    # 47 add esp, 8
            b"\x85\xC0"                        # 50 test eax, eax
            b"\x0F\x85\xAB\xAB\xAB\xAB"        # 52 jnz loc_5A22C9
            b"\x8B\x4C\x24\xAB"                # 58 mov ecx, [esp+78h+L]
            b"\x50"                            # 62 push eax
            b"\x6A\xFF"                        # 63 push 0FFFFFFFFh
            b"\x51"                            # 65 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"            # 66 call lua_tolstring
            b"\x83\xC4\x0C"                    # 71 add esp, 0Ch
            b"\x50"                            # 74 push eax
            b"\x8D\x4C\x24\xAB"                # 75 lea ecx, [esp+7Ch+var_64]
            b"\xFF\x15"                        # 79 call ??0?$basic_string@DU?$
        )
        isstringOffset = 8
        tobooleanOffset = 43
        tolstringOffset = 67
        fromCharsOffset = (81, False)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("Error: CAchievementMgr usage not found")
        exit(1)

    self.lua_isstring = self.getAddr(offset,
                                     isstringOffset,
                                     isstringOffset + 4)
    self.addRawFuncType("lua_isstring",
                        self.lua_isstring,
                        "int __cdecl lua_isstring(void *L, int idx)")

    self.lua_toboolean = self.getAddr(offset,
                                      tobooleanOffset,
                                      tobooleanOffset + 4)
    self.addRawFuncType("lua_toboolean",
                        self.lua_toboolean,
                        "BOOL __cdecl lua_toboolean(void *L, int idx)")

    self.lua_tolstring = self.getAddr(offset,
                                      tolstringOffset,
                                      tolstringOffset + 4)
    self.addRawFuncType("lua_tolstring",
                        self.lua_tolstring,
                        "const char *__cdecl lua_tolstring"
                        "(void *L, int idx, size_t *len)")

    if fromCharsOffset[1] is True:
        self.std_string_from_chars = self.getAddr(offset,
                                                  fromCharsOffset[0],
                                                  fromCharsOffset[0] + 4)
        self.addRawFuncType("std::string::from_chars",
                            self.std_string_from_chars,
                            "struct_std_string *__thiscall "
                            "std_string_from_chars"
                            "(struct_std_string *this, const char *src)")
    else:
        self.std_string_from_chars = \
            self.exe.readUInt(offset + fromCharsOffset[0])
        self.addVaFuncType("std::string::from_chars",
                           self.std_string_from_chars,
                           "struct_std_string *(__thiscall*)("
                           "std_string_from_chars)"
                           "(struct_std_string *this, const char *src)")
