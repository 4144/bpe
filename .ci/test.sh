#!/usr/bin/env bash

${PYTHON} ./info.py ${CLIENT}.exe || exit 1
./.ci/compare.sh ${CLIENT}/bpe_info.txt || exit 1
./.ci/compare.sh ida/ida_bpe_${CLIENT}.idc || exit 1

${PYTHON} ./class.py ${CLIENT}.exe || exit 1
./.ci/compare.sh ${CLIENT}/bpe_class.txt || exit 1
./.ci/compare.sh classes/${CLIENT}.txt || exit 1

${PYTHON} ./list.py ${CLIENT}.exe || exit 1
./.ci/compare.sh ${CLIENT}/bpe_list.txt || exit 1
./.ci/compare.sh decompile_bpe.sh || exit 1

${PYTHON} ./maxchars.py ${CLIENT}.exe || exit 1
./.ci/compare.sh ${CLIENT}/bpe_maxchars.txt || exit 1
./.ci/compare.sh maxchars_bpe.h || exit 1

${PYTHON} ./msgstringtable.py ${CLIENT}.exe || exit 1
./.ci/compare.sh ${CLIENT}/bpe_msgstringtable.txt || exit 1
./.ci/compare.sh msgstringtable/${CLIENT}/msgstringtable.txt || exit 1
./.ci/compare.sh msgstringtable/${CLIENT}/msgstringtable_hex.txt || exit 1
./.ci/compare.sh msgstringtable/${CLIENT}/msgstringtable_text.txt || exit 1

${PYTHON} ./sendpackets.py ${CLIENT}.exe || exit 1
./.ci/compare.sh ${CLIENT}/bpe_sendpackets.txt || exit 1
./.ci/compare.sh sendpackets/${CLIENT}/sendpackets.txt || exit 1
