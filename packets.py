#! /usr/bin/env -S python2 -OO
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from bpe.extractor import Extractor


def processClients():
    files = sorted(os.listdir("clients"))
    for file1 in files:
        if file1[0] == ".":
            continue
        # skip binary with condition in fill packets.
        # some strange packet ids in code
        if file1 == "2008-06-17bSakexe.exe" or \
           file1 == "2008-06-24bSakexe.exe" or \
           file1 == "2008-07-01aSakexe.exe" or \
           file1 == "2008-07-02aSakexe.exe" or \
           file1 == "2008-11-19aSakexe.exe" or \
           file1 == "2008-11-26aSakexe.exe":
            continue
        extractor = Extractor()
        extractor.init(file1, "bpe_packets_info.txt")
        extractor.getpackets()


extractor = Extractor()
if len(sys.argv) > 1:
    extractor.init(sys.argv[1], "bpe_packets_info.txt")
    extractor.getpackets()
else:
    processClients()
